# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
#
# Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
# All Rights Reserved.

cmake_minimum_required(VERSION 3.0.0)

project(fmt-download NONE)

include(ExternalProject)
ExternalProject_Add(GoogleTest
        GIT_REPOSITORY    https://github.com/google/googletest.git
        GIT_TAG           release-1.11.0
        SOURCE_DIR        "${CMAKE_CURRENT_BINARY_DIR}/src"
        BINARY_DIR        "${CMAKE_CURRENT_BINARY_DIR}/build"
        CONFIGURE_COMMAND ""
        BUILD_COMMAND     ""
        INSTALL_COMMAND   ""
        TEST_COMMAND      "")
