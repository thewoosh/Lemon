# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
#
# Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
# All Rights Reserved.

option(ENABLE_PCH "Enable PreCompiledHeaders" ON)

if (ENABLE_PCH)
    add_library(PCH_Libraries INTERFACE)
    target_precompile_headers(PCH_Libraries INTERFACE
            # C++ standard libraries
            <memory>
            <string>
            <vector>

            # Our own libraries
            Source/Base/Logger.hpp)
    target_link_libraries(project_diagnostics INTERFACE PCH_Libraries)
endif()
