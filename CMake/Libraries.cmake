# SPDX-License-Identifier: MPL-2.0
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at https://mozilla.org/MPL/2.0/.
#
# Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
# All Rights Reserved.

option(LIBRARY_FMT_NON_LOCAL "Don't use the system's {fmt} library" OFF)
option(LIBRARY_GOOGLETEST_NON_LOCAL "Don't use the system's GoogleTest library" OFF)

if (NOT LIBRARY_FMT_NON_LOCAL)
    find_package(fmt QUIET)
endif()

if (NOT ${LIBRARY_GOOGLETEST_NON_LOCAL})
    find_package(GTest 1.10.0 QUIET)
endif()

if (NOT fmt_FOUND)
    add_subdirectory(${CMAKE_SOURCE_DIR}/ThirdParty/fmt)
endif()

if ((${CMAKE_TESTING_ENABLED}) AND (NOT ${GTest_FOUND}))
    add_subdirectory(${CMAKE_SOURCE_DIR}/ThirdParty/GoogleTest)
endif()

find_package(glfw3 REQUIRED)
find_package(GLEW REQUIRED)
set(OpenGL_GL_PREFERENCE "GLVND")
find_package(OpenGL REQUIRED)

find_package(Freetype REQUIRED)
include_directories(${FREETYPE_INCLUDE_DIR_freetype2})
