/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string_view>
#include <vector>

namespace html {
    struct Token;
} // namespace html

namespace html::testing {

    void
    runTokenizerComparison(std::u32string_view input, const std::vector<Token> &expectedTokens) noexcept;

} // namespace html::testing