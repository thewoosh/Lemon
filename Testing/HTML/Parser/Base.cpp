/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#define TESTING_BASE_DONT_CREATE_MAIN
#include "Testing/Base.hpp"

#include "Source/HTML/Parser/Tokenizer.hpp"

namespace html::testing {

    static void
    compareDOCTYPEs(const Token::DoctypeData &result, const Token::DoctypeData &expected) noexcept {
        EXPECT_EQ(result.name, expected.name);
        EXPECT_EQ(result.publicIdentifier, expected.publicIdentifier);
        EXPECT_EQ(result.systemIdentifier, expected.systemIdentifier);
    }

    static void
    compareTags(const Token::TagData &result, const Token::TagData &expected) noexcept {
        EXPECT_EQ(result.tagName, expected.tagName);
        EXPECT_EQ(result.selfClosingFlag, expected.selfClosingFlag);
    }

    void
    runTokenizerComparison(std::u32string_view input, const std::vector<Token> &expectedTokens) noexcept {
        std::vector<Token> resultedTokens;
        Tokenizer tokenizer{input};

        tokenizer.setTokenEmissionCallback([&](Token &&token) -> bool {
            resultedTokens.push_back(std::move(token));
            return true;
        });

        EXPECT_TRUE(tokenizer.run());
        EXPECT_EQ(std::size(resultedTokens), std::size(expectedTokens));

        const auto max = std::min(std::size(resultedTokens), std::size(expectedTokens));
        for (std::size_t i = 0; i < max; ++i) {
            const auto &result = resultedTokens[i];
            const auto &expected = expectedTokens[i];

            EXPECT_EQ(result.type(), expected.type());
            if (result.type() != expected.type())
                continue;

            switch (result.type()) {
                case Token::Type::DOCTYPE:
                    compareDOCTYPEs(result.asDoctype(), expected.asDoctype());
                    break;
                case Token::Type::START_TAG:
                case Token::Type::END_TAG:
                    compareTags(result.asTag(), expected.asTag());
                    break;
                case Token::Type::COMMENT:
                    EXPECT_EQ(result.asComment(), expected.asComment());
                    break;
                case Token::Type::CHARACTER:
                    EXPECT_EQ(result.asCharacter(), expected.asCharacter());
                    break;
                case Token::Type::END_OF_FILE:
                    break;
                default:
                    LEMON_ASSERT_NOT_REACHED_OR_RETURN_VOID();
            }
        }
    }

} // namespace html::testing
