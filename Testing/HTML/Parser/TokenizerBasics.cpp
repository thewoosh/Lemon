/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Testing/Base.hpp"
#include "Testing/HTML/Parser/Base.hpp"

#include <string_view>

#include "Source/Base/ContainerAdditions.hpp"
#include "Source/HTML/Parser/Token.hpp"

namespace html::testing {

    TEST(TokenizerBasics, MinimalDocument) {
        constexpr const std::u32string_view text{UR"(<!DOCTYPE html><html><head></head><body></body></html>)"};

        runTokenizerComparison(text, base::createVector<Token>(
                Token::createDoctype("html"),
                Token::createStartTag("html"),
                Token::createStartTag("head"),
                Token::createEndTag("head"),
                Token::createStartTag("body"),
                Token::createEndTag("body"),
                Token::createEndTag("html"),
                Token::createEOF()
        ));
    }

    TEST(TokenizerBasics, AttributeTest) {
        constexpr const std::u32string_view text{UR"(<html attrib-name="attrib-value" empty-attrib="">)"};

        runTokenizerComparison(text, base::createVector<Token>(
                Token::createStartTag("html", false, AttributeList(base::createVector<Attribute>(
                        Attribute{"attrib-name", "attrib-value"},
                        Attribute{"empty-attrib", std::string{}}
                ))),
                Token::createEOF()
        ));
    }

} // namespace html::testing
