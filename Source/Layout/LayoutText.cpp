/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/Layout/LayoutText.hpp"

#include "Include/text/UnicodeBlocks/ArabicPresentationFormsB.hpp"
#include "Include/text/UnicodeBlocks/C0ControlsAndBasicLatin.hpp"
#include "Include/text/UnicodeBlocks/GeneralPunctuation.hpp"

#include "Source/Base/Assert.hpp"
#include "Source/DOM/Text.hpp"
#include "Source/Graphics/Painter.hpp"
#include "Source/Text/Encoding/UTF8Encoder.hpp"
#include "Source/Text/Encoding/UTF8Iterator.hpp"

namespace layout {

    [[nodiscard]] inline constexpr bool
    isWhitespace(char32_t character) noexcept {
        return character == text::SPACE
            || character == text::CHARACTER_TABULATION
            || character == text::LINE_FEED
            || character == text::CARRIAGE_RETURN
            || character == text::ZERO_WIDTH_SPACE
            || character == text::LINE_SEPARATOR
            || character == text::PARAGRAPH_SEPARATOR
            || character == text::ZERO_WIDTH_NO_BREAK_SPACE;
    }

    [[nodiscard]] std::string
    trimString(const std::string &input) noexcept {
        std::string result{};

        bool begin{true};
        bool lastWasWhitespace{false};
        bool encodeStatus = text::encoding::forEachUTF8(input, [&] (char32_t character) {
            if (isWhitespace(character)) {
                if (begin || lastWasWhitespace)
                    return;
                lastWasWhitespace = true;
                text::encoding::appendCodePoint(result, text::SPACE);
                return;
            }

            lastWasWhitespace = false;
            begin = false;
            text::encoding::appendCodePoint(result, character);
        });

        LEMON_ASSERT(encodeStatus);
        static_cast<void>(encodeStatus);

        auto end = std::end(result);
        for (; end != std::begin(result); --end) {
            if (!isWhitespace(static_cast<char32_t>(*end))) {
                break;
            }
        }

        result.resize(static_cast<std::size_t>(std::distance(std::begin(result), end)));
        return result;
    }

    [[nodiscard]] std::vector<std::string_view>
    splitTextIntoWords(std::string_view input) noexcept {
        std::vector<std::string_view> words;
        auto it = std::begin(input);

        while (it != std::end(input)) {
            const std::string_view remainingText{it, std::end(input)};
            if (std::empty(remainingText))
                break;

            auto space = remainingText.find(' ');
            if (space == std::string_view::npos) {
                words.emplace_back(it, std::end(input));
                break;
            }

            words.emplace_back(it, it + space);
            it += space + 1;
        }

        return words;
    }

    LayoutText::LayoutText(LayoutNode *parent, dom::Text *text, css::StyleProperties &&style) noexcept
            : LayoutNode(LayoutNode::Type::TEXT, parent, text, std::move(style)) {
        m_text = trimString(text->data());
    }

    void
    LayoutText::calculate(css::StyleResolver &styleResolver, gfx::Painter *painter, gfx::Size<float> maxSize) noexcept {
        LayoutNode::calculate(styleResolver, painter, maxSize);

        const auto words = splitTextIntoWords(m_text);
        auto currentLineBegin = std::begin(words);

        maxSize.width() -= m_style.border.left.value + m_style.border.right.value + m_style.padding.left.value + m_style.padding.right.value;
        maxSize.height() -= m_style.border.top.value + m_style.border.bottom.value + m_style.padding.top.value + m_style.padding.bottom.value;

        float currentY = position().y() + m_style.border.top.value + m_style.padding.top.value;
        auto beginX = calculateBeginXAtPosition(currentY);

        gfx::Size<float> lastLineSize{0, 0};
        while (currentLineBegin < std::end(words)) {
            auto endIt = currentLineBegin;
            gfx::Size<float> lineSize{0, 0};

            for (; endIt != std::end(words); ++endIt) {
                const std::string_view line{currentLineBegin->begin(), endIt->end()};
                lineSize = painter->calculateTextSize(line, static_cast<gfx::FontSize>(m_style.fontSize.value));
                if (beginX + lineSize.width() > getHorizontalChildSpaceAtPosition(currentY)) {
                    if (endIt != currentLineBegin)
                        --endIt;
                    break;
                }
            }

            const bool isLastSegment = currentLineBegin == endIt || endIt == std::cend(words);
            const auto line = isLastSegment
                            ? std::string_view{currentLineBegin->begin(), std::prev(std::cend(words))->end()}
                            : std::string_view{currentLineBegin->begin(), endIt->end()};

            lastLineSize = lineSize;

            m_segments.push_back(Segment{line, {beginX - position().x(), currentY - position().y()}, lineSize});
            currentY += lineSize.height();
            beginX = calculateBeginXAtPosition(currentY);
            currentLineBegin = std::next(endIt);

            if (m_contentSize.width() < lineSize.width())
                m_contentSize.width() = lineSize.width();

            if (isLastSegment && std::next(currentLineBegin) == std::end(words))
                break;
        }

        if (!std::empty(m_segments)) {
            m_contentSize.height() = m_segments.back().position.y() + lastLineSize.height();
        } else {
            m_contentSize = {0, 0};
        }
    }

    void
    LayoutText::paint(gfx::Painter *painter) noexcept {
        LayoutNode::paint(painter);

        for (const auto &segment : m_segments) {
            const gfx::Position<float> position{segment.position.x() + m_position.x(),
                                                segment.position.y() + m_position.y()};

            if (m_style.textDecorationLine == css::TextDecorationLineType::UNDERLINE) {
                if (m_style.textUnderlinePosition == css::TextUnderlinePosition::AUTO) {
                    painter->paintUnderlineDecorationForText(segment.text, position, m_style.textColor,
                                                             static_cast<gfx::FontSize>(m_style.fontSize.value),
                                                             m_style.textDecorationThickness);
                } else if (m_style.textUnderlinePosition == css::TextUnderlinePosition::UNDER) {
                    painter->paintRect(
                            {position.x(), position.y() + segment.size.height() - (m_style.textDecorationThickness / 2)},
                            {segment.size.width(), m_style.textDecorationThickness}, m_style.textColor);
                }
            }

            painter->paintText(segment.text, position, m_style.textColor,
                               static_cast<gfx::FontSize>(m_style.fontSize.value));
        }
    }

} // namespace layout
