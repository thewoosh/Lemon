/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include "Source/Graphics/Types/Position.hpp"
#include "Source/Layout/LayoutNode.hpp"

namespace layout {

    class LayoutBlock
            : public LayoutNode {
    public:
        [[nodiscard]] inline
        LayoutBlock(LayoutNode *parent, dom::Node *node, css::StyleProperties &&style) noexcept
                : LayoutNode(LayoutNode::Type::BLOCK, parent, node, std::move(style)) {
        }

        void
        calculate(css::StyleResolver &, gfx::Painter *painter, gfx::Size<float> maxSize) noexcept override;

        [[nodiscard]] float
        calculateBeginXAtPosition(float y) const noexcept override;

        [[nodiscard]] float
        getHorizontalChildSpaceAtPosition(float y) const noexcept override;

    private:
        void
        calculateContentSize() noexcept;

        std::vector<LayoutNode *> m_floatingLeft{};
        std::vector<LayoutNode *> m_floatingRight{};
        float m_fallbackWidth{0.0f};
    };

} // namespace layout
