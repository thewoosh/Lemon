/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <memory>
#include <vector>

#include "Source/CSS/StyleProperties.hpp"
#include "Source/Graphics/Types/Position.hpp"
#include "Source/Graphics/Types/Size.hpp"
#include "Source/Layout/HitTestResult.hpp"

namespace css {
    class StyleResolver;
} // namespace css

namespace dom {
    struct Node;
    struct Text;
} // namespace dom

namespace gfx {
    class Painter;
} // namespace gfx

namespace layout {

    class LayoutNode {
        friend class LayoutEngine;

    public:
        enum class Type {
            BLOCK,
            MARKER,
            TEXT
        };

        [[nodiscard]] inline
        LayoutNode(Type type, LayoutNode *parent, dom::Node *node, css::StyleProperties &&style) noexcept
                : m_type(type)
                , m_parent(parent)
                , m_node(node)
                , m_style(std::move(style)) {
        }

        virtual
        ~LayoutNode() noexcept = default;

        virtual void
        calculate(css::StyleResolver &styleResolver, gfx::Painter *painter, gfx::Size<float> maxSize) noexcept;

        [[nodiscard]] inline std::vector<std::unique_ptr<LayoutNode>> &
        children() noexcept {
            return m_children;
        }

        [[nodiscard]] inline const std::vector<std::unique_ptr<LayoutNode>> &
        children() const noexcept {
            return m_children;
        }

        [[nodiscard]] inline constexpr dom::Node *
        dom() const noexcept {
            return m_node;
        }

        [[nodiscard]] virtual float
        calculateBeginXAtPosition(float y) const noexcept;

        [[nodiscard]] virtual float
        getHorizontalChildSpaceAtPosition(float y) const noexcept;

        [[nodiscard]] virtual HitTestResult
        hitTest(gfx::Position<float> mousePosition) noexcept;

        [[nodiscard]] inline constexpr LayoutNode *
        parent() const noexcept {
            return m_parent;
        }

        virtual void
        paint(gfx::Painter *painter) noexcept;

        [[nodiscard]] inline constexpr gfx::Position<float> &
        position() noexcept {
            return m_position;
        }

        [[nodiscard]] inline constexpr gfx::Position<float>
        position() const noexcept {
            return m_position;
        }

        /**
         * Returns the content size of the node. This means that the border and
         * padding aren't calculated for. See visualSize().
         */
        [[nodiscard]] inline constexpr gfx::Size<float> &
        contentSize() noexcept {
            return m_contentSize;
        }

        [[nodiscard]] inline constexpr gfx::Size<float>
        contentSize() const noexcept {
            return m_contentSize;
        }

        [[nodiscard]] inline css::StyleProperties &
        style() noexcept {
            return m_style;
        }

        [[nodiscard]] inline const css::StyleProperties &
        style() const noexcept {
            return m_style;
        }

        [[nodiscard]] inline constexpr Type
        type() const noexcept {
            return m_type;
        }

        [[nodiscard]] inline constexpr gfx::Size<float>
        visualSize() const noexcept {
            return {
                m_style.border.left.value + m_style.padding.left.value
                + m_contentSize.width()
                + m_style.border.right.value + m_style.padding.right.value,
                m_style.border.top.value + m_style.padding.top.value
                + m_contentSize.height()
                + m_style.border.bottom.value + m_style.padding.bottom.value,
            };
        }

    protected:
        void
        calculateHeightGeneric(gfx::Size<float> maxSize) noexcept;

        void
        calculateWidthGeneric(gfx::Size<float> maxSize) noexcept;

        void
        paintBorder(gfx::Painter *painter) noexcept;

        const Type m_type;
        LayoutNode *const m_parent;
        dom::Node *const m_node;

        std::vector<std::unique_ptr<LayoutNode>> m_children{};

        css::StyleProperties m_style{};
        gfx::Position<float> m_position{0, 0};
        gfx::Size<float> m_contentSize{0, 0};
    };

} // namespace layout
