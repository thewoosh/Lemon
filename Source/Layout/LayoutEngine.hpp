/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <memory>

#include "Source/Graphics/Types/Size.hpp"

namespace css {
    class StyleResolver;
} // namespace css

namespace gfx {
    class Painter;
} // namespace gfx

namespace dom {
    struct Document;
    struct Node;
} // namespace dom

namespace layout {

    class LayoutNode;

    class LayoutEngine {
    public:
        [[nodiscard]] explicit
        LayoutEngine(dom::Document *document) noexcept;

        ~LayoutEngine() noexcept;

        [[nodiscard]] inline constexpr layout::LayoutNode *
        bodyLayoutNode() const noexcept {
            return m_bodyLayoutNode;
        }

        void
        doLayout(LayoutNode *rootNode, gfx::Painter *painter, gfx::Size<float> maxSize) noexcept;

        [[nodiscard]] std::unique_ptr<LayoutNode>
        generateFromDocument() noexcept;

    private:
        [[nodiscard]] std::unique_ptr<LayoutNode>
        generateLayout(LayoutNode *parent, dom::Node *node) noexcept;

        dom::Document *const m_document;
        std::unique_ptr<css::StyleResolver> m_styleResolver;

        layout::LayoutNode *m_bodyLayoutNode{nullptr};
    };

} // namespace layout
