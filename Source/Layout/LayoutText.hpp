/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include "Source/Graphics/Types/Position.hpp"
#include "Source/Layout/LayoutNode.hpp"

namespace layout {

    class LayoutText
            : public LayoutNode {
    public:
        [[nodiscard]] explicit
        LayoutText(LayoutNode *parent, dom::Text *node, css::StyleProperties &&style) noexcept;

        void
        calculate(css::StyleResolver &, gfx::Painter *painter, gfx::Size<float> maxSize) noexcept override;

        void
        paint(gfx::Painter *painter) noexcept override;

        [[nodiscard]] inline const std::string &
        text() const noexcept {
            return m_text;
        }

    private:
        struct Segment {
            std::string_view text;
            gfx::Position<float> position;
            gfx::Size<float> size;
        };

        std::string m_text;
        std::vector<Segment> m_segments;
    };

} // namespace layout
