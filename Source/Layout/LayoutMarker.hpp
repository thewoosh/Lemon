/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include "Source/Layout/LayoutNode.hpp"

namespace layout {

    class LayoutMarker final
            : public LayoutNode {
    public:
        [[nodiscard]]
        LayoutMarker(LayoutNode *parent, css::StyleProperties &&style) noexcept;

        void
        paint(gfx::Painter *painter) noexcept override;
    };

} // namespace layout
