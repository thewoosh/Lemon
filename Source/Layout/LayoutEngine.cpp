/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/Layout/LayoutEngine.hpp"

#include "Source/Base/Assert.hpp"
#include "Source/CSS/StyleResolver.hpp"
#include "Source/DOM/Document.hpp"
#include "Source/DOM/Element.hpp"
#include "Source/DOM/Node.hpp"
#include "Source/DOM/Text.hpp"
#include "Source/Infra/Namespaces.hpp"
#include "Source/Layout/LayoutBlock.hpp"
#include "Source/Layout/LayoutNode.hpp"
#include "Source/Layout/LayoutMarker.hpp"
#include "Source/Layout/LayoutText.hpp"

namespace layout {

    LayoutEngine::LayoutEngine(dom::Document *document) noexcept
            : m_document(document)
            , m_styleResolver(std::make_unique<css::StyleResolver>(document)) {
    }

    LayoutEngine::~LayoutEngine() noexcept = default;

    void
    collectInheritedProperties(css::StyleProperties &style,
                               const LayoutNode *const parent) noexcept {
        style.fontSize = {1.0f, css::VLVUnit::EM};
        style.listStyleType = parent->style().listStyleType;
        style.textColor = parent->style().textColor;
        style.textDecorationLine = parent->style().textDecorationLine;

        style.inheritedFontSize = parent->style().fontSize;
    }

    std::unique_ptr<LayoutNode>
    LayoutEngine::generateLayout(LayoutNode *parent, dom::Node *node) noexcept {
        std::unique_ptr<LayoutNode> layoutNode{};

        css::StyleProperties style{};
        if (parent)
            collectInheritedProperties(style, parent);

        if (node->nodeType() == dom::NodeType::ELEMENT)
            m_styleResolver->collectStyleProperties(style, static_cast<dom::Element *>(node));

        if (style.display == css::DisplayType::NONE)
            return nullptr;

        switch (node->nodeType()) {
            case dom::NodeType::COMMENT:
            case dom::NodeType::DOCUMENT_TYPE:
                return nullptr;
            case dom::NodeType::DOCUMENT:
                layoutNode = std::make_unique<LayoutBlock>(parent, node, std::forward<css::StyleProperties>(style));
                break;
            case dom::NodeType::ELEMENT: {
                const auto *element = static_cast<const dom::Element *>(node);

                if (style.display == css::DisplayType::LIST_ITEM) {
                    parent->children().push_back(std::make_unique<LayoutMarker>(parent, std::forward<css::StyleProperties>(style)));
                }

                layoutNode = std::make_unique<LayoutBlock>(parent, node, std::forward<css::StyleProperties>(style));

                if (element->localName() == "body" && element->namespaceURI() == infra::namespaces::HTML) {
                    m_bodyLayoutNode = layoutNode.get();
                }
                break;
            }
            case dom::NodeType::TEXT:
                layoutNode = std::make_unique<LayoutText>(parent, static_cast<dom::Text *>(node),
                                                          std::forward<css::StyleProperties>(style));
                break;
            default:
                LEMON_ASSERT_NOT_REACHED_OR_RETURN(nullptr);
        }

        for (auto &domChild : node->children()) {
            auto childLayout = generateLayout(layoutNode.get(), domChild.get());
            if (childLayout) {
                layoutNode->children().push_back(std::move(childLayout));
            }
        }

        return layoutNode;
    }

    void
    LayoutEngine::doLayout(LayoutNode *rootNode, gfx::Painter *painter, gfx::Size<float> maxSize) noexcept {
        rootNode->calculate(*m_styleResolver, painter, maxSize);
    }

    std::unique_ptr<LayoutNode>
    LayoutEngine::generateFromDocument() noexcept {
        return generateLayout(nullptr, m_document);
    }

} // namespace layout
