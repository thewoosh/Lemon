/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/Layout/LayoutBlock.hpp"

#include "Source/Base/Assert.hpp"
#include "Source/DOM/Element.hpp"
#include "Source/Graphics/Painter.hpp"

namespace layout {

    [[nodiscard]] inline constexpr float
    calculateTotalWidth(const LayoutNode *node) noexcept {
        if (node->contentSize().width() == 0.0f)
            return 0.0f;

        return node->style().border.top.value
               + node->style().padding.top.value
               + node->contentSize().width()
               + node->style().padding.bottom.value
               + node->style().border.bottom.value;
    }

    [[nodiscard]] inline constexpr float
    calculateTotalHorizontalSpace(const LayoutNode *node) noexcept {
        if (node->contentSize().width() == 0.0f)
            return 0.0f;

        return calculateTotalWidth(node) + node->style().margin.left.value + node->style().margin.right.value;
    }

    [[nodiscard]] inline constexpr float
    calculateTotalHeight(const LayoutNode *node) noexcept {
        if (node->contentSize().height() == 0.0f)
            return 0.0f;

        return node->style().border.left.value
               + node->style().padding.left.value
               + node->contentSize().height()
               + node->style().padding.right.value
               + node->style().border.right.value;
    }

    [[nodiscard]] inline constexpr float
    calculateTotalVerticalSpace(const LayoutNode *node) noexcept {
        if (node->contentSize().height() == 0.0f)
            return 0.0f;

        return calculateTotalHeight(node) + node->style().margin.top.value + node->style().margin.bottom.value;
    }

    [[nodiscard]] inline constexpr gfx::Size<float>
    calculateTotalSize(const LayoutNode *node) noexcept {
        return {calculateTotalWidth(node), calculateTotalHeight(node)};
    }

    static void
    applyDisplacement(LayoutNode *node, gfx::Position<float> position) noexcept {
        node->position().x() += position.x();
        node->position().y() += position.y();

        for (auto &child : node->children()) {
            applyDisplacement(child.get(), position);
        }
    }

    void
    LayoutBlock::calculate(css::StyleResolver &styleResolver, gfx::Painter *painter, gfx::Size<float> maxSize) noexcept {
        m_fallbackWidth = maxSize.width();
        LayoutNode::calculate(styleResolver, painter, maxSize);

        LEMON_ASSERT(maxSize.width() > 0);

        auto currentPosition = position();
        currentPosition.y() += m_style.border.top.value + m_style.padding.top.value;
        currentPosition.x() = calculateBeginXAtPosition(currentPosition.y());

        float lineHeight = 0;

        maxSize.width() -= m_style.border.left.value + m_style.padding.left.value + m_style.margin.left.value
                - m_style.border.right.value + m_style.padding.right.value + m_style.margin.right.value;
        maxSize.height() -= m_style.border.top.value + m_style.padding.top.value + m_style.margin.top.value
                - m_style.border.bottom.value + m_style.padding.bottom.value + m_style.margin.bottom.value;

        //
        // Floating boxes stage
        //
        for (const auto &child : children()) {
            if (child->style().floatType == css::FloatType::NONE)
                continue;

            LEMON_ASSERT(currentPosition.x() >= 0.0f);

            child->position() = currentPosition;
            child->calculate(styleResolver, painter, maxSize);

            float xDisplacement = 0.0f;

            if (child->style().floatType == css::FloatType::LEFT) {
                if (!m_floatingLeft.empty()) {
                    const auto &neighbor = m_floatingLeft.back();
                    xDisplacement = neighbor->position().x() + calculateTotalHorizontalSpace(neighbor) - child->position().x();
                }

                m_floatingLeft.push_back(child.get());
            } else if (child->style().floatType == css::FloatType::RIGHT) {
                if (!m_floatingRight.empty()) {
                    const auto &neighbor = m_floatingRight.back();
                    xDisplacement = neighbor->position().x() - calculateTotalHorizontalSpace(child.get());
                } else {
                    xDisplacement = maxSize.width() - calculateTotalHorizontalSpace(child.get());
                }
                m_floatingRight.push_back(child.get());
            }

            if (xDisplacement != 0.0f) {
                applyDisplacement(child.get(), {xDisplacement, 0.0f});
            }
        }

        //
        // Non-floating boxes stage
        //
        currentPosition.x() = calculateBeginXAtPosition(currentPosition.y());

        for (const auto &child : children()) {
            if (child->style().display == css::DisplayType::NONE)
                continue;
            if (child->style().floatType != css::FloatType::NONE)
                continue;

            if (child->style().display == css::DisplayType::BLOCK) {
                // display: block;
                currentPosition.y() += lineHeight;
                currentPosition.x() = calculateBeginXAtPosition(currentPosition.y());
                lineHeight = 0;
            }
            // else => display: inline

            child->position() = currentPosition;

            child->calculate(styleResolver, painter, {getHorizontalChildSpaceAtPosition(currentPosition.y()), maxSize.height()});

            auto childSize = calculateTotalSize(child.get());
            childSize.width() += child->style().margin.right.value;
            childSize.height() += child->style().margin.bottom.value;

            currentPosition.x() += childSize.width();

            const auto childLineHeight = child->position().y() + childSize.height() - currentPosition.y();
            if (lineHeight < childLineHeight)
                lineHeight = childLineHeight;
        }

        calculateContentSize();

        // Specific to Document nodes
        if (dom()->nodeType() == dom::NodeType::DOCUMENT) {
            m_contentSize.width() = std::max(m_contentSize.width(), maxSize.width());
            m_contentSize.height() = std::max(m_contentSize.height(), maxSize.height());
        }
    }

    float
    LayoutBlock::calculateBeginXAtPosition(float y) const noexcept {
        auto maxX = position().x() + m_style.border.left.value + m_style.padding.left.value;

        for (const auto *left : m_floatingLeft) {
            if (left->position().y() > y || left->position().y() + calculateTotalVerticalSpace(left) < y)
                continue;

            const auto x = left->position().x() + calculateTotalHorizontalSpace(left);
            if (maxX < x)
                maxX = x;
        }

        return maxX;
    }

    void
    LayoutBlock::calculateContentSize() noexcept {
        if (!style().heightIsAuto && !style().widthIsAuto)
            return;

        const auto contentStartX = position().x() + style().margin.left.value + style().border.left.value
                                 + style().padding.left.value;
        const auto contentStartY = position().y() + style().margin.top.value + style().border.top.value
                                 + style().padding.top.value;

        for (const auto &child : children()) {
            if (style().widthIsAuto) {
                auto width = calculateTotalHorizontalSpace(child.get());
                width += child->position().x() - contentStartX;

                if (m_contentSize.width() < width)
                    m_contentSize.width() = width;
            }

            if (style().heightIsAuto) {
                auto height = calculateTotalVerticalSpace(child.get());
                height += child->position().y() - contentStartY;

                if (style().heightIsAuto && m_contentSize.height() < height)
                    m_contentSize.height() = height;
            }
        }
    }

    float
    LayoutBlock::getHorizontalChildSpaceAtPosition(float y) const noexcept {
        auto parentWidth = m_fallbackWidth;
        if (parent())
            parentWidth = parent()->getHorizontalChildSpaceAtPosition(y);
        parentWidth -= m_style.border.left.value + m_style.padding.left.value + m_style.margin.left.value;
        parentWidth -= m_style.border.right.value + m_style.padding.right.value + m_style.margin.right.value;

        for (const auto *right : m_floatingRight) {
            if (right->position().y() > y || right->position().y() + calculateTotalVerticalSpace(right) < y)
                continue;
            const auto width = right->position().x() - position().x();
            if (parentWidth > width)
                parentWidth = width;
        }

        return parentWidth;
    }

} // namespace layout
