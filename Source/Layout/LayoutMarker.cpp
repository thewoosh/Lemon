/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#include "Source/Layout/LayoutMarker.hpp"

#include "Source/Graphics/Painter.hpp"
#include "Source/Text/Encoding/UTF8Encoder.hpp"

namespace layout {

    LayoutMarker::LayoutMarker(LayoutNode *parent, css::StyleProperties &&style) noexcept
            : LayoutNode(LayoutNode::Type::MARKER, parent, nullptr, std::move(style)) {
    }

    void
    LayoutMarker::paint(gfx::Painter *painter) noexcept {
        std::string_view str{"*"};

        auto size = painter->calculateTextSize(str, static_cast<gfx::FontSize>(m_style.fontSize.value));
        size.height() /= 4;
        size.width() = size.height();

        auto pos = position();
        pos.x() -= size.width() * 2;
        pos.y() += size.height() / 2;

//        painter->paintRect(pos, size, m_style.textColor);
        painter->paintText(str, pos, m_style.textColor, static_cast<gfx::FontSize>(m_style.fontSize.value));
    }

} // namespace layout
