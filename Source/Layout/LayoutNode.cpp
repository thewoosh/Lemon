/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/Layout/LayoutNode.hpp"

#include "Source/Base/Assert.hpp"
#include "Source/CSS/StyleResolver.hpp"
#include "Source/DOM/Element.hpp"
#include "Source/DOM/Text.hpp"
#include "Source/Graphics/Painter.hpp"

namespace layout {

    inline constexpr void
    resolveVariadicLengthValue(css::VariadicLengthValue<float> &vlv, float fontSize, gfx::Size<float> maxSize) {
        if (vlv.unit == css::VLVUnit::AUTO)
            return;

        if (vlv.unit == css::VLVUnit::PERCENTAGE) {
            vlv.value = vlv.value / 100.0f * maxSize.width();
        } else if (vlv.unit == css::VLVUnit::EM) {
            vlv.value = vlv.value * fontSize;
        } else if (vlv.unit != css::VLVUnit::PIXELS) {
            LOG_ERROR("Unknown vlv unit: {} {}", vlv.value, vlv.unit);
            LEMON_ASSERT_NOT_REACHED_OR_RETURN_VOID();
        }

        vlv.unit = css::VLVUnit::PIXELS;
    }

    inline constexpr void
    resolveVariadicLengthValue(css::Edge<css::VariadicLengthValue<float>> &edge, float fontSize,
                              gfx::Size<float> maxSize) {
        resolveVariadicLengthValue(edge.top, fontSize, maxSize);
        resolveVariadicLengthValue(edge.bottom, fontSize, maxSize);
        resolveVariadicLengthValue(edge.left, fontSize, maxSize);
        resolveVariadicLengthValue(edge.right, fontSize, maxSize);
    }

    float
    resolveFontSize(LayoutNode *node, gfx::Size<float> maxSize) noexcept {
        if (node->style().fontSize.unit == css::VLVUnit::PIXELS)
            return node->style().fontSize.value;

        LEMON_ASSERT(node->parent());

        node->style().inheritedFontSize.value = resolveFontSize(node->parent(), maxSize);
        node->style().inheritedFontSize.unit = css::VLVUnit::PIXELS;

        resolveVariadicLengthValue(node->style().fontSize, node->style().inheritedFontSize.value, maxSize);
        return node->style().fontSize.value;
    }

    void
    LayoutNode::calculate(css::StyleResolver &, gfx::Painter *, gfx::Size<float> maxSize) noexcept {
        // Remember: font-size first to ensure font-relative units are correct.
        if (parent()) {
            resolveFontSize(this, maxSize);
        }

        resolveVariadicLengthValue(m_style.margin, m_style.fontSize.value, maxSize);
        resolveVariadicLengthValue(m_style.padding, m_style.fontSize.value, maxSize);
        resolveVariadicLengthValue(m_style.border, m_style.fontSize.value, maxSize);

        position().x() += m_style.margin.left.value;
        position().y() += m_style.margin.top.value;

        m_style.heightIsAuto = m_style.height.unit == css::VLVUnit::AUTO;
        m_style.widthIsAuto = m_style.width.unit == css::VLVUnit::AUTO;

        resolveVariadicLengthValue(m_style.height, m_style.fontSize.value, maxSize);
        resolveVariadicLengthValue(m_style.width, m_style.fontSize.value, maxSize);

        if (m_style.floatType != css::FloatType::NONE) {
            // TODO
            LOG_WARNING("TODO: width generation for float: {}", m_style.floatType);
            calculateHeightGeneric(maxSize);
            calculateWidthGeneric(maxSize);
        } else if (isInlineLevelDisplay(m_style.display)) {
            // TODO
            LOG_WARNING("TODO: width generation for display: {}", m_style.display);
        } else if (m_style.display == css::DisplayType::BLOCK) {
            calculateHeightGeneric(maxSize);
            calculateWidthGeneric(maxSize);
        }
    }

    float
    LayoutNode::calculateBeginXAtPosition(float y) const noexcept {
        float beginX = m_position.x();
        if (parent())
            beginX = parent()->calculateBeginXAtPosition(y) + m_style.margin.left.value;
        return beginX + m_style.border.left.value + m_style.padding.left.value;
    }

    void
    LayoutNode::calculateHeightGeneric(gfx::Size<float> maxSize) noexcept {
        if (m_style.heightIsAuto) {
            m_contentSize.height() = maxSize.height() - m_style.margin.top.value - m_style.margin.bottom.value
                                     - m_style.border.top.value - m_style.border.bottom.value
                                     - m_style.padding.top.value - m_style.padding.bottom.value;
        } else {
            m_contentSize.height() = m_style.height.value;
        }
    }

    void
    LayoutNode::calculateWidthGeneric(gfx::Size<float> maxSize) noexcept {
        if (m_style.widthIsAuto) {
            m_contentSize.width() = maxSize.width() - m_style.margin.left.value - m_style.margin.right.value
                                    - m_style.border.left.value - m_style.border.right.value
                                    - m_style.padding.left.value - m_style.padding.right.value;
        } else {
            m_contentSize.width() = m_style.width.value;
        }
    }

    float
    LayoutNode::getHorizontalChildSpaceAtPosition(float y) const noexcept {
        if (parent())
            return parent()->getHorizontalChildSpaceAtPosition(y);
        return m_contentSize.width();
    }

    HitTestResult
    LayoutNode::hitTest(const gfx::Position<float> mousePosition) noexcept {
        for (auto &child : m_children) {
            if (auto result = child->hitTest(mousePosition); result.layoutNode != nullptr)
                return result;
        }

        const gfx::Size<float> contentSize{
                m_style.border.left.value + m_style.padding.left.value +
                m_contentSize.width() +
                m_style.border.right.value + m_style.padding.right.value,
                m_style.border.top.value + m_style.padding.top.value +
                m_contentSize.height() +
                m_style.border.bottom.value + m_style.padding.bottom.value
        };

        if (mousePosition.x() >= m_position.x() && mousePosition.x() <= m_position.x() + contentSize.width()) {
            if (mousePosition.y() >= m_position.y() && mousePosition.y() <= m_position.y() + contentSize.height()) {
                return {this};
            }
        }

        return {};
    }

    void
    LayoutNode::paint(gfx::Painter *painter) noexcept {
        const gfx::Position<float> backgroundPosition{
                m_position.x() + m_style.border.left.value,
                m_position.y() + m_style.border.top.value,
        };

        const gfx::Size<float> backgroundSize{
                m_style.padding.left.value + m_contentSize.width() + m_style.padding.right.value,
                m_style.padding.top.value + m_contentSize.height() + m_style.padding.bottom.value
        };

        if (m_style.backgroundColor.a() != 0.0f && m_contentSize.width() != 0.0f && m_contentSize.height() != 0.0f)
            painter->paintRect(backgroundPosition, backgroundSize, m_style.backgroundColor);

        paintBorder(painter);

        for (const auto &child : m_children) {
            child->paint(painter);
        }
    }

    void
    LayoutNode::paintBorder(gfx::Painter *painter) noexcept {
        // TODO support the triangle corners of borders of different colors.

        const auto widthForHorizontal = m_style.border.left.value + m_style.padding.left.value
                                        + m_contentSize.width()
                                        + m_style.border.right.value + m_style.padding.right.value;

        const auto heightForVertical = m_style.border.top.value + m_style.padding.top.value
                                       + m_contentSize.height()
                                       + m_style.border.bottom.value + m_style.padding.bottom.value;

        // top
        painter->paintRect(m_position, {widthForHorizontal, m_style.border.top.value}, m_style.borderColor.top);

        // bottom
        const auto bottomY = m_position.y() + m_style.border.top.value + m_style.padding.top.value + m_contentSize.height()
                             + m_style.padding.bottom.value;
        painter->paintRect({m_position.x(), bottomY}, {widthForHorizontal, m_style.border.bottom.value},
                           m_style.borderColor.bottom);

        // left
        painter->paintRect(m_position, {m_style.border.left.value, heightForVertical}, m_style.borderColor.left);

        // right
        const auto rightX = m_position.x() + m_style.border.left.value + m_style.padding.left.value + m_contentSize.width()
                            + m_style.padding.right.value;
        painter->paintRect({rightX, m_position.y()}, {m_style.border.right.value, heightForVertical},
                           m_style.borderColor.right);
    }

} // namespace layout
