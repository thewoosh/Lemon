/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/Text/Strings.hpp"

#include "Include/text/Category.hpp"

#include "Source/Text/Encoding/UTF8Encoder.hpp"
#include "Source/Text/Encoding/UTF8Iterator.hpp"

namespace text {

    // TODO this algorithm should be optimized
    std::string
    trim(std::string_view sv) noexcept {
        std::u32string intermediate;
        intermediate.reserve(sv.length());

        bool startFlag{true};
        bool status = encoding::forEachUTF8(sv, [&] (char32_t character) {
            if (isWhitespace(character)) {
                if (startFlag)
                    return;
            } else {
                startFlag = false;
            }

            intermediate += character;
        });

        static_cast<void>(status);

        for (auto it = std::crbegin(intermediate); it != std::crend(intermediate); ++it) {
            if (!isWhitespace(*it))
                break;
            intermediate.pop_back();
        }

        std::string result;
        result.reserve(intermediate.length());
        for (char32_t character : intermediate) {
            text::encoding::appendCodePoint(result, character);
        }

        return result;
    }

} // namespace text
