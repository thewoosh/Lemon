/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/Text/Encoding/UTF8Encoder.hpp"

#include <cstdint>

#include "Include/text/UnicodeBlocks/C0ControlsAndBasicLatin.hpp"

#include "Source/Base/Assert.hpp"

namespace text::encoding {

    template <typename CharacterType=char, typename IOQueueFunction>
    inline constexpr void
    runUTF8EncoderHandler(char32_t codePoint, IOQueueFunction queueFunction) noexcept {
        if (codePoint <= text::DELETE) {
            queueFunction(static_cast<CharacterType>(codePoint));
            return;
        }

        std::uint8_t count, offset;
        if (codePoint <= 0x07FF) {
            count = 1;
            offset = 0xC0;
        } else if (codePoint <= 0xFFFF) {
            count = 2;
            offset = 0xE0;
        } else if (codePoint <= 0x10FFFF) {
            count = 3;
            offset = 0xF0;
        } else {
            LEMON_ASSERT_NOT_REACHED_OR_RETURN_VOID();
        }

        queueFunction(static_cast<CharacterType>((codePoint >> (6 * count)) + offset));
        for (; count > 0; --count) {
            const auto temp = codePoint >> (6 * (count - 1));
            queueFunction(static_cast<CharacterType>(0x80 | (temp & 0x3F)));
        }
    }

    void
    appendCodePoint(std::string &str, char32_t codePoint) noexcept {
        if (codePoint > 0x80) {
            if (codePoint < 0x07FF) {
                str.reserve(std::max(std::size(str) + 2, str.capacity()));
            } else if (codePoint < 0xFFFF) {
                str.reserve(std::max(std::size(str) + 3, str.capacity()));
            } else if (codePoint < 0x10FFFF) {
                str.reserve(std::max(std::size(str) + 4, str.capacity()));
            } else {
                return;
            }
        }

        runUTF8EncoderHandler(codePoint, [&str] (char byte) {
            str += byte;
        });
    }

} // namespace text::encoding
