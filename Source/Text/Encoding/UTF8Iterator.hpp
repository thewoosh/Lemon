/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string_view>

namespace text::encoding {

    template <typename Function>
    [[nodiscard]] inline constexpr bool
    forEachUTF8(std::string_view input, Function function) noexcept {
        char32_t codePoint{0};

        std::uint8_t bytesSeen{0};
        std::uint8_t bytesNeeded{0};

        std::uint8_t lowerBoundary{0x80};
        std::uint8_t upperBoundary{0xBF};

        for (char element : input) {
            const auto byte = static_cast<std::uint8_t>(element);
            if (bytesNeeded == 0) {
                if (byte <= 0x7F) {
                    function(static_cast<char32_t>(byte));
                    continue;
                }

                if (byte >= 0xC2 && byte <= 0xDF) {
                    bytesNeeded = 1;
                    codePoint = static_cast<char32_t>(byte & 0x1F);
                    continue;
                }

                if (byte >= 0xE0 && byte <= 0xEF) {
                    bytesNeeded = 2;
                    codePoint = static_cast<char32_t>(byte & 0xF);
                    continue;
                }

                if (byte >= 0xF0 && byte <= 0xF4) {
                    bytesNeeded = 3;
                    codePoint = static_cast<char32_t>(byte & 0x7);
                    continue;
                }

                //LOG_ERROR("incorrect first byte of UTF-8 sequence");
                return false;
            }

            if (byte < lowerBoundary || byte > upperBoundary) {
                //LOG_ERROR("byte outside boundaries");
                return false;
            }

            lowerBoundary = 0x80;
            upperBoundary = 0xBF;
            codePoint = (codePoint << 6) | static_cast<char32_t>(byte & 0x3F);

            ++bytesSeen;
            if (bytesSeen != bytesNeeded)
                continue;

            const auto theCodePoint = codePoint;
            codePoint = bytesNeeded = bytesSeen = 0;
            function(theCodePoint);
        }

        if (bytesNeeded != 0) {
            //LOG_ERROR("bytesNeeded != 0 on EOS");
            return false;
        }

        return true;
    }


} // namespace text::encoding
