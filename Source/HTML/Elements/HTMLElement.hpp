/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string>
#include <utility>

#include "Source/DOM/Element.hpp"
#include "Source/Infra/Namespaces.hpp"

namespace html {

    struct HTMLElement
        : public dom::Element {

        [[nodiscard]] inline
        HTMLElement(Node *parent, std::string &&localName) noexcept
                : dom::Element(parent, std::move(localName)) {
            // https://html.spec.whatwg.org/multipage/dom.html#html-element-constructors:html-namespace-2
            m_namespaceURI = infra::namespaces::HTML;
        }

    };

} // namespace html
