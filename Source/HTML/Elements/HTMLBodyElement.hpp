/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string>
#include <utility>

#include "Source/HTML/Elements/HTMLHtmlElement.hpp"

namespace html {

    /**
     * https://html.spec.whatwg.org/multipage/sections.html#htmlbodyelement
     */
    struct HTMLBodyElement
            : public HTMLElement {

        [[nodiscard]] inline
        HTMLBodyElement(Node *parent, std::string &&localName) noexcept
                : HTMLElement(parent, std::move(localName)) {
        }

    };

} // namespace html
