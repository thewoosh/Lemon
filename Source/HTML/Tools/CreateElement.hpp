/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <memory>
#include <optional>
#include <string>
#include <string_view>

namespace dom {
    struct Document;
    struct Element;
    struct Node;
} // namespace dom

namespace html {

    /**
     * https://dom.spec.whatwg.org/#concept-create-element
     * https://html.spec.whatwg.org/#element-interfaces
     */
    [[nodiscard]] std::unique_ptr<dom::Element>
    createElement(dom::Document *, dom::Node *, std::string &&localName) noexcept;

} // namespace html
