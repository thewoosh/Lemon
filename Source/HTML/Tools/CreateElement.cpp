/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/HTML/Tools/CreateElement.hpp"

#include "Source/DOM/Element.hpp"
#include "Source/HTML/Elements/HTMLElement.hpp"
#include "Source/HTML/Elements/HTMLBodyElement.hpp"
#include "Source/HTML/Elements/HTMLFormElement.hpp"
#include "Source/HTML/Elements/HTMLHeadElement.hpp"
#include "Source/HTML/Elements/HTMLHtmlElement.hpp"
#include "Source/HTML/Elements/HTMLStyleElement.hpp"

namespace html {

    /**
     * https://dom.spec.whatwg.org/#concept-create-element
     * https://html.spec.whatwg.org/#element-interfaces
     */
    std::unique_ptr<dom::Element>
    createElement(dom::Document *, dom::Node *parent, std::string &&localName) noexcept {
        if (localName == "body") {
            return std::make_unique<HTMLBodyElement>(parent, std::move(localName));
        }

        if (localName == "form") {
            return std::make_unique<HTMLFormElement>(parent, std::move(localName));
        }

        if (localName == "head") {
            return std::make_unique<HTMLHeadElement>(parent, std::move(localName));
        }

        if (localName == "html") {
            return std::make_unique<HTMLHtmlElement>(parent, std::move(localName));
        }

        if (localName == "style") {
            return std::make_unique<HTMLStyleElement>(parent, std::move(localName));
        }

        return std::make_unique<HTMLElement>(parent, std::move(localName));
    }

} // namespace html
