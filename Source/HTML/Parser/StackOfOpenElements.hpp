/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <algorithm>
#include <vector>

#include "Source/DOM/Element.hpp"
#include "Source/Infra/Namespaces.hpp"

namespace html {

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#the-stack-of-open-elements
     */
    class StackOfOpenElements {
    public:
        [[nodiscard]] inline dom::Element *
        bottommost() const noexcept {
            return m_data.back();
        }

        [[nodiscard]] inline std::vector<dom::Element *> &
        data() noexcept {
            return m_data;
        }

        [[nodiscard]] inline const std::vector<dom::Element *> &
        data() const noexcept {
            return m_data;
        }

        [[nodiscard]] inline bool
        hasInHTMLElementInScope(std::string_view name) const noexcept {
            return std::any_of(std::cbegin(m_data), std::cend(m_data), [name](const auto *element) {
                return element->localName() == name && element->namespaceURI() == infra::namespaces::HTML;
            });
        }

        [[nodiscard]] inline bool
        hasElementInScope(dom::Element *element) const noexcept {
            return std::any_of(std::cbegin(m_data), std::cend(m_data), [element](const auto *dataElement) {
                return dataElement == element;
            });
        }

        template <typename...Args>
        [[nodiscard]] inline bool
        hasOneOfHTMLElementInScope(Args &&...args) const noexcept {
            return std::any_of(std::cbegin(m_data), std::cend(m_data), [&](const auto *element) {
                if (element->namespaceURI() != infra::namespaces::HTML)
                    return false;
                bool array[]{ (element->localName() == args)... };
                return std::find(std::cbegin(array), std::cend(array), true) != std::cend(array);
            });
        }

        [[nodiscard]] inline bool
        hasTemplateElement() const noexcept {
            return hasInHTMLElementInScope("template");
        }

        inline dom::Element *
        popOffCurrentNode() noexcept {
            auto element = m_data.back();
            m_data.pop_back();
            return element;
        }

        [[nodiscard]] inline bool
        popUntilElement(dom::Element *element) noexcept {
            while (!std::empty(m_data)) {
                bool flag = m_data.back() == element;
                m_data.pop_back();
                if (flag)
                    return true;
            }

            return false;
        }

        inline void
        push(dom::Element *element) noexcept {
            m_data.push_back(element);
        }

        [[nodiscard]] inline dom::Element *
        topmost() const noexcept {
            return m_data.front();
        }

    private:
        std::vector<dom::Element *> m_data;
    };

} // namespace html
