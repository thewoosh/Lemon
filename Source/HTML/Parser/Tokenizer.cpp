/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/HTML/Parser/Tokenizer.hpp"

#include <algorithm>

#include "Include/text/Tools.hpp"
#include "Include/text/UnicodeBlocks/C0ControlsAndBasicLatin.hpp"
#include "Include/text/UnicodeBlocks/Specials.hpp"

#include "Source/Base/Assert.hpp"
#include "Source/Base/Logger.hpp"
#include "Source/Text/Encoding/UTF8Encoder.hpp"

#define PARSE_ERROR(name) \
    do { \
        LOG_ERROR("Parse Error: {}", toString(TokenizerParseError::name)); \
        const auto pos = static_cast<std::string_view::size_type>(std::distance(std::cbegin(m_data), m_it)); \
        const auto begin = m_data.rfind(U'\n', pos) + 1; \
        const auto end = m_data.substr(pos).find(U'\n'); \
        const auto line = m_data.substr(begin, end); \
        std::string lineString{}; \
        for (char32_t codePoint : line) { \
            text::encoding::appendCodePoint(lineString, codePoint); \
        } \
        LOG_ERROR("{}", lineString); \
        LOG_ERROR("{}^", std::string(pos - begin - 1, '~')); \
    } while (0)

/**
 * Requires that b is a string of non-ASCII-uppercase letters!
 */
[[nodiscard]] inline constexpr bool
startsWith(std::u32string_view a, std::u32string_view b) noexcept {
    if (a.length() < b.length())
        return false;
    return std::equal(std::begin(a), std::begin(a) + b.length(), std::begin(b), std::end(b),
                      [] (char32_t characterA, char32_t characterB) {
                          return text::toASCIILowercase(characterA) == characterB;
                      }
    );
}

namespace html {

#define DEFINE_STATE(name) \
    void \
    Tokenizer::stateImplementation##name() noexcept

    bool
    Tokenizer::isCurrentTokenAnAppropriateEndTagToken() const noexcept {
        LEMON_ASSERT(m_currentToken.has_value());
        LEMON_ASSERT(m_currentToken->type() == Token::Type::END_TAG);

        if (!m_appropriateEndTagName.has_value())
            return false;

        return m_appropriateEndTagName.value() == m_currentToken->asEndTag().tagName;
    }

    char32_t
    Tokenizer::consumeNextInputCharacter() noexcept {
        if (m_it == std::cend(m_data)) {
            ++m_it;
            return constants::endOfFile;
        }

        m_currentInputCharacter = *m_it;
        ++m_it;
        m_nextInputCharacter = *m_it;
        return m_currentInputCharacter;
    }

    void
    Tokenizer::emitCurrentToken() noexcept {
        LEMON_ASSERT(m_currentToken.has_value());
        emitToken(std::move(m_currentToken.value()));
        m_currentToken.reset();
    }

    void
    Tokenizer::emitToken(Token &&token) noexcept {
        if (m_verbose)
            LOG_TRACE("Token emitted: {}", token.toString());
        if (m_tokenEmissionCallback) {
            const auto success = m_tokenEmissionCallback(std::move(token));
            LEMON_ASSERT(success);

            static_cast<void>(success);
            // TODO add abort flag
        }
    }

    void
    Tokenizer::invokeState(TokenizerState tokenizerState) noexcept {
        switch (tokenizerState) {
#define LEMON_HTML_INVOKE_STATE_ITERATOR(name) \
            case TokenizerState::name: \
                stateImplementation##name(); \
                break;
            LEMON_HTML_ITERATE_TOKENIZER_STATES(LEMON_HTML_INVOKE_STATE_ITERATOR)
            default:
                LEMON_ASSERT_NOT_REACHED_OR_RETURN_VOID();
        }
    }

    void
    Tokenizer::reconsumeIn(TokenizerState tokenizerState) noexcept {
        --m_it;
        m_currentInputCharacter = *(m_it - 1);
        m_nextInputCharacter = *m_it;

        switchTo(tokenizerState);
        invokeState(tokenizerState);
    }

    bool
    Tokenizer::run() noexcept {
        while (m_it <= std::cend(m_data)) {
            invokeState(m_state);
        }

        return true;
    }

    void
    Tokenizer::setReturnState(TokenizerState tokenizerState) noexcept {
        LEMON_ASSERT(!m_returnState.has_value());
        m_returnState = tokenizerState;
    }

    void
    Tokenizer::switchTo(TokenizerState tokenizerState) noexcept {
        m_state = tokenizerState;
    }

    DEFINE_STATE(DATA) {
        using namespace text;
        switch (consumeNextInputCharacter()) {
            case AMPERSAND:
                setReturnState(TokenizerState::DATA);
                switchTo(TokenizerState::CHARACTER_REFERENCE);
                break;
            case LESS_THAN_SIGN:
                switchTo(TokenizerState::TAG_OPEN);
                break;
            case NULL_CHARACTER:
                PARSE_ERROR(UNEXPECTED_NULL_CHARACTER);
                break;
            case constants::endOfFile:
                emitToken(Token::createEOF());
                break;
            default:
                emitToken(Token::createCharacter(m_currentInputCharacter));
                break;
        }
    }

    DEFINE_STATE(RCDATA) {
        using namespace text;
        switch (consumeNextInputCharacter()) {
            case AMPERSAND:
                setReturnState(TokenizerState::RCDATA);
                switchTo(TokenizerState::CHARACTER_REFERENCE);
                break;
            case LESS_THAN_SIGN:
                switchTo(TokenizerState::RCDATA_LESS_THAN_SIGN);
                break;
            case NULL_CHARACTER:
                PARSE_ERROR(UNEXPECTED_NULL_CHARACTER);
                break;
            case constants::endOfFile:
                emitToken(Token::createEOF());
                break;
            default:
                emitToken(Token::createCharacter(m_currentInputCharacter));
                break;
        }
    }

    DEFINE_STATE(RAWTEXT) {
        switch (consumeNextInputCharacter()) {
            case text::LESS_THAN_SIGN:
                switchTo(TokenizerState::RAWTEXT_LESS_THAN_SIGN);
                break;
            case text::NULL_CHARACTER:
                PARSE_ERROR(UNEXPECTED_NULL_CHARACTER);
                emitToken(Token::createCharacter(text::REPLACEMENT_CHARACTER));
                break;
            case constants::endOfFile:
                emitToken(Token::createEOF());
                break;
            default:
                emitToken(Token::createCharacter(m_currentInputCharacter));
                break;
        }
    }

    DEFINE_STATE(TAG_OPEN) {
        switch (consumeNextInputCharacter()) {
            case text::EXCLAMATION_MARK:
                switchTo(TokenizerState::MARKUP_DECLARATION_OPEN);
                break;
            case text::SOLIDUS:
                switchTo(TokenizerState::END_TAG_OPEN);
                break;
            case text::QUESTION_MARK:
                PARSE_ERROR(UNEXPECTED_QUESTION_MARK_INSTEAD_OF_TAG_NAME);
                m_currentToken = Token::createComment(std::string{});
                break;
            case constants::endOfFile:
                PARSE_ERROR(EOF_BEFORE_TAG_NAME);
                emitToken(Token::createCharacter(text::LESS_THAN_SIGN));
                emitToken(Token::createEOF());
                break;
            default:
                if (text::isASCIIAlpha(m_currentInputCharacter)) {
                    m_currentToken = Token::createStartTag(std::string{});
                    reconsumeIn(TokenizerState::TAG_NAME);
                    break;
                }

                PARSE_ERROR(INVALID_FIRST_CHARACTER_OF_TAG_NAME);
                emitToken(Token::createCharacter(text::LESS_THAN_SIGN));
                reconsumeIn(TokenizerState::DATA);
                break;
        }
    }

    DEFINE_STATE(END_TAG_OPEN) {
        switch (consumeNextInputCharacter()) {
            case text::GREATER_THAN_SIGN:
                PARSE_ERROR(MISSING_END_TAG_NAME);
                break;
            case constants::endOfFile:
                PARSE_ERROR(EOF_BEFORE_TAG_NAME);
                emitToken(Token::createCharacter(text::LESS_THAN_SIGN));
                emitToken(Token::createCharacter(text::SOLIDUS));
                emitToken(Token::createEOF());
                break;
            default:
                if (text::isASCIIAlpha(m_currentInputCharacter)) {
                    m_currentToken = Token::createEndTag(std::string{});
                    reconsumeIn(TokenizerState::TAG_NAME);
                    break;
                }

                PARSE_ERROR(INVALID_FIRST_CHARACTER_OF_TAG_NAME);
                m_currentToken = Token::createComment(std::string{});
                reconsumeIn(TokenizerState::BOGUS_COMMENT);
                break;
        }
    }

    DEFINE_STATE(TAG_NAME) {
        switch (consumeNextInputCharacter()) {
            case text::CHARACTER_TABULATION:
            case text::LINE_FEED:
            case text::FORM_FEED:
            case text::SPACE:
                switchTo(TokenizerState::BEFORE_ATTRIBUTE_NAME);
                break;
            case text::SOLIDUS:
                switchTo(TokenizerState::SELF_CLOSING_START_TAG);
                break;
            case text::GREATER_THAN_SIGN:
                switchTo(TokenizerState::DATA);
                emitCurrentToken();
                break;
            case text::NULL_CHARACTER:
                PARSE_ERROR(UNEXPECTED_NULL_CHARACTER);
                text::encoding::appendCodePoint(m_currentToken->asTag().tagName, text::REPLACEMENT_CHARACTER);
                break;
            case constants::endOfFile:
                PARSE_ERROR(EOF_IN_TAG);
                emitToken(Token::createEOF());
                break;
            default:
                if (text::isASCIIAlphaUpper(m_currentInputCharacter)) {
                    m_currentToken->asTag().tagName += text::toASCIILowercase(static_cast<char>(m_currentInputCharacter));
                } else {
                    text::encoding::appendCodePoint(m_currentToken->asTag().tagName, m_currentInputCharacter);
                }
                break;
        }
    }

    DEFINE_STATE(RCDATA_LESS_THAN_SIGN) {
        const auto character = consumeNextInputCharacter();

        if (character == text::SOLIDUS) {
            switchTo(TokenizerState::RCDATA_END_TAG_OPEN);
        } else {
            emitToken(Token::createCharacter(character));
        }
    }

    DEFINE_STATE(RCDATA_END_TAG_OPEN) {
        const auto character = consumeNextInputCharacter();

        if (text::isASCIIAlpha(character)) {
            m_currentToken = Token::createEndTag("");
            reconsumeIn(TokenizerState::RCDATA_END_TAG_NAME);
        } else {
            emitToken(Token::createCharacter(text::LESS_THAN_SIGN));
            emitToken(Token::createCharacter(text::SOLIDUS));
        }
    }

    DEFINE_STATE(RCDATA_END_TAG_NAME) {
        switch (consumeNextInputCharacter()) {
            case text::CHARACTER_TABULATION:
            case text::LINE_FEED:
            case text::FORM_FEED:
            case text::SPACE:
                if (isCurrentTokenAnAppropriateEndTagToken())
                    switchTo(TokenizerState::BEFORE_ATTRIBUTE_NAME);
                else
                    goto anythingElse;
                break;
            case text::SOLIDUS:
                if (isCurrentTokenAnAppropriateEndTagToken())
                    switchTo(TokenizerState::SELF_CLOSING_START_TAG);
                else
                    goto anythingElse;
                break;
            case text::GREATER_THAN_SIGN:
                if (isCurrentTokenAnAppropriateEndTagToken()) {
                    switchTo(TokenizerState::DATA);
                    emitCurrentToken();
                    break;
                }
                goto anythingElse;
        default:
            if (text::isASCIIAlphaUpper(m_currentInputCharacter)) {
                m_currentToken->asEndTag().tagName += static_cast<char>(text::toASCIILowercase(m_currentInputCharacter));
            } else if (text::isASCIIAlphaLower(m_currentInputCharacter)) {
                m_currentToken->asEndTag().tagName += static_cast<char>(m_currentInputCharacter);
            }
            break;
        anythingElse:
            emitToken(Token::createCharacter(text::LESS_THAN_SIGN));
            emitToken(Token::createCharacter(text::SOLIDUS));
            for (char character : m_currentToken->asEndTag().tagName) {
                emitToken(Token::createCharacter(static_cast<char32_t>(character)));
            }
            m_currentToken = std::nullopt;
            reconsumeIn(TokenizerState::RCDATA);
            break;
        }
    }

    DEFINE_STATE(RAWTEXT_LESS_THAN_SIGN) {
        if (consumeNextInputCharacter() == text::SOLIDUS) {
            m_temporaryBuffer = std::u32string{};
            switchTo(TokenizerState::RAWTEXT_END_TAG_OPEN);
        } else {
            emitToken(Token::createCharacter(text::LESS_THAN_SIGN));
            reconsumeIn(TokenizerState::RAWTEXT);
        }
    }

    DEFINE_STATE(RAWTEXT_END_TAG_OPEN) {
        if (text::isASCIIAlpha(consumeNextInputCharacter())) {
            m_currentToken = Token::createEndTag(std::string{});
            reconsumeIn(TokenizerState::RAWTEXT_END_TAG_NAME);
        } else {
            emitToken(Token::createCharacter(text::LESS_THAN_SIGN));
            emitToken(Token::createCharacter(text::GREATER_THAN_SIGN));
            reconsumeIn(TokenizerState::RAWTEXT);
        }
    }

    DEFINE_STATE(RAWTEXT_END_TAG_NAME) {
        switch (consumeNextInputCharacter()) {
            case text::CHARACTER_TABULATION:
            case text::LINE_FEED:
            case text::FORM_FEED:
            case text::SPACE:
                if (isCurrentTokenAnAppropriateEndTagToken())
                    switchTo(TokenizerState::BEFORE_ATTRIBUTE_NAME);
                else goto anythingElse;
                break;
            case text::SOLIDUS:
                if (isCurrentTokenAnAppropriateEndTagToken())
                    switchTo(TokenizerState::SELF_CLOSING_START_TAG);
                else goto anythingElse;
                break;
            case text::GREATER_THAN_SIGN:
                if (isCurrentTokenAnAppropriateEndTagToken()) {
                    switchTo(TokenizerState::DATA);
                    emitCurrentToken();
                } else goto anythingElse;
                break;
            default:
                if (text::isASCIIAlphaUpper(m_currentInputCharacter)) {
                    m_currentToken->asEndTag().tagName += static_cast<char>(text::toASCIILowercase(m_currentInputCharacter));
                    m_temporaryBuffer.value() += m_currentInputCharacter;
                    break;
                }

                if (text::isASCIIAlphaLower(m_currentInputCharacter)) {
                    m_currentToken->asEndTag().tagName += static_cast<char>(m_currentInputCharacter);
                    m_temporaryBuffer.value() += m_currentInputCharacter;
                    break;
                }

            anythingElse:
                emitToken(Token::createCharacter(text::LESS_THAN_SIGN));
                emitToken(Token::createCharacter(text::SOLIDUS));
                for (auto character : m_temporaryBuffer.value())
                    emitToken(Token::createCharacter(character));
                m_temporaryBuffer.reset();
                m_currentToken.reset();
                reconsumeIn(TokenizerState::RAWTEXT);
                break;
        }
    }

    DEFINE_STATE(BEFORE_ATTRIBUTE_NAME) {
        switch (consumeNextInputCharacter()) {
            case text::CHARACTER_TABULATION:
            case text::LINE_FEED:
            case text::FORM_FEED:
            case text::SPACE:
                break;
            case text::SOLIDUS:
            case text::GREATER_THAN_SIGN:
            case constants::endOfFile:
                reconsumeIn(TokenizerState::AFTER_ATTRIBUTE_NAME);
                break;
            case text::EQUALS_SIGN:
                PARSE_ERROR(UNEXPECTED_EQUALS_SIGN_BEFORE_ATTRIBUTE_NAME);
                m_currentAttribute = &m_currentToken->asTag().attributeList.emplace_back("=", std::string{});
                switchTo(TokenizerState::ATTRIBUTE_NAME);
                break;
            default: {
                std::string name;
                text::encoding::appendCodePoint(name, m_currentInputCharacter);
                m_currentAttribute = &m_currentToken->asTag().attributeList.emplace_back(std::move(name), std::string{});
                switchTo(TokenizerState::ATTRIBUTE_NAME);
                break;
            }
        }
    }

    DEFINE_STATE(ATTRIBUTE_NAME) {
        switch (consumeNextInputCharacter()) {
            case text::CHARACTER_TABULATION:
            case text::LINE_FEED:
            case text::FORM_FEED:
            case text::SPACE:
            case text::SOLIDUS:
            case text::GREATER_THAN_SIGN:
                reconsumeIn(TokenizerState::AFTER_ATTRIBUTE_NAME);
                break;
            case text::EQUALS_SIGN:
                switchTo(TokenizerState::BEFORE_ATTRIBUTE_VALUE);
                break;
            case text::NULL_CHARACTER:
                PARSE_ERROR(UNEXPECTED_NULL_CHARACTER);
                text::encoding::appendCodePoint(m_currentAttribute->name(), text::REPLACEMENT_CHARACTER);
                break;
            case text::QUOTATION_MARK:
            case text::APOSTROPHE:
            case text::LESS_THAN_SIGN:
                PARSE_ERROR(UNEXPECTED_CHARACTER_IN_ATTRIBUTE_NAME);
                [[fallthrough]];
            default:
                if (text::isASCIIAlphaUpper(m_currentInputCharacter)) {
                    m_currentAttribute->name() += static_cast<char>(text::toASCIILowercase(m_currentInputCharacter));
                } else {
                    text::encoding::appendCodePoint(m_currentAttribute->name(), m_currentInputCharacter);
                }
                break;
        }
    }

    DEFINE_STATE(BEFORE_ATTRIBUTE_VALUE) {
        switch (consumeNextInputCharacter()) {
            case text::CHARACTER_TABULATION:
            case text::LINE_FEED:
            case text::FORM_FEED:
            case text::SPACE:
                break;
            case text::QUOTATION_MARK:
                switchTo(TokenizerState::ATTRIBUTE_VALUE_DOUBLE_QUOTED);
                break;
            case text::APOSTROPHE:
                switchTo(TokenizerState::ATTRIBUTE_VALUE_SINGLE_QUOTED);
                break;
            case text::GREATER_THAN_SIGN:
                PARSE_ERROR(MISSING_ATTRIBUTE_VALUE);
                switchTo(TokenizerState::DATA);
                emitCurrentToken();
                break;
            default:
                reconsumeIn(TokenizerState::ATTRIBUTE_VALUE_UNQUOTED);
                break;
        }
    }

    DEFINE_STATE(ATTRIBUTE_VALUE_DOUBLE_QUOTED) {
        switch (consumeNextInputCharacter()) {
            case text::QUOTATION_MARK:
                switchTo(TokenizerState::AFTER_ATTRIBUTE_VALUE_QUOTED);
                break;
            case text::AMPERSAND:
                setReturnState(TokenizerState::ATTRIBUTE_VALUE_UNQUOTED);
                switchTo(TokenizerState::CHARACTER_REFERENCE);
                break;
            case text::NULL_CHARACTER:
                PARSE_ERROR(UNEXPECTED_NULL_CHARACTER);
                text::encoding::appendCodePoint(m_currentAttribute->value(), text::REPLACEMENT_CHARACTER);
                break;
            case constants::endOfFile:
                PARSE_ERROR(EOF_IN_TAG);
                emitToken(Token::createEOF());
                break;
            default:
                text::encoding::appendCodePoint(m_currentAttribute->value(), m_currentInputCharacter);
                break;
        }
    }

    DEFINE_STATE(AFTER_ATTRIBUTE_VALUE_QUOTED) {
        switch (consumeNextInputCharacter()) {
            case text::CHARACTER_TABULATION:
            case text::LINE_FEED:
            case text::FORM_FEED:
            case text::SPACE:
                switchTo(TokenizerState::BEFORE_ATTRIBUTE_NAME);
                break;
            case text::SOLIDUS:
                switchTo(TokenizerState::SELF_CLOSING_START_TAG);
                break;
            case text::GREATER_THAN_SIGN:
                switchTo(TokenizerState::DATA);
                emitCurrentToken();
                break;
            case constants::endOfFile:
                PARSE_ERROR(EOF_IN_TAG);
                emitToken(Token::createEOF());
                break;
            default:
                PARSE_ERROR(MISSING_WHITESPACE_BETWEEN_ATTRIBUTES);
                reconsumeIn(TokenizerState::BEFORE_ATTRIBUTE_NAME);
                break;
        }
    }

    DEFINE_STATE(BOGUS_COMMENT) {
        switch (consumeNextInputCharacter()) {
            case text::GREATER_THAN_SIGN:
                switchTo(TokenizerState::DATA);
                emitCurrentToken();
                break;
            case constants::endOfFile:
                emitCurrentToken();
                emitToken(Token::createEOF());
                break;
            case text::NULL_CHARACTER:
                PARSE_ERROR(UNEXPECTED_NULL_CHARACTER);
                text::encoding::appendCodePoint(m_currentToken->asComment(), text::REPLACEMENT_CHARACTER);
                break;
            default:
                text::encoding::appendCodePoint(m_currentToken->asComment(), m_currentInputCharacter);
                break;
        }
    }

    DEFINE_STATE(MARKUP_DECLARATION_OPEN) {
        const auto next = std::u32string_view(m_it, std::cend(m_data));
        if (next.starts_with(U"--")) {
            m_it += 2;
            m_currentToken = Token::createComment(std::string{});
            switchTo(TokenizerState::COMMENT);
            return;
        }

        constexpr const std::u32string_view doctypeString{U"DOCTYPE"};
        if (std::size(next) >= std::size(doctypeString)
                && std::equal(std::cbegin(doctypeString), std::cend(doctypeString), std::cbegin(next),
                       [] (char a, char b) -> bool {
                           return a == text::toASCIIUppercase(b);
                })) {
            m_it += std::size(doctypeString);
            switchTo(TokenizerState::DOCTYPE);
            return;
        }

        constexpr const std::u32string_view cdataString{U"[CDATA["};
        if (next.starts_with(cdataString)) {
            m_it += std::size(cdataString);
            // TODO
            // https://html.spec.whatwg.org/multipage/parsing.html#markup-declaration-open-state
            LEMON_ASSERT_NOT_REACHED_OR_RETURN_VOID();
        }

        PARSE_ERROR(INCORRECTLY_OPENED_COMMENT);
        m_currentToken = Token::createComment(std::string{});
        switchTo(TokenizerState::BOGUS_COMMENT);
    }

    DEFINE_STATE(COMMENT_START) {
        switch (consumeNextInputCharacter()) {
            case text::HYPHEN_MINUS:
                switchTo(TokenizerState::COMMENT_START_DASH);
                break;
            case text::GREATER_THAN_SIGN:
                PARSE_ERROR(ABRUPT_CLOSING_OF_EMPTY_COMMENT);
                switchTo(TokenizerState::DATA);
                emitCurrentToken();
                break;
            default:
                reconsumeIn(TokenizerState::COMMENT);
                break;
        }
    }

    DEFINE_STATE(COMMENT_START_DASH) {
        switch (consumeNextInputCharacter()) {
            case text::HYPHEN_MINUS:
                switchTo(TokenizerState::COMMENT_END);
                break;
            case text::GREATER_THAN_SIGN:
                PARSE_ERROR(ABRUPT_CLOSING_OF_EMPTY_COMMENT);
                switchTo(TokenizerState::DATA);
                emitCurrentToken();
                break;
            case constants::endOfFile:
                PARSE_ERROR(EOF_IN_COMMENT);
                emitCurrentToken();
                emitToken(Token::createEOF());
                break;
            default:
                m_currentToken->asComment() += '-';
                reconsumeIn(TokenizerState::COMMENT);
                break;
        }
    }

    DEFINE_STATE(COMMENT) {
        switch (consumeNextInputCharacter()) {
            case text::LESS_THAN_SIGN:
                m_currentToken->asComment() += '<';
                switchTo(TokenizerState::COMMENT_LESS_THAN_SIGN);
                break;
            case text::HYPHEN_MINUS:
                switchTo(TokenizerState::COMMENT_END);
                break;
            case text::NULL_CHARACTER:
                PARSE_ERROR(UNEXPECTED_NULL_CHARACTER);
                text::encoding::appendCodePoint(m_currentToken->asComment(), text::REPLACEMENT_CHARACTER);
                break;
            case constants::endOfFile:
                PARSE_ERROR(EOF_IN_COMMENT);
                emitCurrentToken();
                emitToken(Token::createEOF());
                break;
            default:
                text::encoding::appendCodePoint(m_currentToken->asComment(), m_currentInputCharacter);
                break;
        }
    }

    DEFINE_STATE(COMMENT_LESS_THAN_SIGN) {
        switch (consumeNextInputCharacter()) {
            case text::EXCLAMATION_MARK:
                m_currentToken->asComment() += '!';
                switchTo(TokenizerState::COMMENT_LESS_THAN_SIGN_BANG);
                break;
            case text::LESS_THAN_SIGN:
                m_currentToken->asComment() += '<';
                break;
            default:
                reconsumeIn(TokenizerState::COMMENT);
                break;
        }
    }

    DEFINE_STATE(COMMENT_LESS_THAN_SIGN_BANG) {
       if (consumeNextInputCharacter() == text::HYPHEN_MINUS)
           switchTo(TokenizerState::COMMENT_LESS_THAN_SIGN_BANG_DASH);
       else
           reconsumeIn(TokenizerState::COMMENT);
    }

    DEFINE_STATE(COMMENT_LESS_THAN_SIGN_BANG_DASH) {
        if (consumeNextInputCharacter() == text::HYPHEN_MINUS)
            switchTo(TokenizerState::COMMENT_LESS_THAN_SIGN_BANG_DASH);
        else
            reconsumeIn(TokenizerState::COMMENT_END_DASH);
    }

    DEFINE_STATE(COMMENT_LESS_THAN_SIGN_BANG_DASH_DASH) {
        if (consumeNextInputCharacter() == text::GREATER_THAN_SIGN || m_currentInputCharacter == constants::endOfFile) {
            reconsumeIn(TokenizerState::COMMENT_END);
        } else {
            PARSE_ERROR(NESTED_COMMENT);
            reconsumeIn(TokenizerState::COMMENT_END);
        }
    }

    DEFINE_STATE(COMMENT_END_DASH) {
        switch (consumeNextInputCharacter()) {
            case text::HYPHEN_MINUS:
                switchTo(TokenizerState::COMMENT_END);
                break;
            case constants::endOfFile:
                PARSE_ERROR(EOF_IN_COMMENT);
                emitCurrentToken();
                emitToken(Token::createEOF());
                break;
            default:
                m_currentToken->asComment() += '-';
                reconsumeIn(TokenizerState::COMMENT);
                break;
        }
    }

    DEFINE_STATE(COMMENT_END) {
        switch (consumeNextInputCharacter()) {
            case text::GREATER_THAN_SIGN:
                switchTo(TokenizerState::DATA);
                emitCurrentToken();
                break;
            case text::EXCLAMATION_MARK:
                switchTo(TokenizerState::COMMENT_END_BANG);
                break;
            case text::HYPHEN_MINUS:
                m_currentToken->asComment() += '-';
                break;
            case constants::endOfFile:
                PARSE_ERROR(EOF_IN_COMMENT);
                emitCurrentToken();
                emitToken(Token::createEOF());
                break;
            default:
                m_currentToken->asComment() += "--";
                reconsumeIn(TokenizerState::COMMENT);
                break;
        }
    }

    DEFINE_STATE(COMMENT_END_BANG) {
        switch (consumeNextInputCharacter()) {
            case text::HYPHEN_MINUS:
                m_currentToken->asComment() += "--!";
                switchTo(TokenizerState::COMMENT_END_DASH);
                break;
            case text::GREATER_THAN_SIGN:
                PARSE_ERROR(INCORRECTLY_CLOSED_COMMENT);
                switchTo(TokenizerState::DATA);
                emitCurrentToken();
                break;
            case constants::endOfFile:
                PARSE_ERROR(EOF_IN_COMMENT);
                emitCurrentToken();
                emitToken(Token::createEOF());
                break;
            default:
                m_currentToken->asComment() += "--!";
                switchTo(TokenizerState::COMMENT);
                break;
        }
    }

    DEFINE_STATE(DOCTYPE) {
        switch (consumeNextInputCharacter()) {
            case text::CHARACTER_TABULATION:
            case text::LINE_FEED:
            case text::FORM_FEED:
            case text::SPACE:
                switchTo(TokenizerState::BEFORE_DOCTYPE_NAME);
                break;
            case text::GREATER_THAN_SIGN:
                reconsumeIn(TokenizerState::BEFORE_DOCTYPE_NAME);
                break;
            case constants::endOfFile:
                PARSE_ERROR(EOF_IN_DOCTYPE);
                emitToken(Token::createDoctype(std::nullopt, std::nullopt, std::nullopt, true));
                emitToken(Token::createEOF());
                break;
            default:
                PARSE_ERROR(MISSING_WHITESPACE_BEFORE_DOCTYPE_NAME);
                reconsumeIn(TokenizerState::BEFORE_DOCTYPE_NAME);
                break;
        }
    }

    DEFINE_STATE(BEFORE_DOCTYPE_NAME) {
        LEMON_ASSERT(!m_currentToken.has_value());

        switch (consumeNextInputCharacter()) {
            case text::CHARACTER_TABULATION:
            case text::LINE_FEED:
            case text::FORM_FEED:
            case text::SPACE:
                break;
            case text::NULL_CHARACTER:
                PARSE_ERROR(UNEXPECTED_NULL_CHARACTER);
                m_currentToken = Token::createDoctype("\uFFFD");
                switchTo(TokenizerState::DOCTYPE_NAME);
                break;
            case text::GREATER_THAN_SIGN:
                PARSE_ERROR(MISSING_DOCTYPE_NAME);
                switchTo(TokenizerState::DATA);
                emitToken(Token::createDoctype(std::nullopt, std::nullopt, std::nullopt, true));
                m_currentToken.reset();
                break;
            case constants::endOfFile:
                PARSE_ERROR(EOF_IN_DOCTYPE);
                emitToken(Token::createDoctype(std::nullopt, std::nullopt, std::nullopt, true));
                m_currentToken.reset();
                emitToken(Token::createEOF());
                break;
            default:
                std::string name;
                if (text::isASCIIAlphaUpper(m_currentInputCharacter)) {
                    name += text::toASCIILowercase(static_cast<char>(m_currentInputCharacter));
                } else {
                    text::encoding::appendCodePoint(name, m_currentInputCharacter);
                }
                m_currentToken = Token::createDoctype(std::move(name), std::nullopt, std::nullopt, false);
                switchTo(TokenizerState::DOCTYPE_NAME);
                break;
        }
    }

    DEFINE_STATE(DOCTYPE_NAME) {
        switch (consumeNextInputCharacter()) {
            case text::CHARACTER_TABULATION:
            case text::LINE_FEED:
            case text::FORM_FEED:
            case text::SPACE:
                switchTo(TokenizerState::AFTER_DOCTYPE_NAME);
                break;
            case text::GREATER_THAN_SIGN:
                switchTo(TokenizerState::DATA);
                emitCurrentToken();
                break;
            case text::NULL_CHARACTER:
                PARSE_ERROR(UNEXPECTED_NULL_CHARACTER);
                text::encoding::appendCodePoint(m_currentToken->asDoctype().name.value(), text::REPLACEMENT_CHARACTER);
                break;
            case constants::endOfFile:
                PARSE_ERROR(EOF_IN_DOCTYPE);
                m_currentToken->asDoctype().forceQuirks = true;
                emitCurrentToken();
                emitToken(Token::createEOF());
                break;
            default:
                if (text::isASCIIAlphaUpper(m_currentInputCharacter)) {
                    m_currentToken->asDoctype().name.value() += text::toASCIILowercase(static_cast<char>(m_currentInputCharacter));
                } else {
                    text::encoding::appendCodePoint(m_currentToken->asDoctype().name.value(), m_currentInputCharacter);
                }
                break;
        }
    }

    DEFINE_STATE(AFTER_DOCTYPE_NAME) {
        switch (consumeNextInputCharacter()) {
            case text::CHARACTER_TABULATION:
            case text::LINE_FEED:
            case text::FORM_FEED:
            case text::SPACE:
                // Ignore the character.
                break;
            case text::GREATER_THAN_SIGN:
                // This would be e.g. <!DOCTYPE html >
                switchTo(TokenizerState::DATA);
                emitCurrentToken();
                break;
            case constants::endOfFile:
                PARSE_ERROR(EOF_IN_DOCTYPE);
                m_currentToken->asDoctype().forceQuirks = true;
                emitCurrentToken();
                emitToken(Token::createEOF());
                break;
            default: {
                const std::u32string_view remaining(m_it, std::end(m_data));
                if (text::toASCIILowercase(m_currentInputCharacter) == text::LATIN_SMALL_LETTER_P
                        && startsWith(remaining, U"ublic")) {
                    m_it += 5;
                    switchTo(TokenizerState::AFTER_DOCTYPE_PUBLIC_KEYWORD);
                    break;
                }
                if (text::toASCIILowercase(m_currentInputCharacter) == text::LATIN_SMALL_LETTER_S
                        && startsWith(remaining, U"ystem")) {
                    m_it += 5;
                    switchTo(TokenizerState::AFTER_DOCTYPE_SYSTEM_KEYWORD);
                    break;
                }

                PARSE_ERROR(INVALID_CHARACTER_SEQUENCE_AFTER_DOCTYPE_NAME);
                m_currentToken->asDoctype().forceQuirks = true;
                reconsumeIn(TokenizerState::BOGUS_DOCTYPE);
                break;
            }
        }
    }

    DEFINE_STATE(AFTER_DOCTYPE_PUBLIC_KEYWORD) {
        switch (consumeNextInputCharacter()) {
            case text::CHARACTER_TABULATION:
            case text::LINE_FEED:
            case text::FORM_FEED:
            case text::SPACE:
                switchTo(TokenizerState::BEFORE_DOCTYPE_PUBLIC_IDENTIFIER);
                break;
            case text::QUOTATION_MARK:
                PARSE_ERROR(MISSING_WHITESPACE_AFTER_DOCTYPE_PUBLIC_KEYWORD);
                m_currentToken->asDoctype().forceQuirks = true;
                m_currentToken->asDoctype().publicIdentifier = std::string{};
                switchTo(TokenizerState::DOCTYPE_PUBLIC_IDENTIFIER_DOUBLE_QUOTED);
                break;
            case text::APOSTROPHE:
                PARSE_ERROR(MISSING_WHITESPACE_AFTER_DOCTYPE_PUBLIC_KEYWORD);
                m_currentToken->asDoctype().forceQuirks = true;
                m_currentToken->asDoctype().publicIdentifier = std::string{};
                switchTo(TokenizerState::DOCTYPE_PUBLIC_IDENTIFIER_SINGLE_QUOTED);
                break;
            case text::GREATER_THAN_SIGN:
                PARSE_ERROR(MISSING_DOCTYPE_PUBLIC_IDENTIFIER);
                m_currentToken->asDoctype().forceQuirks = true;
                switchTo(TokenizerState::DATA);
                emitCurrentToken();
                break;
            case constants::endOfFile:
                PARSE_ERROR(EOF_IN_DOCTYPE);
                m_currentToken->asDoctype().forceQuirks = true;
                emitCurrentToken();
                emitToken(Token::createEOF());
                break;
            default:
                PARSE_ERROR(MISSING_QUOTE_BEFORE_DOCTYPE_PUBLIC_IDENTIFIER);
                m_currentToken->asDoctype().forceQuirks = true;
                reconsumeIn(TokenizerState::BOGUS_DOCTYPE);
                break;
        }
    }

    DEFINE_STATE(BEFORE_DOCTYPE_PUBLIC_IDENTIFIER) {
        switch (consumeNextInputCharacter()) {
            case text::CHARACTER_TABULATION:
            case text::LINE_FEED:
            case text::FORM_FEED:
            case text::SPACE:
                // Ignore the character.
                break;
            case text::QUOTATION_MARK:
                m_currentToken->asDoctype().publicIdentifier = std::string{};
                switchTo(TokenizerState::DOCTYPE_PUBLIC_IDENTIFIER_DOUBLE_QUOTED);
                break;
            case text::APOSTROPHE:
                m_currentToken->asDoctype().publicIdentifier = std::string{};
                switchTo(TokenizerState::DOCTYPE_PUBLIC_IDENTIFIER_SINGLE_QUOTED);
                break;
            case text::GREATER_THAN_SIGN:
                PARSE_ERROR(MISSING_DOCTYPE_PUBLIC_IDENTIFIER);
                m_currentToken->asDoctype().forceQuirks = true;
                switchTo(TokenizerState::DATA);
                emitCurrentToken();
                break;
            case constants::endOfFile:
                PARSE_ERROR(EOF_IN_DOCTYPE);
                m_currentToken->asDoctype().forceQuirks = true;
                emitCurrentToken();
                emitToken(Token::createEOF());
                break;
            default:
                PARSE_ERROR(MISSING_QUOTE_BEFORE_DOCTYPE_PUBLIC_IDENTIFIER);
                m_currentToken->asDoctype().forceQuirks = true;
                reconsumeIn(TokenizerState::BOGUS_DOCTYPE);
                break;
        }
    }

    DEFINE_STATE(DOCTYPE_PUBLIC_IDENTIFIER_DOUBLE_QUOTED) {
        switch (consumeNextInputCharacter()) {
            case text::QUOTATION_MARK:
                switchTo(TokenizerState::AFTER_DOCTYPE_PUBLIC_IDENTIFIER);
                break;
            case text::NULL_CHARACTER:
                PARSE_ERROR(UNEXPECTED_NULL_CHARACTER);
                text::encoding::appendCodePoint(m_currentToken->asDoctype().publicIdentifier.value(),
                                                text::REPLACEMENT_CHARACTER);
                break;
            case text::GREATER_THAN_SIGN:
                PARSE_ERROR(ABRUPT_DOCTYPE_PUBLIC_IDENTIFIER);
                m_currentToken->asDoctype().forceQuirks = true;
                switchTo(TokenizerState::DATA);
                emitCurrentToken();
                break;
            case constants::endOfFile:
                PARSE_ERROR(EOF_IN_DOCTYPE);
                m_currentToken->asDoctype().forceQuirks = true;
                emitCurrentToken();
                emitToken(Token::createEOF());
                break;
            default:
                text::encoding::appendCodePoint(m_currentToken->asDoctype().publicIdentifier.value(),
                                                m_currentInputCharacter);
                break;
        }
    }

    DEFINE_STATE(DOCTYPE_PUBLIC_IDENTIFIER_SINGLE_QUOTED) {
        switch (consumeNextInputCharacter()) {
            case text::APOSTROPHE:
                switchTo(TokenizerState::AFTER_DOCTYPE_PUBLIC_IDENTIFIER);
                break;
            case text::NULL_CHARACTER:
                PARSE_ERROR(UNEXPECTED_NULL_CHARACTER);
                text::encoding::appendCodePoint(m_currentToken->asDoctype().publicIdentifier.value(),
                                                text::REPLACEMENT_CHARACTER);
                break;
            case text::GREATER_THAN_SIGN:
                PARSE_ERROR(ABRUPT_DOCTYPE_PUBLIC_IDENTIFIER);
                m_currentToken->asDoctype().forceQuirks = true;
                switchTo(TokenizerState::DATA);
                emitCurrentToken();
                break;
            case constants::endOfFile:
                PARSE_ERROR(EOF_IN_DOCTYPE);
                m_currentToken->asDoctype().forceQuirks = true;
                emitCurrentToken();
                emitToken(Token::createEOF());
                break;
            default:
                text::encoding::appendCodePoint(m_currentToken->asDoctype().publicIdentifier.value(),
                                                m_currentInputCharacter);
                break;
        }
    }

    DEFINE_STATE(AFTER_DOCTYPE_PUBLIC_IDENTIFIER) {
        switch (consumeNextInputCharacter()) {
            case text::CHARACTER_TABULATION:
            case text::LINE_FEED:
            case text::FORM_FEED:
            case text::SPACE:
                switchTo(TokenizerState::BETWEEN_DOCTYPE_PUBLIC_AND_SYSTEM_IDENTIFIERS);
                break;
            case text::GREATER_THAN_SIGN:
                switchTo(TokenizerState::DATA);
                emitCurrentToken();
                break;
            case text::QUOTATION_MARK:
                PARSE_ERROR(MISSING_WHITESPACE_BETWEEN_DOCTYPE_PUBLIC_AND_SYSTEM_IDENTIFIERS);
                m_currentToken->asDoctype().systemIdentifier.emplace();
                switchTo(TokenizerState::DOCTYPE_SYSTEM_IDENTIFIER_DOUBLE_QUOTED);
                break;
            case text::APOSTROPHE:
                PARSE_ERROR(MISSING_WHITESPACE_BETWEEN_DOCTYPE_PUBLIC_AND_SYSTEM_IDENTIFIERS);
                m_currentToken->asDoctype().systemIdentifier.emplace();
                switchTo(TokenizerState::DOCTYPE_SYSTEM_IDENTIFIER_SINGLE_QUOTED);
                break;
            case constants::endOfFile:
                PARSE_ERROR(EOF_IN_DOCTYPE);
                m_currentToken->asDoctype().forceQuirks = true;
                emitCurrentToken();
                emitToken(Token::createEOF());
                break;
            default:
                PARSE_ERROR(MISSING_QUOTE_BEFORE_DOCTYPE_SYSTEM_IDENTIFIER);
                m_currentToken->asDoctype().forceQuirks = true;
                reconsumeIn(TokenizerState::BOGUS_DOCTYPE);
                break;
        }
    }

    DEFINE_STATE(BETWEEN_DOCTYPE_PUBLIC_AND_SYSTEM_IDENTIFIERS) {
        switch (consumeNextInputCharacter()) {
            case text::CHARACTER_TABULATION:
            case text::LINE_FEED:
            case text::FORM_FEED:
            case text::SPACE:
                // Ignore the character.
                break;
            case text::GREATER_THAN_SIGN:
                // <!DOCTYPE html PUBLIC "agaigag"   >
                switchTo(TokenizerState::DATA);
                emitCurrentToken();
                break;
            case text::QUOTATION_MARK:
                m_currentToken->asDoctype().systemIdentifier.emplace();
                switchTo(TokenizerState::DOCTYPE_SYSTEM_IDENTIFIER_DOUBLE_QUOTED);
                break;
            case text::APOSTROPHE:
                m_currentToken->asDoctype().systemIdentifier.emplace();
                switchTo(TokenizerState::DOCTYPE_SYSTEM_IDENTIFIER_SINGLE_QUOTED);
                break;
            case constants::endOfFile:
                PARSE_ERROR(EOF_IN_DOCTYPE);
                m_currentToken->asDoctype().forceQuirks = true;
                emitCurrentToken();
                emitToken(Token::createEOF());
                break;
            default:
                PARSE_ERROR(MISSING_QUOTE_BEFORE_DOCTYPE_SYSTEM_IDENTIFIER);
                m_currentToken->asDoctype().forceQuirks = true;
                reconsumeIn(TokenizerState::BOGUS_DOCTYPE);
                break;
        }
    }

    DEFINE_STATE(AFTER_DOCTYPE_SYSTEM_KEYWORD) {
        switch (consumeNextInputCharacter()) {
            case text::CHARACTER_TABULATION:
            case text::LINE_FEED:
            case text::FORM_FEED:
            case text::SPACE:
                switchTo(TokenizerState::BEFORE_DOCTYPE_SYSTEM_IDENTIFIER);
                break;
            case text::QUOTATION_MARK:
                PARSE_ERROR(MISSING_WHITESPACE_AFTER_DOCTYPE_SYSTEM_KEYWORD);
                m_currentToken->asDoctype().systemIdentifier.emplace();
                switchTo(TokenizerState::DOCTYPE_SYSTEM_IDENTIFIER_DOUBLE_QUOTED);
                break;
            case text::APOSTROPHE:
                PARSE_ERROR(MISSING_WHITESPACE_AFTER_DOCTYPE_SYSTEM_KEYWORD);
                m_currentToken->asDoctype().systemIdentifier.emplace();
                switchTo(TokenizerState::DOCTYPE_SYSTEM_IDENTIFIER_SINGLE_QUOTED);
                break;
            case text::GREATER_THAN_SIGN:
                PARSE_ERROR(MISSING_DOCTYPE_SYSTEM_IDENTIFIER);
                m_currentToken->asDoctype().forceQuirks = true;
                switchTo(TokenizerState::DATA);
                emitCurrentToken();
                break;
            case constants::endOfFile:
                PARSE_ERROR(EOF_IN_DOCTYPE);
                m_currentToken->asDoctype().forceQuirks = true;
                emitCurrentToken();
                emitToken(Token::createEOF());
                break;
            default:
                PARSE_ERROR(MISSING_QUOTE_BEFORE_DOCTYPE_SYSTEM_IDENTIFIER);
                m_currentToken->asDoctype().forceQuirks = true;
                reconsumeIn(TokenizerState::BOGUS_DOCTYPE);
                break;
        }
    }

    DEFINE_STATE(BEFORE_DOCTYPE_SYSTEM_IDENTIFIER) {
        switch (consumeNextInputCharacter()) {
            case text::CHARACTER_TABULATION:
            case text::LINE_FEED:
            case text::FORM_FEED:
            case text::SPACE:
                // Ignore the character.
                break;
            case text::QUOTATION_MARK:
                m_currentToken->asDoctype().systemIdentifier.emplace();
                switchTo(TokenizerState::DOCTYPE_SYSTEM_IDENTIFIER_DOUBLE_QUOTED);
                break;
            case text::APOSTROPHE:
                m_currentToken->asDoctype().systemIdentifier.emplace();
                switchTo(TokenizerState::DOCTYPE_SYSTEM_IDENTIFIER_SINGLE_QUOTED);
                break;
            case text::GREATER_THAN_SIGN:
                // <!DOCTYPE html SYSTEM>
                PARSE_ERROR(MISSING_DOCTYPE_SYSTEM_IDENTIFIER);
                m_currentToken->asDoctype().forceQuirks = true;
                switchTo(TokenizerState::DATA);
                emitCurrentToken();
                break;
            case constants::endOfFile:
                PARSE_ERROR(EOF_IN_DOCTYPE);
                m_currentToken->asDoctype().forceQuirks = true;
                emitCurrentToken();
                emitToken(Token::createEOF());
                break;
            default:
                PARSE_ERROR(MISSING_QUOTE_BEFORE_DOCTYPE_SYSTEM_IDENTIFIER);
                m_currentToken->asDoctype().forceQuirks = true;
                reconsumeIn(TokenizerState::BOGUS_DOCTYPE);
                break;
        }
    }

    DEFINE_STATE(DOCTYPE_SYSTEM_IDENTIFIER_DOUBLE_QUOTED) {
        switch (consumeNextInputCharacter()) {
            case text::QUOTATION_MARK:
                switchTo(TokenizerState::AFTER_DOCTYPE_SYSTEM_IDENTIFIER);
                break;
            case text::NULL_CHARACTER:
                PARSE_ERROR(UNEXPECTED_NULL_CHARACTER);
                text::encoding::appendCodePoint(m_currentToken->asDoctype().systemIdentifier.value(),
                                                text::REPLACEMENT_CHARACTER);
                break;
            case text::GREATER_THAN_SIGN:
                PARSE_ERROR(ABRUPT_DOCTYPE_SYSTEM_IDENTIFIER);
                m_currentToken->asDoctype().forceQuirks = true;
                switchTo(TokenizerState::DATA);
                emitCurrentToken();
                break;
            case constants::endOfFile:
                PARSE_ERROR(EOF_IN_DOCTYPE);
                m_currentToken->asDoctype().forceQuirks = true;
                emitCurrentToken();
                emitToken(Token::createEOF());
                break;
            default:
                text::encoding::appendCodePoint(m_currentToken->asDoctype().systemIdentifier.value(),
                                                m_currentInputCharacter);
                break;
        }
    }

    DEFINE_STATE(DOCTYPE_SYSTEM_IDENTIFIER_SINGLE_QUOTED) {
        switch (consumeNextInputCharacter()) {
            case text::APOSTROPHE:
                switchTo(TokenizerState::AFTER_DOCTYPE_SYSTEM_IDENTIFIER);
                break;
            case text::NULL_CHARACTER:
                PARSE_ERROR(UNEXPECTED_NULL_CHARACTER);
                text::encoding::appendCodePoint(m_currentToken->asDoctype().systemIdentifier.value(),
                                                text::REPLACEMENT_CHARACTER);
                break;
            case text::GREATER_THAN_SIGN:
                PARSE_ERROR(ABRUPT_DOCTYPE_SYSTEM_IDENTIFIER);
                m_currentToken->asDoctype().forceQuirks = true;
                switchTo(TokenizerState::DATA);
                emitCurrentToken();
                break;
            case constants::endOfFile:
                PARSE_ERROR(EOF_IN_DOCTYPE);
                m_currentToken->asDoctype().forceQuirks = true;
                emitCurrentToken();
                emitToken(Token::createEOF());
                break;
            default:
                text::encoding::appendCodePoint(m_currentToken->asDoctype().systemIdentifier.value(),
                                                m_currentInputCharacter);
                break;
        }
    }

    DEFINE_STATE(AFTER_DOCTYPE_SYSTEM_IDENTIFIER) {
        switch (consumeNextInputCharacter()) {
            case text::CHARACTER_TABULATION:
            case text::LINE_FEED:
            case text::FORM_FEED:
            case text::SPACE:
                // Ignore the character.
                break;
            case text::GREATER_THAN_SIGN:
                switchTo(TokenizerState::DATA);
                emitCurrentToken();
                break;
            case constants::endOfFile:
                PARSE_ERROR(EOF_IN_DOCTYPE);
                m_currentToken->asDoctype().forceQuirks = true;
                emitCurrentToken();
                emitToken(Token::createEOF());
                break;
            default:
                PARSE_ERROR(UNEXPECTED_CHARACTER_AFTER_DOCTYPE_SYSTEM_IDENTIFIER);
                reconsumeIn(TokenizerState::BOGUS_DOCTYPE);
                // Don't set force-quirks.
                break;
        }
    }

    DEFINE_STATE(BOGUS_DOCTYPE) {
        switch (consumeNextInputCharacter()) {
            case text::GREATER_THAN_SIGN:
                switchTo(TokenizerState::DATA);
                emitCurrentToken();
                break;
            case text::NULL_CHARACTER:
                PARSE_ERROR(UNEXPECTED_NULL_CHARACTER);
                // Ignore the character.
                break;
            case constants::endOfFile:
                emitCurrentToken();
                emitToken(Token::createEOF());
                break;
            default:
                // Ignore the character.
                break;
        }
    }

#define DEFINE_TODO_STATE(name) \
    DEFINE_STATE(name) { \
        LOG_ERROR("Unimplemented state: " #name); \
        LEMON_ASSERT_NOT_REACHED_OR_RETURN_VOID(); \
    }

    DEFINE_TODO_STATE(SCRIPT_DATA)
    DEFINE_TODO_STATE(PLAINTEXT)
    DEFINE_TODO_STATE(SCRIPT_DATA_LESS_THAN_SIGN)
    DEFINE_TODO_STATE(SCRIPT_DATA_END_TAG_OPEN)
    DEFINE_TODO_STATE(SCRIPT_DATA_END_TAG_NAME)
    DEFINE_TODO_STATE(SCRIPT_DATA_ESCAPE_START)
    DEFINE_TODO_STATE(SCRIPT_DATA_ESCAPE_START_DASH)
    DEFINE_TODO_STATE(SCRIPT_DATA_ESCAPED)
    DEFINE_TODO_STATE(SCRIPT_DATA_ESCAPED_DASH)
    DEFINE_TODO_STATE(SCRIPT_DATA_ESCAPED_DASH_DASH)
    DEFINE_TODO_STATE(SCRIPT_DATA_ESCAPED_LESS_THAN_SIGN)
    DEFINE_TODO_STATE(SCRIPT_DATA_ESCAPED_END_TAG_OPEN)
    DEFINE_TODO_STATE(SCRIPT_DATA_ESCAPED_END_TAG_NAME)
    DEFINE_TODO_STATE(SCRIPT_DATA_DOUBLE_ESCAPE_START)
    DEFINE_TODO_STATE(SCRIPT_DATA_DOUBLE_ESCAPED)
    DEFINE_TODO_STATE(SCRIPT_DATA_DOUBLE_ESCAPED_DASH)
    DEFINE_TODO_STATE(SCRIPT_DATA_DOUBLE_ESCAPED_DASH_DASH)
    DEFINE_TODO_STATE(SCRIPT_DATA_DOUBLE_ESCAPED_LESS_THAN_SIGN)
    DEFINE_TODO_STATE(SCRIPT_DATA_DOUBLE_ESCAPED_END)
    DEFINE_TODO_STATE(AFTER_ATTRIBUTE_NAME)
    DEFINE_TODO_STATE(ATTRIBUTE_VALUE_SINGLE_QUOTED)
    DEFINE_TODO_STATE(ATTRIBUTE_VALUE_UNQUOTED)
    DEFINE_TODO_STATE(SELF_CLOSING_START_TAG)
    DEFINE_TODO_STATE(CDATA_SECTION)
    DEFINE_TODO_STATE(CDATA_SECTION_BRACKET)
    DEFINE_TODO_STATE(CDATA_SECTION_END)
    DEFINE_TODO_STATE(CHARACTER_REFERENCE)
    DEFINE_TODO_STATE(NAMED_CHARACTER_REFERENCE)
    DEFINE_TODO_STATE(AMBIGUOUS_AMPERSAND)
    DEFINE_TODO_STATE(NUMERIC_CHARACTER_REFERENCE)
    DEFINE_TODO_STATE(HEXADECIMAL_CHARACTER_REFERENCE_START)
    DEFINE_TODO_STATE(DECIMAL_CHARACTER_REFERENCE_START)
    DEFINE_TODO_STATE(HEXADECIMAL_CHARACTER_REFERENCE)
    DEFINE_TODO_STATE(DECIMAL_CHARCTER_REFERENCE)
    DEFINE_TODO_STATE(NUMERIC_CHARACTER_REFERENCE_END)


} // namespace html
