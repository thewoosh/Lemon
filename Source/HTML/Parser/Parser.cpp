/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/HTML/Parser/Parser.hpp"

#include "Source/DOM/Document.hpp"
#include "Source/HTML/Parser/Tokenizer.hpp"
#include "Source/HTML/Parser/TreeConstructor.hpp"

namespace html {

    std::unique_ptr<dom::Document>
    Parser::parseDocument(std::u32string_view input) noexcept {
        auto document = std::make_unique<dom::Document>();

        Tokenizer tokenizer{input};
        TreeConstructor treeConstructor{&tokenizer, document.get()};

        tokenizer.setTokenEmissionCallback([&](Token &&token) -> bool {
            return treeConstructor.onTokenEmission(std::move(token));
        });

        if (!tokenizer.run())
            return nullptr;

        return document;
    }

} // namespace html
