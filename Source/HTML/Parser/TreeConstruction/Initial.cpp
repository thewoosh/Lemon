/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/HTML/Parser/TreeConstruction/Commons.hpp"

#include "Source/DOM/DocumentType.hpp"

namespace html {

    static void
    handleDoctype(dom::Document *document, const Token::DoctypeData &doctype) noexcept {
        if (doctype.name != "html")
            PARSE_ERROR(DOCTYPE_NAME_NOT_HTML);

        if (doctype.publicIdentifier.has_value())
            PARSE_ERROR(DOCTYPE_HAS_PUBLIC_IDENTIFIER);

        if (doctype.systemIdentifier.has_value() && doctype.systemIdentifier.value() != "about:legacy-compat")
            PARSE_ERROR(DOCTYPE_HAS_SYSTEM_IDENTIFIER);

        document->appendChild(std::make_unique<dom::DocumentType>(
                document,
                doctype.name.value_or(std::string()),
                doctype.publicIdentifier.value_or(std::string()),
                doctype.systemIdentifier.value_or(std::string())
        ));

        // TODO associate to document.doctype in the future

        if (!document->isIframeSrcdocDocument() && !document->parserCannotChangeTheModeFlag()) {
            // TODO match list in order to determine the quirks mode.
        }
    }

    DEFINE_INSERTION_MODE(INITIAL) {
        if (isWhitespace(token))
            return true;

        if (token.type() == Token::Type::COMMENT) {
            insertAComment(this, token.asComment());
            return true;
        }

        if (token.type() == Token::Type::DOCTYPE) {
            handleDoctype(m_document, token.asDoctype());
            switchTheInsertionModeTo(InsertionMode::BEFORE_HTML);
            return true;
        }

        if (!m_document->isIframeSrcdocDocument())
            PARSE_ERROR(EXPECTED_DOCTYPE);

        if (!m_document->parserCannotChangeTheModeFlag())
            m_document->setQuirksMode(dom::QuirksMode::QUIRKS);

        switchTheInsertionModeTo(InsertionMode::BEFORE_HTML);
        return reprocessToken(std::move(token));
    }

} // namespace html
