/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/HTML/Parser/TreeConstruction/Commons.hpp"

#include "Source/HTML/Elements/HTMLHeadElement.hpp"
#include "Source/HTML/Parser/Tokenizer.hpp"
#include "Source/Infra/Namespaces.hpp"

namespace html {

    [[nodiscard]] static bool
    handleScriptTag(TreeConstructor *, Token &&) noexcept {
        // TODO 2021-08-11
        LEMON_ASSERT_NOT_REACHED_OR_RETURN(false);
    }

    DEFINE_INSERTION_MODE(IN_HEAD) {
        if (isWhitespace(token)) {
            insertACharacter(this, token.asCharacter());
            return true;
        }

        if (token.type() == Token::Type::COMMENT) {
            insertAComment(this, token.asComment());
            return true;
        }

        if (token.type() == Token::Type::DOCTYPE) {
            PARSE_ERROR(DOCTYPE_OUTSIDE_INITIAL_INSERTION_MODE);
            return true;
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagName == "html") {
            return processTheRulesFor(InsertionMode::IN_BODY, std::move(token));
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagNameIsAnyOf("base", "basefont", "bgsound", "link")) {
            insertAnHTMLElement(this, std::move(token));
            m_stackOfOpenElements.popOffCurrentNode();
            // TODO acknowledge self closing flag
            return true;
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagName == "meta") {
            insertAnHTMLElement(this, std::move(token));
            auto *element = m_stackOfOpenElements.popOffCurrentNode();
            // TODO acknowledge self closing flag
            // TODO change encoding
            static_cast<void>(element);
            return true;
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagName == "title") {
            insertAnHTMLElement(this, std::move(token));
            m_tokenizer->setAppropriateEndTagName("title");
            m_tokenizer->switchTo(TokenizerState::RCDATA);
            m_originalInsertionMode = m_insertionMode;
            switchTheInsertionModeTo(InsertionMode::TEXT);
            return true;
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagName == "noscript") {
            if (m_document->scriptingFlag()) {
                return followTheGenericRawTextElementParsingAlgorithm(std::move(token));
            }

            insertAnHTMLElement(this, std::move(token));
            switchTheInsertionModeTo(InsertionMode::IN_HEAD_NOSCRIPT);
            return true;
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagNameIsAnyOf("noframes", "style")) {
            return followTheGenericRawTextElementParsingAlgorithm(std::move(token));
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagName == "script") {
            return handleScriptTag(this, std::move(token));
        }

        if (token.type() == Token::Type::END_TAG && token.asEndTag().tagName == "head") {
            auto poppedElement = m_stackOfOpenElements.popOffCurrentNode();
            LEMON_ASSERT(poppedElement->localName() == "head");
            LEMON_ASSERT(poppedElement->namespaceURI() == infra::namespaces::HTML);
            static_cast<void>(poppedElement);

            switchTheInsertionModeTo(InsertionMode::AFTER_HEAD);
            return true;
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagName == "template") {
            LEMON_ASSERT_NOT_REACHED_OR_RETURN(false);
        }

        if (token.type() == Token::Type::END_TAG && token.asStartTag().tagName == "template") {
            LEMON_ASSERT_NOT_REACHED_OR_RETURN(false);
        }

        // No other end tags are allowed from here
        if (token.type() == Token::Type::END_TAG) {
            PARSE_ERROR(ILLEGAL_END_TAG_IN_HEAD);
            return true;
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagName == "head") {
            PARSE_ERROR(ANOTHER_HEAD_ELEMENT_IN_HEAD);
            return true;
        }

        auto poppedElement = m_stackOfOpenElements.popOffCurrentNode();
        static_cast<void>(poppedElement);
        LEMON_ASSERT(poppedElement->localName() == "head");
        LEMON_ASSERT(poppedElement->namespaceURI() == infra::namespaces::HTML);

        switchTheInsertionModeTo(InsertionMode::AFTER_HEAD);
        return reprocessToken(std::move(token));
    }

} // namespace html
