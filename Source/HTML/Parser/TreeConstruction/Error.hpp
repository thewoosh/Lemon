/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 *
 * https://html.spec.whatwg.org/multipage/parsing.html#parse-errors
 */

#pragma once

#include <string_view>

namespace html {

    enum class TreeConstructionError {
        ABRUPT_END_OF_FILE,
        ANOTHER_BODY_ELEMENT_IN_BODY,
        ANOTHER_HEAD_ELEMENT_IN_HEAD,
        DOCTYPE_NAME_NOT_HTML,
        DOCTYPE_HAS_PUBLIC_IDENTIFIER,
        DOCTYPE_HAS_SYSTEM_IDENTIFIER,
        DOCTYPE_OUTSIDE_INITIAL_INSERTION_MODE,
        END_TAG_WITHOUT_MATCHING_START_TAG,
        EOF_IN_TEXT_INSERTION_MODE,
        EXPECTED_DOCTYPE,
        FORM_CLOSED_INCORRECTLY,
        FRAMESET_IN_BODY,
        ILLEGAL_BODY_END_TAG,
        ILLEGAL_HTML_END_TAG,
        ILLEGAL_ELEMENT_BETWEEN_HEAD_AND_BODY,
        ILLEGAL_END_TAG_IN_HEAD,
        ILLEGAL_START_TAG_IN_HTML_ELEMENT,
        ILLEGAL_TAG_IN_BODY_ELEMENT,
        ILLEGAL_TAG_IN_HTML_ELEMENT,
        MISPLACED_P_END_TAG,
        MISSING_END_TAGS,
        MULTIPLE_HEAD_ELEMENTS,
        NESTED_FORM_TAGS,
        NESTED_HEADER_TAGS,
        UNEXPECTED_NULL_CHARACTER,
        UNEXPECTED_TOKEN_AFTER_BODY,
        UNEXPECTED_TOKEN_AFTER_HTML,
        XMLNS_ATTRIBUTE_NAMESPACE_MISMATCH,
    };

    [[nodiscard]] inline constexpr std::string_view
    toString(TreeConstructionError treeConstructionError) noexcept {
        switch (treeConstructionError) {
            case TreeConstructionError::ABRUPT_END_OF_FILE: return "abrupt-end-of-file";
            case TreeConstructionError::ANOTHER_BODY_ELEMENT_IN_BODY: return "another-body-element-in-body";
            case TreeConstructionError::ANOTHER_HEAD_ELEMENT_IN_HEAD: return "another-head-element-in-head";
            case TreeConstructionError::DOCTYPE_NAME_NOT_HTML: return "doctype-name-not-html";
            case TreeConstructionError::DOCTYPE_HAS_PUBLIC_IDENTIFIER: return "doctype-has-public-identifier";
            case TreeConstructionError::DOCTYPE_HAS_SYSTEM_IDENTIFIER: return "doctype-has-system-identifier";
            case TreeConstructionError::DOCTYPE_OUTSIDE_INITIAL_INSERTION_MODE: return "doctype-outside-initial-insertion-mode";
            case TreeConstructionError::END_TAG_WITHOUT_MATCHING_START_TAG: return "end-tag-without-matching-start-tag";
            case TreeConstructionError::EOF_IN_TEXT_INSERTION_MODE: return "eof-in-text-insertion-mode";
            case TreeConstructionError::EXPECTED_DOCTYPE: return "expected-doctype";
            case TreeConstructionError::FORM_CLOSED_INCORRECTLY: return "form-closed-incorrectly";
            case TreeConstructionError::FRAMESET_IN_BODY: return "frameset-in-body";
            case TreeConstructionError::ILLEGAL_BODY_END_TAG: return "illegal-body-end-tag";
            case TreeConstructionError::ILLEGAL_HTML_END_TAG: return "illegal-html-end-tag";
            case TreeConstructionError::ILLEGAL_ELEMENT_BETWEEN_HEAD_AND_BODY: return "illegal-element-between-head-and-body";
            case TreeConstructionError::ILLEGAL_END_TAG_IN_HEAD: return "illegal-end-tag-in-head";
            case TreeConstructionError::ILLEGAL_START_TAG_IN_HTML_ELEMENT: return "illegal-start-tag-in-html-element";
            case TreeConstructionError::ILLEGAL_TAG_IN_BODY_ELEMENT: return "illegal-tag-in-body-element";
            case TreeConstructionError::ILLEGAL_TAG_IN_HTML_ELEMENT: return "illegal-tag-in-html-element";
            case TreeConstructionError::MISPLACED_P_END_TAG: return "misplaced-p-end-tag";
            case TreeConstructionError::MISSING_END_TAGS: return "missing-end-tags";
            case TreeConstructionError::MULTIPLE_HEAD_ELEMENTS: return "multiple-head-elements";
            case TreeConstructionError::NESTED_FORM_TAGS: return "nested-form-tags";
            case TreeConstructionError::NESTED_HEADER_TAGS: return "nested-header-tags";
            case TreeConstructionError::UNEXPECTED_NULL_CHARACTER: return "unexpected-null-character";
            case TreeConstructionError::UNEXPECTED_TOKEN_AFTER_BODY: return "unexpected-token-after-body";
            case TreeConstructionError::UNEXPECTED_TOKEN_AFTER_HTML: return "unexpected-token-after-html";
            case TreeConstructionError::XMLNS_ATTRIBUTE_NAMESPACE_MISMATCH: return "xmlns-attribute-namespace-mismatch";
        }
        return "(invalid)";
    }

} // namespace html

