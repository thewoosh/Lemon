/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/HTML/Parser/TreeConstruction/Commons.hpp"

#include <algorithm>
#include <string_view>

#include "Include/text/UnicodeBlocks/C0ControlsAndBasicLatin.hpp"

#include "Source/Base/Assert.hpp"
#include "Source/HTML/Elements/HTMLFormElement.hpp"
#include "Source/Infra/Namespaces.hpp"

namespace html {

    static void
    copyAttributesIfNotPresent(dom::Element *element, const Token &token) {
        const auto &attributes = element->attributes();

        for (const auto &attrib : token.asStartTag().attributeList) {
            const auto it = std::find_if(std::cbegin(attributes), std::cend(attributes), [&] (const auto &entry) {
                return entry.localName() == attrib.name();
            });

            if (it == std::cend(attributes)) {
                dom::appendAttributeToElement(element, attrib.name(), attrib.value());
            }
        }
    }

    /**
     * This big kind-of-algorithm is seen a dozen times, but unfortunately not
     * separated or named, hence the weird function name.
     */
    [[nodiscard]] static constexpr bool
    isHTMLTagNameSpeciallyClosing(std::string_view name, bool includeP) {
        return name == "dd" || name == "dt" || name == "li" || name == "optgroup" || name == "option" || name == "rb"
            || name == "rp" || name == "rt" || name == "rtc" || name == "tbody" || name == "td" || name == "tfoot"
            || name == "th" || name == "thead" || name == "tr" || name == "body" || name == "html"
            || (includeP && name != "p");
    }

    [[nodiscard]] static bool
    canStackOfOpenElementsCloseNicely(const StackOfOpenElements &stackOfOpenElements) noexcept {
        return std::all_of(std::cbegin(std::data(stackOfOpenElements)), std::cend(std::data(stackOfOpenElements)),
                           [&] (const auto &element) {
                                return element->namespaceURI() != infra::namespaces::HTML
                                    || isHTMLTagNameSpeciallyClosing(element->localName(), true);
                           }
        );
    }

    [[nodiscard]] bool
    isElementSpecial(const dom::Element *element) noexcept {
        if (element->namespaceURI() == infra::namespaces::HTML) {
            static constexpr const std::array<std::string_view, 82> specials{
                "address", "applet", "area", "article", "aside", "base", "basefont", "bgsound", "blockquote", "body",
                "br", "button", "caption", "center", "col", "colgroup", "dd", "details", "dir", "div", "dl", "dt",
                "embed", "fieldset", "figcaption", "figure", "footer", "form", "frame", "frameset", "h1", "h2", "h3",
                "h4", "h5", "h6", "head", "header", "hgroup", "hr", "html", "iframe", "img", "input", "keygen", "li",
                "link", "listing", "main", "marquee", "menu", "meta", "nav", "noembed", "noframes", "noscript",
                "object", "ol", "p", "param", "plaintext", "pre", "script", "section", "select", "source", "style",
                "summary", "table", "tbody", "td", "template", "textarea", "tfoot", "th", "thead", "title", "tr",
                "track", "ul", "wbr", "xmp"
            };

            return std::find(std::cbegin(specials), std::cend(specials), element->localName());
        }

        if (element->namespaceURI() == infra::namespaces::MathML) {
            static constexpr const std::array<std::string_view, 82> specials{
                "mi", "mo", "mn", "ms", "mtext", "annotation-xml"
            };

            return std::find(std::cbegin(specials), std::cend(specials), element->localName());
        }

        if (element->namespaceURI() == infra::namespaces::SVG) {
            return element->localName() == "foreignObject" || element->localName() == "desc" || element->localName() == "title";
        }

        return false;
    }

    DEFINE_INSERTION_MODE(IN_BODY) {
        if (token.type() == Token::Type::CHARACTER) {
            if (token.asCharacter() == text::NULL_CHARACTER) {
                PARSE_ERROR(UNEXPECTED_NULL_CHARACTER);
                return true;
            }

            reconstructTheActiveFormattingElements();
            insertACharacter(this, token.asCharacter());

            if (!isWhitespace(token)) {
                m_document->setFramesetFlagToNotOK();
            }

            return true;
        }

        if (token.type() == Token::Type::COMMENT) {
            insertAComment(this, token.asComment());
            return true;
        }

        if (token.type() == Token::Type::DOCTYPE) {
            PARSE_ERROR(DOCTYPE_OUTSIDE_INITIAL_INSERTION_MODE);
            return true;
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagName == "html") {
            PARSE_ERROR(ILLEGAL_TAG_IN_BODY_ELEMENT);
            if (m_stackOfOpenElements.hasTemplateElement())
                return true;

            copyAttributesIfNotPresent(m_stackOfOpenElements.topmost(), token);
            return true;
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagNameIsAnyOf("base", "basefont", "bgsound",
                "link", "meta", "noframes", "script", "style", "template", "title")) {
            return processTheRulesFor(InsertionMode::IN_HEAD, std::move(token));
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagName == "body") {
            PARSE_ERROR(ANOTHER_BODY_ELEMENT_IN_BODY);
            if (std::size(std::data(m_stackOfOpenElements)) == 1 || m_stackOfOpenElements.hasTemplateElement())
                return true; // ignore
            const auto &secondElement = m_stackOfOpenElements.data()[1];
            if (secondElement->localName() != "body" || secondElement->namespaceURI() != infra::namespaces::HTML)
                return true;
            m_document->setFramesetFlagToNotOK();
            copyAttributesIfNotPresent(secondElement, token);
            return true;
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagName == "template") {
            PARSE_ERROR(FRAMESET_IN_BODY);

            if (std::size(std::data(m_stackOfOpenElements)) == 1 || m_stackOfOpenElements.hasTemplateElement())
                return true; // ignore
            const auto &secondElement = m_stackOfOpenElements.data()[1];
            if (secondElement->localName() != "body" || secondElement->namespaceURI() != infra::namespaces::HTML)
                return true;

            if (!m_document->framesetOK())
                return true;

            while (std::size(std::data(m_stackOfOpenElements)) > 1) {
                m_stackOfOpenElements.popOffCurrentNode();
            }

            insertAnHTMLElement(this, std::move(token));
            switchTheInsertionModeTo(InsertionMode::IN_FRAMESET);
            return true;
        }

        if (token.type() == Token::Type::END_OF_FILE) {
            if (!std::empty(m_stackOfTemplateInsertionModes)) {
                return processTheRulesFor(InsertionMode::IN_TEMPLATE, std::move(token));
            }

            if (!canStackOfOpenElementsCloseNicely(m_stackOfOpenElements)) {
                PARSE_ERROR(ABRUPT_END_OF_FILE);
            }

            return stopParsing();
        }

        if (token.type() == Token::Type::END_TAG && token.asEndTag().tagName == "body") {
            if (!m_stackOfOpenElements.hasInHTMLElementInScope("body")) {
                PARSE_ERROR(ILLEGAL_BODY_END_TAG);
                return true;
            }

            if (!canStackOfOpenElementsCloseNicely(m_stackOfOpenElements)) {
                PARSE_ERROR(ABRUPT_END_OF_FILE);
            }

            switchTheInsertionModeTo(InsertionMode::AFTER_BODY);
            return true;
        }

        if (token.type() == Token::Type::END_TAG && token.asEndTag().tagName == "html") {
            if (!m_stackOfOpenElements.hasInHTMLElementInScope("html")) {
                PARSE_ERROR(ILLEGAL_HTML_END_TAG);
                return true;
            }

            if (!canStackOfOpenElementsCloseNicely(m_stackOfOpenElements)) {
                PARSE_ERROR(ABRUPT_END_OF_FILE);
            }

            switchTheInsertionModeTo(InsertionMode::AFTER_BODY);
            return reprocessToken(std::move(token));
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagNameIsAnyOf("address", "article", "aside",
                "blockquote", "center", "details", "dialog", "dir", "div", "dl", "fieldset", "figcaption", "figure",
                "footer", "header", "hgroup", "main", "menu", "nav", "ol", "p", "section", "summary", "ul")) {
            if (m_stackOfOpenElements.hasInHTMLElementInScope("p"))
                closeAPElement();
            insertAnHTMLElement(this, std::move(token));
            return true;
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagNameIsAnyOf("h1", "h2", "h3",
                                                                                        "h4", "h5", "h6")) {
            if (m_stackOfOpenElements.hasInHTMLElementInScope("p"))
                closeAPElement();
            if (currentElement()->namespaceURI() == infra::namespaces::HTML && (currentElement()->localName() == "h1"
                    || currentElement()->localName() == "h2" || currentElement()->localName() == "h3"
                    || currentElement()->localName() == "h4" || currentElement()->localName() == "h5"
                    || currentElement()->localName() == "h6")) {
                PARSE_ERROR(NESTED_HEADER_TAGS);
                m_stackOfOpenElements.popOffCurrentNode();
            }
            insertAnHTMLElement(this, std::move(token));
            return true;
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagNameIsAnyOf("pre", "listing")) {
            if (m_stackOfOpenElements.hasInHTMLElementInScope("p"))
                closeAPElement();
            // TODO I don't know how to access the next token, when the
            //      tokenizer should dispatch every token immediately?
            // https://html.spec.whatwg.org/multipage/parsing.html#next-token
            LEMON_ASSERT_NOT_REACHED_OR_RETURN(false);
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagName == "form") {
            const bool hasTemplate = m_stackOfOpenElements.hasInHTMLElementInScope("template");

            if (m_formElementPointer != nullptr && !hasTemplate) {
                PARSE_ERROR(NESTED_FORM_TAGS);
                return true;
            }

            if (m_stackOfOpenElements.hasInHTMLElementInScope("p"))
                closeAPElement();

            auto *element = insertAnHTMLElement(this, std::move(token));
            if (!hasTemplate) {
                m_formElementPointer = static_cast<HTMLFormElement *>(element);
            }

            return true;
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagName == "li") {
            m_document->setFramesetFlagToNotOK();

            auto nodeIndex = m_stackOfOpenElements.data().size() - 1;
            auto *node = currentElement();

            while (true) {
                if (node->localName() == "li" && node->namespaceURI() == infra::namespaces::HTML) {
                    generateImpliedEndTags("li");
                    if (currentElement()->localName() != "li" || node->namespaceURI() == infra::namespaces::HTML) {
                        PARSE_ERROR(MISSING_END_TAGS);
                    }
                    static_cast<void>(m_stackOfOpenElements.popUntilElement(node));
                    break;
                }

                if (isElementSpecial(node) && (node->namespaceURI() != infra::namespaces::HTML ||
                        (node->localName() == "address" && node->localName() != "div" && node->localName() != "p"))) {
                    break;
                }

                if (nodeIndex == 0)
                    break;

                node = m_stackOfOpenElements.data()[--nodeIndex];
            }
            if (m_stackOfOpenElements.hasInHTMLElementInScope("p"))
                closeAPElement();
            insertAnHTMLElement(this, std::move(token));
            return true;
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagNameIsAnyOf("dd", "dt")) {
            const std::string_view tagName = token.asStartTag().tagName;
            m_document->setFramesetFlagToNotOK();

            auto nodeIndex = m_stackOfOpenElements.data().size() - 1;
            auto *node = currentElement();

            while (true) {
                if (node->localName() == tagName && node->namespaceURI() == infra::namespaces::HTML) {
                    generateImpliedEndTags(tagName);
                    if (currentElement()->localName() != tagName || node->namespaceURI() == infra::namespaces::HTML) {
                        PARSE_ERROR(MISSING_END_TAGS);
                    }
                    static_cast<void>(m_stackOfOpenElements.popUntilElement(node));
                    break;
                }

                if (isElementSpecial(node) && (node->namespaceURI() != infra::namespaces::HTML ||
                        (node->localName() == "address" && node->localName() != "div" && node->localName() != "p"))) {
                    break;
                }

                if (nodeIndex == 0)
                    break;

                node = m_stackOfOpenElements.data()[--nodeIndex];
            }
            if (m_stackOfOpenElements.hasInHTMLElementInScope("p"))
                closeAPElement();
            insertAnHTMLElement(this, std::move(token));
            return true;
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagName == "plaintext") {
            LEMON_ASSERT_NOT_REACHED_OR_RETURN(false);
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagName == "button") {
            LEMON_ASSERT_NOT_REACHED_OR_RETURN(false);
        }

        if (token.type() == Token::Type::END_TAG && token.asEndTag().tagNameIsAnyOf( "address", "article", "aside",
                                                                                     "blockquote", "button", "center",
                                                                                     "details", "dialog", "dir", "div",
                                                                                     "dl", "fieldset", "figcaption",
                                                                                     "figure", "footer", "header",
                                                                                     "hgroup", "listing", "main", "menu",
                                                                                     "nav", "ol", "pre", "section",
                                                                                     "summary", "ul")) {
            if (!m_stackOfOpenElements.hasInHTMLElementInScope(token.asEndTag().tagName)) {
                PARSE_ERROR(END_TAG_WITHOUT_MATCHING_START_TAG);
                return true;
            }

            generateImpliedEndTags();

            if (currentElement()->namespaceURI() != infra::namespaces::HTML || currentElement()->localName() != token.asEndTag().tagName) {
                PARSE_ERROR(MISSING_END_TAGS);
            }

            while (!std::empty(std::data(m_stackOfOpenElements))) {
                const auto *node = m_stackOfOpenElements.popOffCurrentNode();
                const bool isCorrectEndTag = node->namespaceURI() == infra::namespaces::HTML &&
                        node->localName() == token.asEndTag().tagName;
                if (isCorrectEndTag)
                    break;
            }

            return true;
        }

        if (token.type() == Token::Type::END_TAG && token.asEndTag().tagNameIsAnyOf("address", "article", "aside",
                "blockquote", "button", "center", "details", "dialog", "dir", "div", "dl", "fieldset", "figcaption",
                "figure", "footer", "header", "hgroup", "listing", "main", "menu", "nav", "ol", "pre", "section",
                "summary", "ul")) {
            if (!m_stackOfOpenElements.hasInHTMLElementInScope(token.asEndTag().tagName)) {
                PARSE_ERROR(END_TAG_WITHOUT_MATCHING_START_TAG);
                return true;
            }

            generateImpliedEndTags();
            if (currentElement()->namespaceURI() != infra::namespaces::HTML
                    || currentElement()->localName() != token.asEndTag().tagName) {
                PARSE_ERROR(MISSING_END_TAGS);
            }

            while (!std::empty(std::data(m_stackOfOpenElements))) {
                auto *current = currentElement();
                m_stackOfOpenElements.popOffCurrentNode();
                if (current->localName() == token.asEndTag().tagName
                        && current->namespaceURI() == infra::namespaces::HTML) {
                    break;
                }
            }

            return true;
        }

        if (token.type() == Token::Type::END_TAG && token.asEndTag().tagName == "form") {
            if (!m_stackOfOpenElements.hasInHTMLElementInScope("template")) {
                auto *node = m_formElementPointer;
                m_formElementPointer = nullptr;
                if (node == nullptr || !m_stackOfOpenElements.hasElementInScope(node)) {
                    PARSE_ERROR(FORM_CLOSED_INCORRECTLY);
                    return true;
                }

                generateImpliedEndTags();
                if (currentElement() != node)
                    PARSE_ERROR(MISSING_END_TAGS);

                bool status = m_stackOfOpenElements.popUntilElement(node);
                LEMON_ASSERT(status);
                static_cast<void>(status);
            } else {
                LEMON_ASSERT_NOT_REACHED_OR_RETURN(false);
            }

            return true;
        }

        if (token.type() == Token::Type::END_TAG && token.asEndTag().tagName == "p") {
            if (!m_stackOfOpenElements.hasInHTMLElementInScope("p")) {
                PARSE_ERROR(MISPLACED_P_END_TAG);
                insertAnHTMLElement(this, Token::createStartTag("p"));
            }
            closeAPElement();
            return true;
        }

        if (token.type() == Token::Type::END_TAG && token.asEndTag().tagName == "li") {
            if (!m_stackOfOpenElements.hasInHTMLElementInScope("li")) {
                PARSE_ERROR(END_TAG_WITHOUT_MATCHING_START_TAG);
                return true;
            }

            generateImpliedEndTags("li");
            if (currentElement()->localName() != "li" || currentElement()->namespaceURI() != infra::namespaces::HTML) {
                PARSE_ERROR(MISSING_END_TAGS);
            }

            while (!std::empty(std::data(m_stackOfOpenElements))) {
                const auto *node = m_stackOfOpenElements.popOffCurrentNode();
                const bool isCorrectEndTag = node->namespaceURI() == infra::namespaces::HTML &&
                                             node->localName() == token.asEndTag().tagName;
                if (isCorrectEndTag)
                    break;
            }

            return true;
        }

        if (token.type() == Token::Type::END_TAG && token.asEndTag().tagNameIsAnyOf("dd", "dt")) {
            if (!m_stackOfOpenElements.hasInHTMLElementInScope(token.asEndTag().tagName)) {
                PARSE_ERROR(END_TAG_WITHOUT_MATCHING_START_TAG);
                return true;
            }

            generateImpliedEndTags(token.asEndTag().tagName);
            if (currentElement()->localName() != token.asEndTag().tagName || currentElement()->namespaceURI() != infra::namespaces::HTML) {
                PARSE_ERROR(MISSING_END_TAGS);
            }

            while (!std::empty(std::data(m_stackOfOpenElements))) {
                const auto *node = m_stackOfOpenElements.popOffCurrentNode();
                const bool isCorrectEndTag = node->namespaceURI() == infra::namespaces::HTML &&
                                             node->localName() == token.asEndTag().tagName;
                if (isCorrectEndTag)
                    break;
            }

            return true;
        }

        if (token.type() == Token::Type::END_TAG && token.asEndTag().tagNameIsAnyOf("h1", "h2", "h3",
                                                                                    "h4", "h5", "h6")) {
            if (!m_stackOfOpenElements.hasOneOfHTMLElementInScope("h1", "h2", "h3", "h4", "h5", "h6")) {
                PARSE_ERROR(END_TAG_WITHOUT_MATCHING_START_TAG);
                return true;
            }

            generateImpliedEndTags();
            if (std::empty(std::data(m_stackOfOpenElements))
                    || currentElement()->namespaceURI() != infra::namespaces::HTML
                    || currentElement()->localName() != token.asEndTag().tagName) {
                PARSE_ERROR(MISSING_END_TAGS);
            }

            while (!std::empty(std::data(m_stackOfOpenElements))) {
                auto *element = m_stackOfOpenElements.popOffCurrentNode();
                if (element->namespaceURI() == infra::namespaces::HTML
                        && element->localName() == token.asEndTag().tagName) {
                    break;
                }
            }

            return true;
        }

//        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagName == "a") {
//            LEMON_ASSERT_NOT_REACHED_OR_RETURN(false);
//        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagNameIsAnyOf("b", "big", "code", "em",
                "font", "i", "s", "small", "strike", "strong", "tt", "u")) {
            LEMON_ASSERT_NOT_REACHED_OR_RETURN(false);
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagName == "nobr") {
            LEMON_ASSERT_NOT_REACHED_OR_RETURN(false);
        }

        if (token.type() == Token::Type::END_TAG && token.asEndTag().tagNameIsAnyOf("b", "big", "code", "em", "font",
                "i", "s", "small", "strike", "strong", "tt", "u")) {
            LEMON_ASSERT_NOT_REACHED_OR_RETURN(false);
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagNameIsAnyOf("applet", "marquee",
                                                                                        "object")) {
            LEMON_ASSERT_NOT_REACHED_OR_RETURN(false);
        }

        if (token.type() == Token::Type::END_TAG && token.asEndTag().tagNameIsAnyOf("applet", "marquee", "object")) {
            LEMON_ASSERT_NOT_REACHED_OR_RETURN(false);
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagName == "table") {
            LEMON_ASSERT_NOT_REACHED_OR_RETURN(false);
        }

        if (token.type() == Token::Type::END_TAG && token.asEndTag().tagName == "br") {
            LEMON_ASSERT_NOT_REACHED_OR_RETURN(false);
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagNameIsAnyOf("area", "br", "embed", "img",
                                                                                        "keygen", "wbr")) {
            reconstructTheActiveFormattingElements();
            insertAnHTMLElement(this, std::move(token));
            m_stackOfOpenElements.popOffCurrentNode();
            // TODO acknowledge self closing flag
            m_document->setFramesetFlagToNotOK();
            return true;
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagName == "input") {
            reconstructTheActiveFormattingElements();
            auto *element = insertAnHTMLElement(this, std::move(token));
            m_stackOfOpenElements.popOffCurrentNode();

            // TODO acknowledge self-closing flag

            auto *attribute = element->firstAttribute("type");
            if (attribute == nullptr || *attribute == "hidden") {
                m_document->setFramesetFlagToNotOK();
            }

            return true;
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagNameIsAnyOf("param", "source", "track")) {
            LEMON_ASSERT_NOT_REACHED_OR_RETURN(false);
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagName == "hr") {
            if (m_stackOfOpenElements.hasInHTMLElementInScope("p"))
                closeAPElement();
            insertAnHTMLElement(this, std::move(token));
            m_stackOfOpenElements.popOffCurrentNode();
            // TODO acknowledge self closing flag
            m_document->setFramesetFlagToNotOK();
            return true;
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagName == "image") {
            LEMON_ASSERT_NOT_REACHED_OR_RETURN(false);
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagName == "textarea") {
            LEMON_ASSERT_NOT_REACHED_OR_RETURN(false);
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagName == "xmp") {
            LEMON_ASSERT_NOT_REACHED_OR_RETURN(false);
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagName == "iframe") {
            LEMON_ASSERT_NOT_REACHED_OR_RETURN(false);
        }

        if (token.type() == Token::Type::START_TAG && (token.asStartTag().tagName == "noembed"
                || (m_document->scriptingFlag() && token.asStartTag().tagName == "noscript"))) {
            LEMON_ASSERT_NOT_REACHED_OR_RETURN(false);
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagName == "select") {
            LEMON_ASSERT_NOT_REACHED_OR_RETURN(false);
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagNameIsAnyOf("optgroup", "option")) {
            LEMON_ASSERT_NOT_REACHED_OR_RETURN(false);
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagNameIsAnyOf("rb", "rtc")) {
            LEMON_ASSERT_NOT_REACHED_OR_RETURN(false);
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagNameIsAnyOf("rp", "rt")) {
            LEMON_ASSERT_NOT_REACHED_OR_RETURN(false);
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagName == "math") {
            LEMON_ASSERT_NOT_REACHED_OR_RETURN(false);
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagName == "svg") {
            LEMON_ASSERT_NOT_REACHED_OR_RETURN(false);
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagNameIsAnyOf("caption", "col", "colgroup",
                "frame", "head", "tbody", "td", "tfoot", "th", "thead", "tr")) {
            LEMON_ASSERT_NOT_REACHED_OR_RETURN(false);
        }

        // Any other start tag
        if (token.type() == Token::Type::START_TAG) {
            reconstructTheActiveFormattingElements();
            insertAnHTMLElement(this, std::move(token));
            return true;
        }

        // Any other end tag
        if (token.type() == Token::Type::END_TAG) {
            for (std::size_t i = std::size(m_stackOfOpenElements.data()); i > 0; --i) {
                auto *node = m_stackOfOpenElements.data()[i - 1];

                if (node->namespaceURI() == infra::namespaces::HTML && node->localName() == token.asEndTag().tagName) {
                    generateImpliedEndTags(node->localName());
                    if (node != currentElement()) {
                        PARSE_ERROR(MISSING_END_TAGS);
                        // ignore the token
                        return true;
                    }

                    dom::Element *justRemoved;
                    do {
                        justRemoved = m_stackOfOpenElements.data().back();
                        m_stackOfOpenElements.data().pop_back();
                    } while (justRemoved != node);

                    return true;
                }

                if (isElementSpecial(node)) {
                    // FIXME: this should probably be a different error, but what?
                    PARSE_ERROR(MISSING_END_TAGS);
                    return true;
                }
            }

            return true;
        }

        LEMON_ASSERT_NOT_REACHED_OR_RETURN(false);
    }

} // namespace html
