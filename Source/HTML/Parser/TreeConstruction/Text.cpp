/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/HTML/Parser/TreeConstruction/Commons.hpp"

#include "Source/Base/Assert.hpp"

namespace html {

    DEFINE_INSERTION_MODE(TEXT) {
        if (token.type() == Token::Type::CHARACTER) {
            insertACharacter(this, token.asCharacter());
            return true;
        }

        if (token.type() == Token::Type::END_OF_FILE) {
            PARSE_ERROR(EOF_IN_TEXT_INSERTION_MODE);
            // TODO scripts
            m_stackOfOpenElements.popOffCurrentNode();
            switchTheInsertionModeTo(m_originalInsertionMode.value());
            m_originalInsertionMode.reset();
            return true;
        }

        if (token.type() == Token::Type::END_TAG && token.asEndTag().tagName == "script") {
            // TODO
            LEMON_ASSERT_NOT_REACHED_OR_RETURN(false);
        }

        LEMON_ASSERT(token.type() == Token::Type::END_TAG);
        m_stackOfOpenElements.popOffCurrentNode();
        switchTheInsertionModeTo(m_originalInsertionMode.value());
        m_originalInsertionMode.reset();
        return true;
    }

} // namespace html
