/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/HTML/Parser/TreeConstruction/Commons.hpp"

#include "Source/Infra/Namespaces.hpp"

namespace html {

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#the-after-after-body-insertion-mode
     */
    DEFINE_INSERTION_MODE(AFTER_AFTER_BODY) {
        if (token.type() == Token::Type::COMMENT) {
            insertAComment(this, token.asComment(), InsertionPosition{
                m_document,
                m_document->children().end()
            });
            return true;
        }

        if (token.type() == Token::Type::DOCTYPE || isWhitespace(token)
                || (token.type() == Token::Type::START_TAG && token.asStartTag().tagName == "html")) {
            return processTheRulesFor(InsertionMode::IN_BODY, std::move(token));
        }

        if (token.type() == Token::Type::END_OF_FILE) {
            return stopParsing();
        }

        PARSE_ERROR(UNEXPECTED_TOKEN_AFTER_HTML);
        switchTheInsertionModeTo(InsertionMode::IN_BODY);
        return reprocessToken(std::move(token));
    }

} // namespace html
