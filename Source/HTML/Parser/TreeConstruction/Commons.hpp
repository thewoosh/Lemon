/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include "Include/text/UnicodeBlocks/C0ControlsAndBasicLatin.hpp"

#include <optional>
#include <string_view>

#include "Source/DOM/Document.hpp"
#include "Source/DOM/Element.hpp"
#include "Source/HTML/Elements/HTMLElement.hpp"
#include "Source/HTML/Parser/TreeConstructor.hpp"
#include "Source/HTML/Parser/TreeConstruction/Error.hpp"

#define DEFINE_INSERTION_MODE(name) \
    [[nodiscard]] bool \
    TreeConstructor::insertionModeImplementation##name(Token &&token) noexcept

#define PARSE_ERROR(name) \
    do { \
        LOG_ERROR("Parse Error: {}", toString(TreeConstructionError::name)); \
    } while (0)

namespace html {

    [[nodiscard]] inline constexpr bool
    isWhitespace(const Token &token) {
        if (token.type() != Token::Type::CHARACTER)
            return false;
        switch (token.asCharacter()) {
            case text::CHARACTER_TABULATION:
            case text::LINE_FEED:
            case text::FORM_FEED:
            case text::SPACE:
                return true;
            default:
                return false;
        }
    }

    struct InsertionPosition {
        dom::Node *node;
        std::vector<std::unique_ptr<dom::Node>>::iterator position;
    };


    [[nodiscard]] InsertionPosition
    appropriatePlaceForInsertingANode(TreeConstructor *, dom::Node *overrideTarget=nullptr) noexcept;

    [[nodiscard]] std::unique_ptr<dom::Element>
    createAnElementForTheToken(TreeConstructor *, const Token::StartTagData &, std::string_view givenNamespace,
                               dom::Node *givenParent) noexcept;

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#insert-a-character
     */
    void
    insertACharacter(TreeConstructor *, char32_t) noexcept;

    void
    insertAComment(TreeConstructor *, std::string_view data,
                   std::optional<InsertionPosition> &&position=std::nullopt) noexcept;

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#insert-a-foreign-element
     */
    dom::Element *
    insertAForeignElement(TreeConstructor *, Token &&token, std::string_view givenNamespace) noexcept;

    /**
     * https://html.spec.whatwg.org/multipage/parsing.html#insert-an-html-element
     */
    HTMLElement *
    insertAnHTMLElement(TreeConstructor *, Token &&token) noexcept;

} // namespace html
