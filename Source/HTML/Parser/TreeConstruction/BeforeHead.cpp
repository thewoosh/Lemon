/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/HTML/Parser/TreeConstruction/Commons.hpp"

#include "Source/Infra/Namespaces.hpp"

namespace html {

    DEFINE_INSERTION_MODE(BEFORE_HEAD) {
        if (isWhitespace(token))
            return true;

        if (token.type() == Token::Type::COMMENT) {
            insertAComment(this, token.asComment());
            return true;
        }

        if (token.type() == Token::Type::DOCTYPE) {
            PARSE_ERROR(DOCTYPE_OUTSIDE_INITIAL_INSERTION_MODE);
            return true;
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagName == "html") {
            return processTheRulesFor(InsertionMode::IN_BODY, std::move(token));
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagName == "head") {
            m_headElementPointer = insertAnHTMLElement(this, std::move(token));

            switchTheInsertionModeTo(InsertionMode::IN_HEAD);
            return true;
        }

        if (token.type() == Token::Type::END_TAG && !token.asEndTag().tagNameIsAnyOf("head", "body", "html", "br")) {
            PARSE_ERROR(ILLEGAL_TAG_IN_HTML_ELEMENT);
            return true;
        }

        auto headElement = insertAnHTMLElement(this, Token::createStartTag("head"));
        m_headElementPointer = headElement;
        switchTheInsertionModeTo(InsertionMode::IN_HEAD);
        return reprocessToken(std::move(token));
    }

} // namespace html
