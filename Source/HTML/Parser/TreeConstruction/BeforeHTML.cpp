/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/HTML/Parser/TreeConstruction/Commons.hpp"

#include "Source/HTML/Elements/HTMLHtmlElement.hpp"
#include "Source/Infra/Namespaces.hpp"

namespace html {

    DEFINE_INSERTION_MODE(BEFORE_HTML) {
        if (token.type() == Token::Type::DOCTYPE) {
            PARSE_ERROR(DOCTYPE_OUTSIDE_INITIAL_INSERTION_MODE);
            return true;
        }

        if (token.type() == Token::Type::COMMENT) {
            insertAComment(this, token.asComment(), InsertionPosition{m_document, m_document->children().end()});
            return true;
        }

        if (isWhitespace(token)) {
            return true;
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagName == "html") {
            auto newElement = createAnElementForTheToken(this, token.asStartTag(), infra::namespaces::HTML, m_document);
            dom::Element *element = newElement.get();
            m_document->appendChild(std::move(newElement));

            m_stackOfOpenElements.push(element);
            switchTheInsertionModeTo(InsertionMode::BEFORE_HEAD);
            return true;
        }

        if (token.type() == Token::Type::END_TAG && !token.asEndTag().tagNameIsAnyOf("head", "body", "html", "br")) {
            PARSE_ERROR(ILLEGAL_START_TAG_IN_HTML_ELEMENT);
            return true;
        }

        m_document->appendChild(std::make_unique<HTMLHtmlElement>(m_document, "html"));
        m_stackOfOpenElements.push(static_cast<dom::Element *>(m_document->children().back().get()));
        switchTheInsertionModeTo(InsertionMode::BEFORE_HEAD);
        return reprocessToken(std::move(token));
    }

} // namespace html
