/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/HTML/Parser/TreeConstruction/Commons.hpp"

#include <iterator>

#include "Source/DOM/Comment.hpp"
#include "Source/DOM/Text.hpp"
#include "Source/Infra/Namespaces.hpp"
#include "Source/Text/Encoding/UTF8Encoder.hpp"

namespace html {

    InsertionPosition
    appropriatePlaceForInsertingANode(TreeConstructor *treeConstructor, dom::Node *overrideTarget) noexcept {
        dom::Node *node = overrideTarget;
        if (node == nullptr) {
            node = treeConstructor->currentElement();
        }

        // multipage/parsing.html#appropriate-place-for-inserting-a-node
        return {node, node->children().end()};
    }

    // multipage/parsing.html#create-an-element-for-the-token
    std::unique_ptr<dom::Element>
    createAnElementForTheToken(TreeConstructor *treeConstructor,
                               const Token::StartTagData &tag,
                               std::string_view givenNamespace,
                               dom::Node *givenParent) noexcept {
        const auto &localName = tag.tagName;
        const auto is = tag.attributeList.find("is");
        // TODO custom element definitions
        // TODO scripts

        auto element = dom::createElement(treeConstructor->document(), givenParent, std::string(localName), givenNamespace,
                                          std::nullopt, is /*, set synchronous flag when scripts are enabled*/);
        for (const auto &attribute : tag.attributeList) {
            dom::appendAttributeToElement(element.get(), attribute.name(), attribute.value());
        }

        // TODO scripts

        for (const auto &attribute : element->attributes()) {
            if (attribute.localName() == "xmlns" && attribute.namespaceURI() == infra::namespaces::XMLNS
                    && attribute.value() != element->namespaceURI()) {
                PARSE_ERROR(XMLNS_ATTRIBUTE_NAMESPACE_MISMATCH);
            }

            if (attribute.localName() == "xmlns:xlink" && attribute.namespaceURI() == infra::namespaces::XMLNS
                    && attribute.value() != infra::namespaces::XLink) {
                PARSE_ERROR(XMLNS_ATTRIBUTE_NAMESPACE_MISMATCH);
            }
        }

        element->reset();

        // TODO form-associated element algorithm

        return element;
    }

    void
    insertACharacter(TreeConstructor *treeConstructor, char32_t character) noexcept {
        const auto adjustedInsertionLocation = appropriatePlaceForInsertingANode(treeConstructor);
        if (adjustedInsertionLocation.node->nodeType() == dom::NodeType::DOCUMENT)
            return;

        if (!std::empty(adjustedInsertionLocation.node->children())
                && adjustedInsertionLocation.position != std::begin(adjustedInsertionLocation.node->children())
                && std::prev(adjustedInsertionLocation.position)->get()->nodeType() == dom::NodeType::TEXT) {
            auto *textNode = static_cast<dom::Text *>(std::prev(adjustedInsertionLocation.position)->get());
            text::encoding::appendCodePoint(textNode->data(), character);
            return;
        }

        std::string string;
        text::encoding::appendCodePoint(string, character);

        adjustedInsertionLocation.node->children().insert(adjustedInsertionLocation.position,
                std::make_unique<dom::Text>(adjustedInsertionLocation.node, std::move(string)));
    }

    void
    insertAComment(TreeConstructor *treeConstructor, std::string_view data,
                   std::optional<InsertionPosition> &&position) noexcept {
        InsertionPosition adjustedInsertionLocation = position.has_value()
                ? position.value() : appropriatePlaceForInsertingANode(treeConstructor, treeConstructor->document());

        adjustedInsertionLocation.node->children().insert(adjustedInsertionLocation.position,
                std::make_unique<dom::Comment>(adjustedInsertionLocation.node, std::string(data)));
    }

    dom::Element *
    insertAForeignElement(TreeConstructor *treeConstructor, Token &&token, std::string_view givenNamespace) noexcept {
        auto adjustedInsertionLocation = appropriatePlaceForInsertingANode(treeConstructor);
        auto newElement = createAnElementForTheToken(treeConstructor, token.asStartTag(), givenNamespace,
                                                     adjustedInsertionLocation.node);
        dom::Element *element = newElement.get();

        // TODO check for whether or not the element can be inserted
        {
            // TODO reactions
            adjustedInsertionLocation.node->appendChild(std::move(newElement));
            // TODO reactions
        }
        // TODO otherwise we should store the element in some dead element list,
        //      since otherwise the created element won't be part of any parent
        //      outside this function, meaning a dangling pointer is passed
        //      around.

        treeConstructor->stackOfOpenElements().push(element);
        return element;
    }

    HTMLElement *
    insertAnHTMLElement(TreeConstructor *treeConstructor, Token &&token) noexcept {
        return static_cast<HTMLElement *>(
                insertAForeignElement(treeConstructor, std::move(token), infra::namespaces::HTML));
    }

} // namespace html

