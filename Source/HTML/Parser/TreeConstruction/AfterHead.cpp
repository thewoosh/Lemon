/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/HTML/Parser/TreeConstruction/Commons.hpp"

#include "Source/Infra/Namespaces.hpp"

namespace html {

    DEFINE_INSERTION_MODE(AFTER_HEAD) {
        if (isWhitespace(token)) {
            insertACharacter(this, token.asCharacter());
            return true;
        }

        if (token.type() == Token::Type::COMMENT) {
            insertAComment(this, token.asComment());
            return true;
        }

        if (token.type() == Token::Type::DOCTYPE) {
            PARSE_ERROR(DOCTYPE_OUTSIDE_INITIAL_INSERTION_MODE);
            return true;
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagName == "html") {
            return processTheRulesFor(InsertionMode::IN_BODY, std::move(token));
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagName == "body") {
            insertAnHTMLElement(this, std::move(token));
            m_document->setFramesetFlagToNotOK();
            switchTheInsertionModeTo(InsertionMode::IN_BODY);
            return true;
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagName == "frameset") {
            insertAnHTMLElement(this, std::move(token));
            switchTheInsertionModeTo(InsertionMode::IN_FRAMESET);
            return true;
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagNameIsAnyOf("base", "basefont", "bgsound",
                "link", "meta", "noframes", "script", "style", "template", "title")) {
            PARSE_ERROR(ILLEGAL_ELEMENT_BETWEEN_HEAD_AND_BODY);
            m_stackOfOpenElements.push(m_headElementPointer);
            if (!processTheRulesFor(InsertionMode::IN_HEAD, std::move(token)))
                return false;
            if (!m_stackOfOpenElements.popUntilElement(m_headElementPointer))
                return false;
            return true;
        }

        if (token.type() == Token::Type::END_TAG && token.asEndTag().tagName == "template") {
            return processTheRulesFor(InsertionMode::IN_HEAD, std::move(token));
        }

        if (token.type() == Token::Type::START_TAG && token.asStartTag().tagName == "head") {
            PARSE_ERROR(MULTIPLE_HEAD_ELEMENTS);
            return true;
        }

        if (token.type() == Token::Type::END_TAG && !token.asEndTag().tagNameIsAnyOf("body", "html", "br")) {
            PARSE_ERROR(ILLEGAL_ELEMENT_BETWEEN_HEAD_AND_BODY);
            return true;
        }

        insertAnHTMLElement(this, Token::createStartTag("body"));
        switchTheInsertionModeTo(InsertionMode::IN_BODY);
        return reprocessToken(std::move(token));
    }

} // namespace html
