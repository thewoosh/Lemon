/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/HTML/Parser/TreeConstructor.hpp"

#include "Source/Base/Assert.hpp"
#include "Source/Base/Logger.hpp"
#include "Source/HTML/Parser/Tokenizer.hpp"
#include "Source/HTML/Parser/TreeConstruction/Commons.hpp"

namespace html {

    void
    TreeConstructor::closeAPElement() noexcept {
        constexpr const std::array<std::string_view, 9> names{
            "dd", "dt", "li", "optgroup", "option", "rb", "rp", "rt","rtc"
        };

        // Generate implied end tags, except a </p>
        while (!std::empty(std::data(m_stackOfOpenElements))
                && currentElement()->namespaceURI() == infra::namespaces::HTML
                && std::find(std::begin(names), std::end(names), currentElement()->localName()) != std::end(names)) {
            m_stackOfOpenElements.popOffCurrentNode();
        }

        if (std::empty(std::data(m_stackOfOpenElements)) || currentElement()->namespaceURI() != infra::namespaces::HTML
                || currentElement()->localName() != "p") {
            PARSE_ERROR(MISPLACED_P_END_TAG);
        }

        while (!std::empty(std::data(m_stackOfOpenElements))) {
            auto *element = m_stackOfOpenElements.popOffCurrentNode();
            if (element->namespaceURI() == infra::namespaces::HTML && element->localName() == "p") {
                break;
            }
        }
    }

    dom::Element *
    TreeConstructor::currentElement() noexcept {
        if (std::empty(std::data(m_stackOfOpenElements)))
            return nullptr;
        return std::data(m_stackOfOpenElements).back();
    }

    bool
    TreeConstructor::followTheGenericRawTextElementParsingAlgorithm(Token &&token) noexcept {
        m_tokenizer->setAppropriateEndTagName(std::string(token.asStartTag().tagName));
        insertAnHTMLElement(this, std::move(token));
        m_tokenizer->switchTo(TokenizerState::RAWTEXT);
        m_originalInsertionMode = m_insertionMode;
        switchTheInsertionModeTo(InsertionMode::TEXT);
        return true;
    }

    void TreeConstructor::generateImpliedEndTags(std::string_view exceptForLocalName) noexcept {
        constexpr const std::array<std::string_view, 10> names{
            "dd", "dt", "li", "optgroup", "option", "p", "rb", "rp", "rt","rtc"
        };

        while (!std::empty(std::data(m_stackOfOpenElements))
                && currentElement()->localName() != exceptForLocalName
                && std::find(std::begin(names), std::end(names), currentElement()->localName()) != std::end(names)
                && currentElement()->namespaceURI() == infra::namespaces::HTML) {
            LOG_DEBUG("Popped HTML element with name \"{}\"", currentElement()->localName());
            m_stackOfOpenElements.popOffCurrentNode();
        }
    }

    bool
    TreeConstructor::onTokenEmission(Token &&token) noexcept {
        static_cast<void>(token);
        static_cast<void>(m_tokenizer);
        static_cast<void>(m_document);
        return processTheRulesFor(m_insertionMode, std::move(token));
    }

    bool
    TreeConstructor::processTheRulesFor(InsertionMode insertionMode, Token &&token) noexcept {
        switch (insertionMode) {
#define LEMON_HTML_INVOKE_INSERTION_MODE_ITERATOR(name) \
            case InsertionMode::name: \
                if (m_verbose) \
                    LOG_INFO("Invoking " #name " from {}", m_tokenizer->state()); \
                return insertionModeImplementation##name(std::move(token));
            LEMON_HTML_ITERATE_INSERTION_MODES(LEMON_HTML_INVOKE_INSERTION_MODE_ITERATOR)
            default:
                LEMON_ASSERT_NOT_REACHED_OR_RETURN(false);
        }
    }

    void
    TreeConstructor::reconstructTheActiveFormattingElements() noexcept {
        // TODO
    }

    bool
    TreeConstructor::reprocessToken(Token &&token) noexcept {
        return processTheRulesFor(m_insertionMode, std::move(token));
    }

    bool
    TreeConstructor::stopParsing() noexcept {
        // TODO step 1, 2
        while (!std::empty(std::data(m_stackOfOpenElements)))
            m_stackOfOpenElements.popOffCurrentNode();
        // TODO scripts (step 4 - 10 incl)
        return true;
    }

    void
    TreeConstructor::switchTheInsertionModeTo(InsertionMode insertionMode) noexcept {
        m_insertionMode = insertionMode;
    }

    #define DEFINE_TODO_INSERTION_MODE(name) \
        DEFINE_INSERTION_MODE(name) { \
            dom::printNode(m_document); \
            LOG_ERROR("Unimplemented state: " #name); \
            static_cast<void>(token); \
            LEMON_ASSERT_NOT_REACHED_OR_RETURN(false); \
        }

    DEFINE_TODO_INSERTION_MODE(IN_HEAD_NOSCRIPT)
    DEFINE_TODO_INSERTION_MODE(IN_TABLE)
    DEFINE_TODO_INSERTION_MODE(IN_TABLE_TEXT)
    DEFINE_TODO_INSERTION_MODE(IN_CAPTION)
    DEFINE_TODO_INSERTION_MODE(IN_COLUMN_GROUP)
    DEFINE_TODO_INSERTION_MODE(IN_TABLE_BODY)
    DEFINE_TODO_INSERTION_MODE(IN_ROW)
    DEFINE_TODO_INSERTION_MODE(IN_CELL)
    DEFINE_TODO_INSERTION_MODE(IN_SELECT)
    DEFINE_TODO_INSERTION_MODE(IN_SELECT_IN_TABLE)
    DEFINE_TODO_INSERTION_MODE(IN_TEMPLATE)
    DEFINE_TODO_INSERTION_MODE(IN_FRAMESET)
    DEFINE_TODO_INSERTION_MODE(AFTER_FRAMESET)
    DEFINE_TODO_INSERTION_MODE(AFTER_AFTER_FRAMESET)

} // namespace html
