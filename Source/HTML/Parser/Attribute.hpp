/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string>
#include <vector>

#include "Source/Base/CompilerFeatures.hpp"

namespace html {

    struct Attribute {
        [[nodiscard]] Attribute() = default;
        [[nodiscard]] Attribute(Attribute &&) = default;
        Attribute &operator=(Attribute &&) = default;

        [[nodiscard]] inline LEMON_CONSTEXPR_STRING
        Attribute(std::string &&name, std::string &&value) noexcept
                : m_name(std::move(name))
                , m_value(std::move(value)) {
        }

        [[nodiscard]] inline LEMON_CONSTEXPR_STRING
        Attribute(const std::string &name, const std::string &value) noexcept
                : m_name(name)
                , m_value(value) {
        }

        [[nodiscard]] inline LEMON_CONSTEXPR_STRING std::string &
        name() noexcept {
            return m_name;
        }

        [[nodiscard]] inline LEMON_CONSTEXPR_STRING const std::string &
        name() const noexcept {
            return m_name;
        }

        [[nodiscard]] inline LEMON_CONSTEXPR_STRING std::string &
        value() noexcept {
            return m_value;
        }

        [[nodiscard]] inline LEMON_CONSTEXPR_STRING const std::string &
        value() const noexcept {
            return m_value;
        }

    private:
        std::string m_name;
        std::string m_value;
    };

    struct AttributeList
            : public std::vector<Attribute> {

        [[nodiscard]] AttributeList() noexcept = default;
        [[nodiscard]] AttributeList(AttributeList &&) noexcept = default;
        AttributeList &operator=(AttributeList &&) noexcept = default;

        [[nodiscard]] inline
        AttributeList(std::vector<Attribute> &&copy)
                : vector(std::move(copy)) {
        }

        [[nodiscard]] inline std::optional<std::string_view>
        find(std::string_view name) const noexcept {
            const auto val = find_if(cbegin(), cend(), [&](const auto &entry) {
                return entry.name() == name;
            });

            return val == cend() ? std::nullopt : std::make_optional(std::string_view{val->value()});
        }
    };

} // namespace html
