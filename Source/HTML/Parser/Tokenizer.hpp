/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <functional>
#include <string_view>

#include "Source/HTML/Parser/Constants.hpp"
#include "Source/HTML/Parser/TokenizerParseError.hpp"
#include "Source/HTML/Parser/Token.hpp"
#include "Source/HTML/Parser/TokenizerState.hpp"

namespace html {

    class Tokenizer {
    public:
        [[nodiscard]] inline explicit
        Tokenizer(std::u32string_view data) noexcept
                : m_data(data)
                , m_it(std::cbegin(m_data)) {
            m_nextInputCharacter = std::empty(data) ? std::empty(data) : data.front();
        }

        [[nodiscard]] bool
        run() noexcept;

        inline void
        setTokenEmissionCallback(std::function<bool(Token &&)> &&function) noexcept {
            m_tokenEmissionCallback = std::move(function);
        }

        /**
         * Acts as the name of the last emitted start tag name. We don't want
         * to set this after every start tag "just to be sure", since we want
         * to move the name to the TokenEmitCallback (i.e. Tokenizer), so we
         * would've to make a copy of the name, after *every* start tag.
         *
         * Since 9/10 times we don't care about this value anyway, the Tree
         * Constructor sets this value when it switches to e.g. the TEXT
         * insertion mode.
         */
        void
        setAppropriateEndTagName(std::string &&name) noexcept {
            m_appropriateEndTagName = std::move(name);
        }

        inline constexpr void
        setVerbosity(bool verbose) noexcept {
            m_verbose = verbose;
        }

        [[nodiscard]] inline constexpr TokenizerState
        state() const noexcept {
            return m_state;
        }

        void
        switchTo(TokenizerState tokenizerState) noexcept;

    private:
        [[nodiscard]] bool
        isCurrentTokenAnAppropriateEndTagToken() const noexcept;

        [[nodiscard]] char32_t
        consumeNextInputCharacter() noexcept;

        void
        emitCurrentToken() noexcept;

        void
        emitToken(Token &&) noexcept;

        void
        invokeState(TokenizerState tokenizerState) noexcept;

        void
        reconsumeIn(TokenizerState tokenizerState) noexcept;

        void
        setReturnState(TokenizerState tokenizerState) noexcept;

#define LEMON_HTML_TOKENIZER_DECLARE_STATE_FUNCTION(name) \
        void \
        stateImplementation##name() noexcept;
        LEMON_HTML_ITERATE_TOKENIZER_STATES(LEMON_HTML_TOKENIZER_DECLARE_STATE_FUNCTION)

        std::u32string_view m_data;
        std::u32string_view::const_iterator m_it;

        bool m_verbose{false};

        TokenizerState m_state{TokenizerState::DATA};
        std::optional<TokenizerState> m_returnState{std::nullopt};

        char32_t m_currentInputCharacter{};
        char32_t m_nextInputCharacter{};

        std::optional<Token> m_currentToken{std::nullopt};
        std::optional<std::string> m_appropriateEndTagName{};
        Attribute *m_currentAttribute{nullptr};

        std::function<bool(Token &&)> m_tokenEmissionCallback{};
        std::optional<std::u32string> m_temporaryBuffer{};
    };

} // namespace html
