/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/HTML/Parser/Token.hpp"

#include <sstream>

#include <fmt/core.h>

#include "Include/fmt.hpp"

namespace html {

    [[nodiscard]] std::string
    stringifyTagData(std::string_view typeName, const Token::TagData &data) noexcept {
        std::stringstream stream;
        stream << "Token{type=" << typeName
               << ", tag-name=\"" << data.tagName
               << "\", self-closing-flag=" << (data.selfClosingFlag ? "on" : "off")
               << ", attributes=[";

        bool multipleFlag{false};
        for (const auto &attribute : data.attributeList) {
            if (multipleFlag) {
                stream << ", ";
            } else {
                multipleFlag = true;
            }

            stream << "{\"" << attribute.name() << "\"=\"" << attribute.value() << "\"}";
        }

        stream << "]}";
        return stream.str();
    }

    std::string
    Token::toString() const noexcept {
        switch (type()) {
            case Type::DOCTYPE: {
                const auto &data = asDoctype();

                std::stringstream ss;
                ss << "Token{type=DOCTYPE, name=";

                if (data.name.has_value()) {
                    ss << '"' << data.name.value() << "\", publicIdentifier=";
                } else {
                    ss << "missing, publicIdentifier=";
                }

                if (data.publicIdentifier.has_value()) {
                    ss << '"' << data.publicIdentifier.value() << "\", systemIdentifier=";
                } else {
                    ss << "missing, systemIdentifier=";
                }

                if (data.systemIdentifier.has_value()) {
                    ss << '"' << data.systemIdentifier.value() << "\", force-quirks-flag=";
                } else {
                    ss << "missing, force-quirks-flag=";
                }

                if (data.forceQuirks) {
                    ss << "on}";
                } else {
                    ss << "off}";
                }

                return ss.str();
            }
            case Type::START_TAG:
                return stringifyTagData("start-tag", asStartTag());
            case Type::END_TAG:
                return stringifyTagData("end-tag", asEndTag());
            case Type::COMMENT:
                return fmt::format(FMTLIB_WRAPPER("Token{{type=comment, data=\"{}\"}}"), asComment());
            case Type::CHARACTER:
                return fmt::format(FMTLIB_WRAPPER("Token{{type=character, data=\"U+{:04X}\"}}"), static_cast<std::size_t>(asCharacter()));
            case Type::END_OF_FILE:
                return "Token{type=end-of-file}";
            default:
                return "Token{type=(invalid)}";
        }
    }

} // namespace html
