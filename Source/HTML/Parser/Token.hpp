/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <optional>
#include <string>
#include <utility>
#include <variant>

#include "Source/HTML/Parser/Attribute.hpp"

namespace html {

    struct Token {
        enum class Type {
            DOCTYPE,
            START_TAG,
            END_TAG,
            COMMENT,
            CHARACTER,
            END_OF_FILE,
        };

        struct DoctypeData {
            std::optional<std::string> name;
            std::optional<std::string> publicIdentifier;
            std::optional<std::string> systemIdentifier;
            bool forceQuirks{false};
        };

        struct TagData {
            std::string tagName;
            bool selfClosingFlag;
            AttributeList attributeList;

            template <typename...Args>
            [[nodiscard]] inline bool
            tagNameIsAnyOf(Args &&...args) noexcept {
                bool array[]{ (tagName == args)... };
                return std::find(std::cbegin(array), std::cend(array), true) != std::cend(array);
            }
        };

        struct StartTagData : public TagData{};
        struct EndTagData : public TagData{};

    private:
        struct CharacterData {
            char32_t content;
        };

        struct CommentData {
            std::string content;
        };

        struct EOFData{};

    public:
        [[nodiscard]] inline char32_t
        asCharacter() const noexcept {
            return std::get<CharacterData>(m_data).content;
        }

        [[nodiscard]] inline std::string &
        asComment() noexcept {
            return std::get<CommentData>(m_data).content;
        }

        [[nodiscard]] inline const std::string &
        asComment() const noexcept {
            return std::get<CommentData>(m_data).content;
        }

        [[nodiscard]] inline DoctypeData &
        asDoctype() noexcept {
            return std::get<DoctypeData>(m_data);
        }

        [[nodiscard]] inline const DoctypeData &
        asDoctype() const noexcept {
            return std::get<DoctypeData>(m_data);
        }

        [[nodiscard]] inline EndTagData &
        asEndTag() noexcept {
            return std::get<EndTagData>(m_data);
        }

        [[nodiscard]] inline const EndTagData &
        asEndTag() const noexcept {
            return std::get<EndTagData>(m_data);
        }

        [[nodiscard]] inline StartTagData &
        asStartTag() noexcept {
            return std::get<StartTagData>(m_data);
        }

        [[nodiscard]] inline const StartTagData &
        asStartTag() const noexcept {
            return std::get<StartTagData>(m_data);
        }

        [[nodiscard]] inline TagData &
        asTag() noexcept {
            if (type() == Type::START_TAG)
                return std::get<StartTagData>(m_data);
            else
                return std::get<EndTagData>(m_data);
        }

        [[nodiscard]] inline const TagData &
        asTag() const noexcept {
            if (type() == Type::START_TAG)
                return std::get<StartTagData>(m_data);
            else
                return std::get<EndTagData>(m_data);
        }

        [[nodiscard]] inline static Token
        createCharacter(char32_t data) noexcept {
            return Token{CharacterData{data}};
        }

        [[nodiscard]] inline static Token
        createComment(std::string &&data) noexcept {
            return Token{CommentData{std::move(data)}};
        }

        [[nodiscard]] inline static Token
        createDoctype(std::optional<std::string> &&name=std::nullopt,
                      std::optional<std::string> &&publicIdentifier=std::nullopt,
                      std::optional<std::string> &&systemIdentifier=std::nullopt,
                      bool forceQuirksFlag=false) noexcept {
            return Token{DoctypeData{std::move(name), std::move(publicIdentifier), std::move(systemIdentifier),
                                 forceQuirksFlag}};
        }

        [[nodiscard]] inline static Token
        createEndTag(std::string &&tagName, bool selfClosingFlag=false, AttributeList &&attributeList={}) noexcept {
            return Token{EndTagData{{std::move(tagName), selfClosingFlag, std::move(attributeList)}}};
        }

        [[nodiscard]] inline static Token
        createEOF() noexcept {
            return Token{EOFData{}};
        }

        [[nodiscard]] inline static Token
        createStartTag(std::string &&tagName, bool selfClosingFlag=false, AttributeList &&attributeList={}) noexcept {
            return Token{StartTagData{{std::move(tagName), selfClosingFlag, std::move(attributeList)}}};
        }

        [[nodiscard]] std::string
        toString() const noexcept;

        [[nodiscard]] inline constexpr Type
        type() const noexcept {
            return static_cast<Type>(m_data.index());
        }

    private:
        template <typename...Args>
        [[nodiscard]] inline explicit
        Token(Args &&...args) noexcept
                : m_data(std::forward<Args>(args)...) {
        }

        std::variant<DoctypeData, StartTagData, EndTagData, CommentData, CharacterData, EOFData> m_data;
    };

} // namespace html
