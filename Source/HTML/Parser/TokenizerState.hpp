/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/HTML/Parser/Tokenizer.hpp"

#include "Source/Base/Assert.hpp"
#include "Source/Base/Logger.hpp"

namespace html {

#define LEMON_HTML_ITERATE_TOKENIZER_STATES(function) \
        function(DATA) \
        function(RCDATA) \
        function(RAWTEXT) \
        function(SCRIPT_DATA) \
        function(PLAINTEXT) \
        function(TAG_OPEN) \
        function(END_TAG_OPEN) \
        function(TAG_NAME) \
        function(RCDATA_LESS_THAN_SIGN) \
        function(RCDATA_END_TAG_OPEN) \
        function(RCDATA_END_TAG_NAME) \
        function(RAWTEXT_LESS_THAN_SIGN) \
        function(RAWTEXT_END_TAG_OPEN) \
        function(RAWTEXT_END_TAG_NAME) \
        function(SCRIPT_DATA_LESS_THAN_SIGN) \
        function(SCRIPT_DATA_END_TAG_OPEN) \
        function(SCRIPT_DATA_END_TAG_NAME) \
        function(SCRIPT_DATA_ESCAPE_START) \
        function(SCRIPT_DATA_ESCAPE_START_DASH) \
        function(SCRIPT_DATA_ESCAPED) \
        function(SCRIPT_DATA_ESCAPED_DASH) \
        function(SCRIPT_DATA_ESCAPED_DASH_DASH) \
        function(SCRIPT_DATA_ESCAPED_LESS_THAN_SIGN) \
        function(SCRIPT_DATA_ESCAPED_END_TAG_OPEN) \
        function(SCRIPT_DATA_ESCAPED_END_TAG_NAME) \
        function(SCRIPT_DATA_DOUBLE_ESCAPE_START) \
        function(SCRIPT_DATA_DOUBLE_ESCAPED) \
        function(SCRIPT_DATA_DOUBLE_ESCAPED_DASH) \
        function(SCRIPT_DATA_DOUBLE_ESCAPED_DASH_DASH) \
        function(SCRIPT_DATA_DOUBLE_ESCAPED_LESS_THAN_SIGN) \
        function(SCRIPT_DATA_DOUBLE_ESCAPED_END) \
        function(BEFORE_ATTRIBUTE_NAME) \
        function(ATTRIBUTE_NAME) \
        function(AFTER_ATTRIBUTE_NAME) \
        function(BEFORE_ATTRIBUTE_VALUE) \
        function(ATTRIBUTE_VALUE_DOUBLE_QUOTED) \
        function(ATTRIBUTE_VALUE_SINGLE_QUOTED) \
        function(ATTRIBUTE_VALUE_UNQUOTED) \
        function(AFTER_ATTRIBUTE_VALUE_QUOTED) \
        function(SELF_CLOSING_START_TAG) \
        function(BOGUS_COMMENT) \
        function(MARKUP_DECLARATION_OPEN) \
        function(COMMENT_START) \
        function(COMMENT_START_DASH) \
        function(COMMENT) \
        function(COMMENT_LESS_THAN_SIGN) \
        function(COMMENT_LESS_THAN_SIGN_BANG) \
        function(COMMENT_LESS_THAN_SIGN_BANG_DASH) \
        function(COMMENT_LESS_THAN_SIGN_BANG_DASH_DASH) \
        function(COMMENT_END_DASH) \
        function(COMMENT_END) \
        function(COMMENT_END_BANG) \
        function(DOCTYPE) \
        function(BEFORE_DOCTYPE_NAME) \
        function(DOCTYPE_NAME) \
        function(AFTER_DOCTYPE_NAME) \
        function(AFTER_DOCTYPE_PUBLIC_KEYWORD) \
        function(BEFORE_DOCTYPE_PUBLIC_IDENTIFIER) \
        function(DOCTYPE_PUBLIC_IDENTIFIER_DOUBLE_QUOTED) \
        function(DOCTYPE_PUBLIC_IDENTIFIER_SINGLE_QUOTED) \
        function(AFTER_DOCTYPE_PUBLIC_IDENTIFIER) \
        function(BETWEEN_DOCTYPE_PUBLIC_AND_SYSTEM_IDENTIFIERS) \
        function(AFTER_DOCTYPE_SYSTEM_KEYWORD) \
        function(BEFORE_DOCTYPE_SYSTEM_IDENTIFIER) \
        function(DOCTYPE_SYSTEM_IDENTIFIER_DOUBLE_QUOTED) \
        function(DOCTYPE_SYSTEM_IDENTIFIER_SINGLE_QUOTED) \
        function(AFTER_DOCTYPE_SYSTEM_IDENTIFIER) \
        function(BOGUS_DOCTYPE) \
        function(CDATA_SECTION) \
        function(CDATA_SECTION_BRACKET) \
        function(CDATA_SECTION_END) \
        function(CHARACTER_REFERENCE) \
        function(NAMED_CHARACTER_REFERENCE) \
        function(AMBIGUOUS_AMPERSAND) \
        function(NUMERIC_CHARACTER_REFERENCE) \
        function(HEXADECIMAL_CHARACTER_REFERENCE_START) \
        function(DECIMAL_CHARACTER_REFERENCE_START) \
        function(HEXADECIMAL_CHARACTER_REFERENCE) \
        function(DECIMAL_CHARCTER_REFERENCE) \
        function(NUMERIC_CHARACTER_REFERENCE_END)

    enum class TokenizerState {
#define LEMON_HTML_DEFINE_TOKENIZER_STATE(name) name,
        LEMON_HTML_ITERATE_TOKENIZER_STATES(LEMON_HTML_DEFINE_TOKENIZER_STATE)
    };

} // namespace html
