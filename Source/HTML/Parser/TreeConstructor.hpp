/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string_view>

#include "Source/Base/CompilerFeatures.hpp"
#include "Source/HTML/Parser/Token.hpp"
#include "Source/HTML/Parser/InsertionMode.hpp"
#include "Source/HTML/Parser/StackOfOpenElements.hpp"

namespace dom {
    struct Document;
} // namespace dom

namespace html {

    class Tokenizer;
    struct HTMLFormElement;

    class TreeConstructor {
    public:
        [[nodiscard]] inline
        TreeConstructor(Tokenizer *tokenizer, dom::Document *document) noexcept
                : m_tokenizer(tokenizer)
                , m_document(document) {
        }

        [[nodiscard]] dom::Element *
        currentElement() noexcept;

        [[nodiscard]] inline constexpr dom::Document *
        document() noexcept {
            return m_document;
        }

        [[nodiscard]] inline constexpr const dom::Document *
        document() const noexcept {
            return m_document;
        }

        /**
         * Returns false if a critical error has occurred, otherwise true.
         */
        [[nodiscard]] bool
        onTokenEmission(Token &&) noexcept;

        inline constexpr void
        setVerbosity(bool verbose) noexcept {
            m_verbose = verbose;
        }

        [[nodiscard]] inline constexpr StackOfOpenElements &
        stackOfOpenElements() noexcept {
            return m_stackOfOpenElements;
        }

        [[nodiscard]] inline constexpr const StackOfOpenElements &
        stackOfOpenElements() const noexcept {
            return m_stackOfOpenElements;
        }

    private:
        void
        switchInsertionModeTo(InsertionMode) noexcept;

        #define LEMON_HTML_TREE_CONSTRUCTOR_DECLARE_INSERTION_MODE_FUNCTION(name) \
        [[nodiscard]] bool \
        insertionModeImplementation##name(Token &&) noexcept;
        LEMON_HTML_ITERATE_INSERTION_MODES(LEMON_HTML_TREE_CONSTRUCTOR_DECLARE_INSERTION_MODE_FUNCTION)

        void
        closeAPElement() noexcept;

        [[nodiscard]] bool
        followTheGenericRawTextElementParsingAlgorithm(Token &&) noexcept;

        [[nodiscard]] bool
        processTheRulesFor(InsertionMode insertionMode, Token &&) noexcept;

        void
        generateImpliedEndTags(std::string_view exceptForLocalName={}) noexcept;

        /**
         * https://html.spec.whatwg.org/multipage/parsing.html#reconstruct-the-active-formatting-elements
         */
        void
        reconstructTheActiveFormattingElements() noexcept;

        [[nodiscard]] bool
        reprocessToken(Token &&) noexcept;

        /**
         * https://html.spec.whatwg.org/multipage/parsing.html#stop-parsing
         */
        [[nodiscard]] bool
        stopParsing() noexcept;

        void
        switchTheInsertionModeTo(InsertionMode) noexcept;

        bool m_verbose{false};

        Tokenizer *m_tokenizer;
        dom::Document *m_document;
        dom::Element *m_headElementPointer{nullptr};
        HTMLFormElement *m_formElementPointer{nullptr};

        InsertionMode m_insertionMode{InsertionMode::INITIAL};
        std::optional<InsertionMode> m_originalInsertionMode{};
        std::vector<InsertionMode> m_stackOfTemplateInsertionModes{};
        StackOfOpenElements m_stackOfOpenElements{};
    };

} // namespace html
