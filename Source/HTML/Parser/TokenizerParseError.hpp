/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 *
 * https://html.spec.whatwg.org/multipage/parsing.html#parse-errors
 */

#pragma once

#include <string_view>

namespace html {

    enum class TokenizerParseError {
        ABRUPT_CLOSING_OF_EMPTY_COMMENT,
        ABRUPT_DOCTYPE_PUBLIC_IDENTIFIER,
        ABRUPT_DOCTYPE_SYSTEM_IDENTIFIER,
        ABSENCE_OF_DIGITS_IN_NUMERIC_CHARACTER_REFERENCE,
        CDATA_IN_HTML_CONTENT,
        CHARACTER_REFERENCE_OUTSIDE_UNICODE_RANGE,
        CONTROL_CHARACTER_IN_INPUT_STREAM,
        CONTROL_CHARACTER_REFERENCE,
        END_TAG_WITH_ATTRIBUTES,
        DUPLICATE_ATTRIBUTE,
        END_TAG_WITH_TRAILING_SOLIDUS,
        EOF_BEFORE_TAG_NAME,
        EOF_IN_CDATA,
        EOF_IN_COMMENT,
        EOF_IN_DOCTYPE,
        EOF_IN_SCRIPT_HTML_COMMENT_LIKE_TEXT,
        EOF_IN_TAG,
        INCORRECTLY_CLOSED_COMMENT,
        INCORRECTLY_OPENED_COMMENT,
        INVALID_CHARACTER_SEQUENCE_AFTER_DOCTYPE_NAME,
        INVALID_FIRST_CHARACTER_OF_TAG_NAME,
        MISSING_ATTRIBUTE_VALUE,
        MISSING_DOCTYPE_NAME,
        MISSING_DOCTYPE_PUBLIC_IDENTIFIER,
        MISSING_DOCTYPE_SYSTEM_IDENTIFIER,
        MISSING_END_TAG_NAME,
        MISSING_QUOTE_BEFORE_DOCTYPE_PUBLIC_IDENTIFIER,
        MISSING_QUOTE_BEFORE_DOCTYPE_SYSTEM_IDENTIFIER,
        MISSING_SEMICOLON_AFTER_CHARACTER_REFERENCE,
        MISSING_WHITESPACE_AFTER_DOCTYPE_PUBLIC_KEYWORD,
        MISSING_WHITESPACE_AFTER_DOCTYPE_SYSTEM_KEYWORD,
        MISSING_WHITESPACE_BEFORE_DOCTYPE_NAME,
        MISSING_WHITESPACE_BETWEEN_ATTRIBUTES,
        MISSING_WHITESPACE_BETWEEN_DOCTYPE_PUBLIC_AND_SYSTEM_IDENTIFIERS,
        NESTED_COMMENT,
        NONCHARACTER_CHARACTER_REFERENCE,
        NONCHARACTER_IN_INPUT_STREAM,
        NON_VOID_HTML_ELEMENT_START_TAG_WITH_TRAILING_SOLIDUS,
        NULL_CHARACTER_REFERENCE,
        SURROGATE_CHARACTER_REFERENCE,
        SURROGATE_IN_INPUT_STREAM,
        UNEXPECTED_CHARACTER_AFTER_DOCTYPE_SYSTEM_IDENTIFIER,
        UNEXPECTED_CHARACTER_IN_ATTRIBUTE_NAME,
        UNEXPECTED_CHARACTER_IN_UNQUOTED_ATTRIBUTE_VALUE,
        UNEXPECTED_EQUALS_SIGN_BEFORE_ATTRIBUTE_NAME,
        UNEXPECTED_NULL_CHARACTER,
        UNEXPECTED_QUESTION_MARK_INSTEAD_OF_TAG_NAME,
        UNEXPECTED_SOLIDUS_IN_TAG,
        UNKNOWN_NAMED_CHARACTER_REFERENCE,
   };

    [[nodiscard]] inline constexpr std::string_view
    toString(TokenizerParseError error) noexcept {
        switch (error) {
            case TokenizerParseError::ABRUPT_CLOSING_OF_EMPTY_COMMENT: return "abrupt-closing-of-empty-comment";
            case TokenizerParseError::ABRUPT_DOCTYPE_PUBLIC_IDENTIFIER: return "abrupt-doctype-public-identifier";
            case TokenizerParseError::ABRUPT_DOCTYPE_SYSTEM_IDENTIFIER: return "abrupt-doctype-system-identifier";
            case TokenizerParseError::ABSENCE_OF_DIGITS_IN_NUMERIC_CHARACTER_REFERENCE: return "absence-of-digits-in-numeric-character-reference";
            case TokenizerParseError::CDATA_IN_HTML_CONTENT: return "cdata-in-html-content";
            case TokenizerParseError::CHARACTER_REFERENCE_OUTSIDE_UNICODE_RANGE: return "character-reference-outside-unicode-range";
            case TokenizerParseError::CONTROL_CHARACTER_IN_INPUT_STREAM: return "control-character-in-input-stream";
            case TokenizerParseError::CONTROL_CHARACTER_REFERENCE: return "control-character-reference";
            case TokenizerParseError::END_TAG_WITH_ATTRIBUTES: return "end-tag-with-attributes";
            case TokenizerParseError::DUPLICATE_ATTRIBUTE: return "duplicate-attribute";
            case TokenizerParseError::END_TAG_WITH_TRAILING_SOLIDUS: return "end-tag-with-trailing-solidus";
            case TokenizerParseError::EOF_BEFORE_TAG_NAME: return "eof-before-tag-name";
            case TokenizerParseError::EOF_IN_CDATA: return "eof-in-cdata";
            case TokenizerParseError::EOF_IN_COMMENT: return "eof-in-comment";
            case TokenizerParseError::EOF_IN_DOCTYPE: return "eof-in-doctype";
            case TokenizerParseError::EOF_IN_SCRIPT_HTML_COMMENT_LIKE_TEXT: return "eof-in-script-html-comment-like-text";
            case TokenizerParseError::EOF_IN_TAG: return "eof-in-tag";
            case TokenizerParseError::INCORRECTLY_CLOSED_COMMENT: return "incorrectly-closed-comment";
            case TokenizerParseError::INCORRECTLY_OPENED_COMMENT: return "incorrectly-opened-comment";
            case TokenizerParseError::INVALID_CHARACTER_SEQUENCE_AFTER_DOCTYPE_NAME: return "invalid-character-sequence-after-doctype-name";
            case TokenizerParseError::INVALID_FIRST_CHARACTER_OF_TAG_NAME: return "invalid-first-character-of-tag-name";
            case TokenizerParseError::MISSING_ATTRIBUTE_VALUE: return "missing-attribute-value";
            case TokenizerParseError::MISSING_DOCTYPE_NAME: return "missing-doctype-name";
            case TokenizerParseError::MISSING_DOCTYPE_PUBLIC_IDENTIFIER: return "missing-doctype-public-identifier";
            case TokenizerParseError::MISSING_DOCTYPE_SYSTEM_IDENTIFIER: return "missing-doctype-system-identifier";
            case TokenizerParseError::MISSING_END_TAG_NAME: return "missing-end-tag-name";
            case TokenizerParseError::MISSING_QUOTE_BEFORE_DOCTYPE_PUBLIC_IDENTIFIER: return "missing-quote-before-doctype-public-identifier";
            case TokenizerParseError::MISSING_QUOTE_BEFORE_DOCTYPE_SYSTEM_IDENTIFIER: return "missing-quote-before-doctype-system-identifier";
            case TokenizerParseError::MISSING_SEMICOLON_AFTER_CHARACTER_REFERENCE: return "missing-semicolon-after-character-reference";
            case TokenizerParseError::MISSING_WHITESPACE_AFTER_DOCTYPE_PUBLIC_KEYWORD: return "missing-whitespace-after-doctype-public-keyword";
            case TokenizerParseError::MISSING_WHITESPACE_AFTER_DOCTYPE_SYSTEM_KEYWORD: return "missing-whitespace-after-doctype-system-keyword";
            case TokenizerParseError::MISSING_WHITESPACE_BEFORE_DOCTYPE_NAME: return "missing-whitespace-before-doctype-name";
            case TokenizerParseError::MISSING_WHITESPACE_BETWEEN_ATTRIBUTES: return "missing-whitespace-between-attributes";
            case TokenizerParseError::MISSING_WHITESPACE_BETWEEN_DOCTYPE_PUBLIC_AND_SYSTEM_IDENTIFIERS: return "missing-whitespace-between-doctype-public-and-system-identifiers";
            case TokenizerParseError::NESTED_COMMENT: return "nested-comment";
            case TokenizerParseError::NONCHARACTER_CHARACTER_REFERENCE: return "noncharacter-character-reference";
            case TokenizerParseError::NONCHARACTER_IN_INPUT_STREAM: return "noncharacter-in-input-stream";
            case TokenizerParseError::NON_VOID_HTML_ELEMENT_START_TAG_WITH_TRAILING_SOLIDUS: return "non-void-html-element-start-tag-with-trailing-solidus";
            case TokenizerParseError::NULL_CHARACTER_REFERENCE: return "null-character-reference";
            case TokenizerParseError::SURROGATE_CHARACTER_REFERENCE: return "surrogate-character-reference";
            case TokenizerParseError::SURROGATE_IN_INPUT_STREAM: return "surrogate-in-input-stream";
            case TokenizerParseError::UNEXPECTED_CHARACTER_AFTER_DOCTYPE_SYSTEM_IDENTIFIER: return "unexpected-character-after-doctype-system-identifier";
            case TokenizerParseError::UNEXPECTED_CHARACTER_IN_ATTRIBUTE_NAME: return "unexpected-character-in-attribute-name";
            case TokenizerParseError::UNEXPECTED_CHARACTER_IN_UNQUOTED_ATTRIBUTE_VALUE: return "unexpected-character-in-unquoted-attribute-value";
            case TokenizerParseError::UNEXPECTED_EQUALS_SIGN_BEFORE_ATTRIBUTE_NAME: return "unexpected-equals-sign-before-attribute-name";
            case TokenizerParseError::UNEXPECTED_NULL_CHARACTER: return "unexpected-null-character";
            case TokenizerParseError::UNEXPECTED_QUESTION_MARK_INSTEAD_OF_TAG_NAME: return "unexpected-question-mark-instead-of-tag-name";
            case TokenizerParseError::UNEXPECTED_SOLIDUS_IN_TAG: return "unexpected-solidus-in-tag";
            case TokenizerParseError::UNKNOWN_NAMED_CHARACTER_REFERENCE: return "unknown-named-character-reference";
            default: return "(invalid)";
       }
    }

} // namespace html

