/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

namespace html {

#define LEMON_HTML_ITERATE_INSERTION_MODES(function) \
        function(INITIAL) \
        function(BEFORE_HTML) \
        function(BEFORE_HEAD) \
        function(IN_HEAD) \
        function(IN_HEAD_NOSCRIPT) \
        function(AFTER_HEAD) \
        function(IN_BODY) \
        function(TEXT) \
        function(IN_TABLE) \
        function(IN_TABLE_TEXT) \
        function(IN_CAPTION) \
        function(IN_COLUMN_GROUP) \
        function(IN_TABLE_BODY) \
        function(IN_ROW) \
        function(IN_CELL) \
        function(IN_SELECT) \
        function(IN_SELECT_IN_TABLE) \
        function(IN_TEMPLATE) \
        function(AFTER_BODY) \
        function(IN_FRAMESET) \
        function(AFTER_FRAMESET) \
        function(AFTER_AFTER_BODY) \
        function(AFTER_AFTER_FRAMESET)

    enum class InsertionMode {
#define LEMON_HTML_DEFINE_INSERTION_MODE(name) name,
        LEMON_HTML_ITERATE_INSERTION_MODES(LEMON_HTML_DEFINE_INSERTION_MODE)
    };

} // namespace html
