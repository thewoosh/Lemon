/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include "Source/Graphics/Types/Position.hpp"
#include "Source/Graphics/Types/Size.hpp"

namespace gui {

    struct Element {
        enum class Type {
            PANEL,
            TEXT,
        };

        [[nodiscard]] inline constexpr gfx::Position<float>
        position() const noexcept {
            return m_position;
        }

        inline constexpr void
        setPosition(gfx::Position<float> position) noexcept {
            m_position = position;
        }

        inline constexpr void
        setSize(gfx::Size<float> size) noexcept {
            m_size = size;
        }

        [[nodiscard]] inline constexpr gfx::Size<float>
        size() const noexcept {
            return m_size;
        }

        [[nodiscard]] inline constexpr Type
        type() const noexcept {
            return m_type;
        }

    protected:
        [[nodiscard]] inline constexpr
        Element(Type type, gfx::Position<float> position, gfx::Size<float> size)
                : m_type(type)
                , m_position(position)
                , m_size(size) {
        }

        Type m_type{};

        gfx::Position<float> m_position{0, 0};
        gfx::Size<float> m_size{0, 0};
    };

} // namespace gui
