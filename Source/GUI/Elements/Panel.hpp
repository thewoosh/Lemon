/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <memory>
#include <version>

#include "Source/GUI/Elements/Element.hpp"
#include "Source/Graphics/Types/Color.hpp"

namespace gui {

    struct Panel
            : public Element {
        [[nodiscard]] inline
        Panel(gfx::Position<float> position, gfx::Size<float> size, gfx::Color backgroundColor={}) noexcept
                : Element(Type::PANEL, position, size)
                , m_backgroundColor(backgroundColor) {
        }

        [[nodiscard]] inline constexpr gfx::Color
        backgroundColor() const noexcept {
            return m_backgroundColor;
        }

        [[nodiscard]] inline std::vector<std::unique_ptr<Element>> &
        children() noexcept {
            return m_children;
        }

        [[nodiscard]] inline const std::vector<std::unique_ptr<Element>> &
        children() const noexcept {
            return m_children;
        }

        inline constexpr void
        setBackgroundColor(gfx::Color color) noexcept {
            m_backgroundColor = color;
        }

    private:
        gfx::Color m_backgroundColor;
        std::vector<std::unique_ptr<Element>> m_children{};
    };

} // namespace gui
