/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string_view>

#include "Source/GUI/Elements/Element.hpp"
#include "Source/Graphics/Fonts/Forward.hpp"
#include "Source/Graphics/Types/Color.hpp"

namespace gui {

    struct Text
            : public Element {
        [[nodiscard]] inline constexpr
        Text(gfx::Position<float> position, gfx::Size<float> size, gfx::Font *font, std::string_view contents,
             gfx::Color color={}) noexcept
                : Element(Type::TEXT, position, size)
                , m_font(font)
                , m_contents(contents)
                , m_color(color) {
        }

        [[nodiscard]] inline constexpr gfx::Color
        color() const noexcept {
            return m_color;
        }

        [[nodiscard]] inline constexpr std::string_view
        contents() const noexcept {
            return m_contents;
        }

        [[nodiscard]] inline constexpr gfx::Font *
        font() noexcept {
            return m_font;
        }

        [[nodiscard]] inline constexpr const gfx::Font *
        font() const noexcept {
            return m_font;
        }

        [[nodiscard]] inline constexpr bool
        isDirty() const noexcept {
            return m_dirty;
        }

        inline constexpr void
        setColor(gfx::Color color) noexcept {
            m_color = color;
        }

        inline constexpr void
        setContents(std::string_view contents) noexcept {
            m_contents = contents;
        }

        inline constexpr void
        setDirty(bool dirty) noexcept {
            m_dirty = dirty;
        }

        inline constexpr void
        setFont(gfx::Font *font) noexcept {
            m_font = font;
        }

    private:
        gfx::Font *m_font;
        std::string_view m_contents;
        gfx::Color m_color;

        // Whether the data has been changed since the last render call.
        bool m_dirty{true};
    };

} // namespace gui
