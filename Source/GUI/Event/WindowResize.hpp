/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <cstdint>

#include "Source/Graphics/Types/Size.hpp"

namespace gui::event {

    struct WindowResize {
        [[nodiscard]] inline constexpr explicit
        WindowResize(gfx::Size<std::uint32_t> size) noexcept
                : m_size(size) {
        }

        [[nodiscard]] inline constexpr gfx::Size<std::uint32_t>
        size() const noexcept {
            return m_size;
        }

    private:
        gfx::Size<std::uint32_t> m_size;
    };

} // namespace gui::event
