/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include "Source/GUI/MouseButton.hpp"
#include "Source/Graphics/Types/Position.hpp"

namespace gui::event {

    /**
     * Dispatched when the mouse button was held, but not anymore.
     */
    struct MouseRelease {
        [[nodiscard]] inline constexpr
        MouseRelease(MouseButton mouseButton, gfx::Position<float> position) noexcept
                : m_mouseButton(mouseButton)
                , m_mousePosition(position) {
        }

        [[nodiscard]] inline constexpr MouseButton
        button() const noexcept {
            return m_mouseButton;
        }

        [[nodiscard]] inline constexpr gfx::Position<float>
        position() const noexcept {
            return m_mousePosition;
        }

    private:
        MouseButton m_mouseButton;
        gfx::Position<float> m_mousePosition;
    };

} // namespace gui::event
