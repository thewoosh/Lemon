/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include "Source/Graphics/Types/Position.hpp"

namespace gui::event {

    struct MouseMove {
        [[nodiscard]] inline constexpr
        MouseMove(gfx::Position<float> currentPosition, gfx::Position<float> previousPosition) noexcept
                : m_currentPosition(currentPosition)
                , m_previousPosition(previousPosition) {
        }

        [[nodiscard]] inline constexpr gfx::Position<float>
        currentPosition() const noexcept {
            return m_currentPosition;
        }

        [[nodiscard]] inline constexpr gfx::Position<float>
        delta() const noexcept {
            return {m_currentPosition.x() - m_previousPosition.x(),
                    m_currentPosition.y() - m_previousPosition.y()};
        }

        [[nodiscard]] inline constexpr float
        dx() const noexcept {
            return m_currentPosition.x() - m_previousPosition.x();
        }

        [[nodiscard]] inline constexpr float
        dy() const noexcept {
            return m_currentPosition.y() - m_previousPosition.y();
        }

        [[nodiscard]] inline constexpr gfx::Position<float>
        previousPosition() const noexcept {
            return m_previousPosition;
        }

    private:
        gfx::Position<float> m_currentPosition;
        gfx::Position<float> m_previousPosition;
    };

} // namespace gui::event
