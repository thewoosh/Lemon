/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <functional>
#include <string>
#include <string_view>

#include <cstdint>

#include "Source/GUI/CursorType.hpp"
#include "Source/GUI/Key.hpp"
#include "Source/GUI/Event/MouseClick.hpp"
#include "Source/GUI/Event/MouseMove.hpp"
#include "Source/GUI/Event/MouseRelease.hpp"
#include "Source/GUI/Event/WindowResize.hpp"
#include "Source/GUI/GraphicsAPI.hpp"
#include "Source/Graphics/Types/Size.hpp"

#ifndef GUI_WINDOW_PRIVACY
#   define GUI_WINDOW_PRIVACY private
#endif

namespace gui {

    /**
     * The window API. This class implements RAII, but cannot be copied.
     *
     * NOTE: where
     */
    class Window {
    public:
        [[nodiscard]]
        Window() noexcept = default;

        [[nodiscard]]
        Window(Window &&) noexcept = default;

        [[nodiscard]]
        Window(const Window &) noexcept = delete;

        Window &
        operator=(Window &&) noexcept = default;

        Window &
        operator=(const Window &) noexcept = delete;

        /**
         * Closes the window, e.g. by calling
         */
        virtual
        ~Window() noexcept = default;

        /**
         * Closes the window. Calls to any other function than isClosed() and
         * close() are undefined behavior, and will likely cause a crash.
         *
         * This means that close() can be called repeatedly with or without a
         * call to create().
         *
         * The program can call create() after close(), to open a new window.
         * After that, all functions are available, unless stated otherwise.
         */
        virtual void
        close() noexcept = 0;

        /**
         * Returns whether or not the operating system requests the window to
         * be closed, e.g. by the user clicking the [x] button, Alt+F4 or
         * something else.
         */
        [[nodiscard]] virtual bool
        closeRequested() const noexcept = 0;

        /**
         * Creates the window.
         */
        [[nodiscard]] virtual bool
        create(GraphicsAPI graphicsAPI, std::string_view title, gfx::Size<std::uint32_t> size) noexcept = 0;

        /**
         * Gets the size of the framebuffer (the region we can render in) in
         * pixels.
         */
        [[nodiscard]] virtual gfx::Size<std::uint32_t>
        framebufferSize() const noexcept = 0;

        [[nodiscard]] virtual bool
        isKeyDown(Key) noexcept = 0;

        /**
         * Returns whether or not the window is closed.
         */
        [[nodiscard]] virtual bool
        isClosed() const noexcept = 0;

        /**
         * Requests events from the operating system. Most events will arise
         * from this call.
         */
        virtual void
        poll() noexcept = 0;

        virtual void
        setCursor(CursorType cursor) noexcept = 0;

        /**
         * Updates the title.
         */
        virtual void
        setTitle(const std::string &title) noexcept = 0;

        /**
         * Updates the title.
         */
        virtual void
        setTitle(std::string_view title) noexcept = 0;

        /**
         * Swaps the front and back buffer after rendering. Should be called
         * after all the rendering in a render loop.
         */
        virtual void
        swapBuffers() noexcept = 0;

    GUI_WINDOW_PRIVACY:
        std::function<void(event::WindowResize)> m_resizeCallback;
        std::function<void(event::MouseClick)> m_mouseClickCallback;
        std::function<void(event::MouseMove)> m_mouseMoveCallback;
        std::function<void(event::MouseRelease)> m_mouseReleaseCallback;

    public:
        inline void
        setResizeCallback(decltype(m_resizeCallback) &&function) noexcept {
            m_resizeCallback = std::move(function);
        }

        inline void
        setMouseClickCallback(decltype(m_mouseClickCallback) &&function) noexcept {
            m_mouseClickCallback = std::move(function);
        }

        inline void
        setMouseMoveCallback(decltype(m_mouseMoveCallback) &&function) noexcept {
            m_mouseMoveCallback = std::move(function);
        }
    };

} // namespace gui
