/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include "Source/GUI/Window/Window.hpp"

extern "C"
struct GLFWwindow;

extern "C"
struct GLFWcursor;

#if GUI_GLFW_WINDOW_USE_X11_CURSORS == 1
typedef unsigned long XID;
typedef XID Cursor;
typedef unsigned long XID;
#endif

namespace gui {

#ifndef GUI_GLFW_WINDOW_PRIVACY
#   define GUI_GLFW_WINDOW_PRIVACY private
#endif

    class GLFWWindow final
            : public Window {
    public:
        ~GLFWWindow() noexcept override;

        void
        close() noexcept override;

        [[nodiscard]] bool
        closeRequested() const noexcept override;

        [[nodiscard]] bool
        create(GraphicsAPI graphicsAPI, std::string_view title, gfx::Size<std::uint32_t> size) noexcept override;

        [[nodiscard]] gfx::Size<std::uint32_t>
        framebufferSize() const noexcept override;

        [[nodiscard]] bool
        isKeyDown(Key) noexcept override;

        [[nodiscard]] bool
        isClosed() const noexcept override;

        void
        poll() noexcept override;

        void
        setCursor(CursorType cursor) noexcept override;

        void
        setTitle(const std::string &title) noexcept override;

        void
        setTitle(std::string_view title) noexcept override;

        void
        swapBuffers() noexcept override;

    GUI_GLFW_WINDOW_PRIVACY:
        void
        destroyCursors() noexcept;

        [[nodiscard]] bool
        loadCursors() noexcept;

        GLFWwindow *m_window{nullptr};
        gfx::Position<float> m_cursorPos{0, 0};

#if (GUI_GLFW_WINDOW_USE_X11_CURSORS == 1)
        Cursor m_cursorArrow;
        Cursor m_cursorHand;
        Cursor m_cursorIBeam;
        Cursor m_cursorWait;
#else
        GLFWcursor *m_cursorArrow{nullptr};
        GLFWcursor *m_cursorHand{nullptr};
        GLFWcursor *m_cursorIBeam{nullptr};
#endif
    };

} // namespace gui
