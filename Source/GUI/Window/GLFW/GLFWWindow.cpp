/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#define GUI_WINDOW_PRIVACY public
#define GUI_GLFW_WINDOW_PRIVACY public
#include "GLFWWindow.hpp"

#include <mutex>
#include <optional>

#include <cstddef> // for std::size_t

#if (GUI_GLFW_WINDOW_USE_X11_CURSORS == 1)
#   include <X11/Xcursor/Xcursor.h>
#   define GLFW_EXPOSE_NATIVE_X11
#endif

#include <GLFW/glfw3.h>
#include <GLFW/glfw3native.h>

namespace gui {

    constexpr static const std::array<std::pair<int, Key>, 1> s_keyMap{
        std::make_pair(GLFW_KEY_LEFT_CONTROL, Key::LEFT_CONTROL),
    };

    static std::size_t g_windowCounter{};
    static std::mutex g_windowMutex{};

    [[nodiscard]] static gfx::Position<float>
    getMousePosition(GLFWwindow *window) noexcept {
        double mouseDoubleX, mouseDoubleY;
        glfwGetCursorPos(window, &mouseDoubleX, &mouseDoubleY);
        return gfx::Position<float>{static_cast<float>(mouseDoubleX), static_cast<float>(mouseDoubleY)};
    }

    [[nodiscard]] std::optional<event::MouseButton>
    convertMouseButton(int button) noexcept {
        switch (button) {
            case GLFW_MOUSE_BUTTON_LEFT:
                return event::MouseButton::LEFT;
            case GLFW_MOUSE_BUTTON_MIDDLE:
                return event::MouseButton::MIDDLE;
            case GLFW_MOUSE_BUTTON_RIGHT:
                return event::MouseButton::RIGHT;
            default:
                return std::nullopt;
        }
    }

    static void
    mouseButtonCallback(GLFWwindow *glfwWindow, int inButton, int action, int) {
        auto *window = reinterpret_cast<GLFWWindow *>(glfwGetWindowUserPointer(glfwWindow));

        const auto button = convertMouseButton(inButton);
        if (!button.has_value())
            return;

        if (action == GLFW_RELEASE) {
            if (window->m_mouseReleaseCallback) {
                window->m_mouseReleaseCallback(event::MouseRelease{
                    button.value(), getMousePosition(glfwWindow)
                });
            }
        } else if (window->m_mouseClickCallback) {
            window->m_mouseClickCallback(event::MouseClick{
                    button.value(), getMousePosition(glfwWindow)
            });
        }
    }

    static void
    mouseMoveCallback(GLFWwindow *glfwWindow, double xpos, double ypos) {
        const gfx::Position<float> position{static_cast<float>(xpos), static_cast<float>(ypos)};
        auto *window = reinterpret_cast<GLFWWindow *>(glfwGetWindowUserPointer(glfwWindow));

        if (window->m_mouseMoveCallback)
            window->m_mouseMoveCallback(event::MouseMove{position, window->m_cursorPos});

        window->m_cursorPos = position;
    }

    static void
    framebufferSizeCallback(GLFWwindow *glfwWindow, int width, int height) noexcept {
        auto *window = reinterpret_cast<GLFWWindow *>(glfwGetWindowUserPointer(glfwWindow));

        if (window->m_resizeCallback) {
            window->m_resizeCallback(event::WindowResize{{static_cast<std::uint32_t>(width),
                                                          static_cast<std::uint32_t>(height)}});
        }
    }

    GLFWWindow::~GLFWWindow() noexcept {
        std::lock_guard guard{g_windowMutex};
        --g_windowCounter;

        destroyCursors();

        if (m_window != nullptr) {
            close();
        }

        if (g_windowCounter == 0) {
            glfwTerminate();
        }
    }

    void
    GLFWWindow::close() noexcept {
        glfwDestroyWindow(m_window);
        m_window = nullptr;
    }

    bool
    GLFWWindow::closeRequested() const noexcept {
        return static_cast<bool>(glfwWindowShouldClose(m_window));
    }

    bool
    GLFWWindow::create(GraphicsAPI graphicsAPI, std::string_view title, gfx::Size<std::uint32_t> size) noexcept {
        std::lock_guard guard{g_windowMutex};
        ++g_windowCounter;

        if (!glfwInit()) {
            LOG_ERROR("Failed to glfwInit()");
            return false;
        }

        switch (graphicsAPI) {
            case GraphicsAPI::OPENGL:
                glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_API);
                break;
            case GraphicsAPI::VULKAN:
                glfwWindowHint(GLFW_CLIENT_API, GLFW_NO_API);
                break;
            default:
                LOG_ERROR("Invalid GraphicsAPI passed to GLFWWindow");
                return false;
        }

        m_window = glfwCreateWindow(static_cast<int>(size.width()), static_cast<int>(size.height()),
                                    std::string(title).c_str(), nullptr, nullptr);
        if (m_window == nullptr) {
            LOG_ERROR("Failed to glfwCreateWindow()");
            return false;
        }

        if (graphicsAPI == GraphicsAPI::OPENGL) {
            glfwMakeContextCurrent(m_window);
        }

        glfwSetWindowUserPointer(m_window, this);
        glfwSetMouseButtonCallback(m_window, mouseButtonCallback);
        glfwSetCursorPosCallback(m_window, mouseMoveCallback);
        glfwSetFramebufferSizeCallback(m_window, framebufferSizeCallback);

        if (!loadCursors())
            return false;

        m_cursorPos = getMousePosition(m_window);

        return true;
    }

    void
    GLFWWindow::destroyCursors() noexcept {
#if (GUI_GLFW_WINDOW_USE_X11_CURSORS == 1)

#else
        glfwDestroyCursor(m_cursorArrow);
        glfwDestroyCursor(m_cursorHand);
        glfwDestroyCursor(m_cursorIBeam);
#endif
    }

    gfx::Size<std::uint32_t>
    GLFWWindow::framebufferSize() const noexcept {
        int width, height;
        glfwGetFramebufferSize(m_window, &width, &height);
        return {static_cast<std::uint32_t>(width), static_cast<std::uint32_t>(height)};
    }

    bool
    GLFWWindow::isKeyDown(Key key) noexcept {
        auto it = std::find_if(std::cbegin(s_keyMap), std::cend(s_keyMap), [key] (const auto &pair) {
            return pair.second == key;
        });

        if (it == std::cend(s_keyMap))
            return false;

        return glfwGetKey(m_window, it->first) == GLFW_PRESS;
    }

    bool
    GLFWWindow::isClosed() const noexcept {
        return m_window == nullptr;
    }

    bool
    GLFWWindow::loadCursors() noexcept {
#if (GUI_GLFW_WINDOW_USE_X11_CURSORS == 1)
        auto *const display = glfwGetX11Display();
        m_cursorArrow = XcursorLibraryLoadCursor(display, "left_ptr");
        m_cursorHand = XcursorLibraryLoadCursor(display, "hand2");
        m_cursorIBeam = XcursorLibraryLoadCursor(display, "xterm");
        m_cursorWait = XcursorLibraryLoadCursor(display, "watch");
        return true;
#else
        m_cursorArrow = glfwCreateStandardCursor(GLFW_ARROW_CURSOR);
        m_cursorHand = glfwCreateStandardCursor(GLFW_HAND_CURSOR);
        m_cursorIBeam = glfwCreateStandardCursor(GLFW_IBEAM_CURSOR);
        return true;
#endif
    }

    void
    GLFWWindow::poll() noexcept {
        glfwPollEvents();
    }

    void
    GLFWWindow::setCursor(CursorType cursor) noexcept {
#if GUI_GLFW_WINDOW_USE_X11_CURSORS == 1
        auto *const display = glfwGetX11Display();
        const auto window = glfwGetX11Window(m_window);
        switch (cursor) {
            case CursorType::ARROW:
                XDefineCursor(display, window, m_cursorArrow);
                break;
            case CursorType::HAND:
                XDefineCursor(display, window, m_cursorHand);
                break;
            case CursorType::I_BEAM:
                XDefineCursor(display, window, m_cursorIBeam);
                break;
            case CursorType::WAIT:
                XDefineCursor(display, window, m_cursorWait);
                break;
        }
#else
        switch (cursor) {
            case CursorType::ARROW:
                glfwSetCursor(m_window, m_cursorArrow);
                break;
            case CursorType::HAND:
                glfwSetCursor(m_window, m_cursorHand);
                break;
            case CursorType::I_BEAM:
                glfwSetCursor(m_window, m_cursorIBeam);
                break;
            // These are unfortunately unsupported by GLFW standard cursors :( :
            case CursorType::WAIT:
                break;
        }
#endif
    }

    void
    GLFWWindow::setTitle(const std::string &title) noexcept {
        glfwSetWindowTitle(m_window, title.c_str());
    }

    void
    GLFWWindow::setTitle(std::string_view title) noexcept {
        glfwSetWindowTitle(m_window, std::string(title).c_str());
    }

    void
    GLFWWindow::swapBuffers() noexcept {
        glfwSwapBuffers(m_window);
    }

} // namespace gui
