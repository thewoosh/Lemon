/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string>

#include "Source/DOM/CharacterData.hpp"

namespace dom {

    struct Text
            : public CharacterData {
        [[nodiscard]] inline
        Text(Node *parent, std::string &&data) noexcept
                : CharacterData(NodeType::TEXT, parent, std::move(data)) {
        }
    };

} // namespace dom
