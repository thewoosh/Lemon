/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string_view>

namespace dom {

    enum class NodeType {
        ELEMENT = 1,
        ATTRIBUTE = 2,
        TEXT = 3,
        CDATA_SECTION = 4,
        ENTITY_REFERENCE = 5,
        ENTITY = 6,
        PROCESSING_INSTRUCTION = 7,
        COMMENT = 8,
        DOCUMENT = 9,
        DOCUMENT_TYPE = 10,
        DOCUMENT_FRAGMENT = 11,
        NOTATION = 12
    };

    [[nodiscard]] inline constexpr std::string_view
    toString(NodeType nodeType) noexcept {
        switch (nodeType) {
            case NodeType::ELEMENT: return "ELEMENT_NODE";
            case NodeType::ATTRIBUTE: return "ELEMENT_NODE";
            case NodeType::TEXT: return "TEXT_NODE";
            case NodeType::CDATA_SECTION: return "CDATA_SECTION_NODE";
            case NodeType::ENTITY_REFERENCE: return "ENTITY_REFERENCE_NODE";
            case NodeType::ENTITY: return "ENTITY_NODE";
            case NodeType::PROCESSING_INSTRUCTION: return "PROCESSING_INSTRUCTION_NODE";
            case NodeType::COMMENT: return "COMMENT_NODE";
            case NodeType::DOCUMENT: return "DOCUMENT_NODE";
            case NodeType::DOCUMENT_TYPE: return "DOCUMENT_TYPE_NODE";
            case NodeType::DOCUMENT_FRAGMENT: return "DOCUMENT_FRAGMENT_NODE";
            case NodeType::NOTATION: return "NOTATION_NODE";
            default: return "(invalid)";
        }
    }

} // namespace dom
