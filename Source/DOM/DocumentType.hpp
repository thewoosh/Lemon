/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string>
#include <utility>

#include "Source/DOM/Node.hpp"

namespace dom {

    struct DocumentType
            : public Node {
        [[nodiscard]] inline
        DocumentType(Node *parent, std::string &&name, std::string &&publicId={}, std::string &&systemId={}) noexcept
                : Node(NodeType::DOCUMENT_TYPE, parent)
                , m_name(std::move(name))
                , m_publicId(std::move(publicId))
                , m_systemId(std::move(systemId)) {
        }

        [[nodiscard]] inline const std::string &
        name() const noexcept {
            return m_name;
        }

        [[nodiscard]] inline const std::string &
        publicId() const noexcept {
            return m_publicId;
        }

        [[nodiscard]] inline const std::string &
        systemId() const noexcept {
            return m_systemId;
        }

    private:
        const std::string m_name;
        const std::string m_publicId;
        const std::string m_systemId;
    };

} // namespace dom
