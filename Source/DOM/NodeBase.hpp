/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include "Source/DOM/NodeType.hpp"

#include <memory>
#include <string>
#include <vector>
#include <utility>

namespace dom {

    /**
     * NodeBase is the base class both of Node and Attr. Attr doesn't need
     * inheritance and children support, so it inherits this class instead.
     */
    struct NodeBase {
        [[nodiscard]] inline constexpr NodeType
        nodeType() const noexcept {
            return m_nodeType;
        }

    protected:
        [[nodiscard]] inline constexpr explicit
        NodeBase(NodeType nodeType) noexcept
                : m_nodeType(nodeType) {
        }

    private:
        NodeType m_nodeType;
    };

} // namespace dom
