/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include "Source/DOM/NodeBase.hpp"
#include "Source/DOM/NodeType.hpp"
#include "Source/GUI/MouseButton.hpp"

#include <functional>
#include <memory>
#include <string>
#include <utility>
#include <vector>

namespace dom {

    struct Element;

    struct Node
            : public NodeBase {
        virtual
        ~Node() noexcept = default;

        inline void
        appendChild(std::unique_ptr<Node> &&node) noexcept {
            m_children.push_back(std::move(node));
        }

        [[nodiscard]] inline std::vector<std::unique_ptr<Node>> &
        children() noexcept {
            return m_children;
        }

        [[nodiscard]] inline const std::vector<std::unique_ptr<Node>> &
        children() const noexcept {
            return m_children;
        }

        [[nodiscard]] inline dom::Node *
        parent() const noexcept {
            return m_parent;
        }

        Element *
        findImmediateChildWithTagName(std::string_view tagName) const noexcept;

        void
        forEachChildElementByTagName(std::string_view tagName, const std::function<void(Element *)> &callback) noexcept;

        [[nodiscard]] std::string
        toString() const noexcept;

        std::function<void(gui::event::MouseButton)> onClick{};

    protected:
        [[nodiscard]] inline
        Node(NodeType nodeType, Node *parent) noexcept
                : NodeBase(nodeType)
                , m_parent(parent) {
        }

    private:
        Node *const m_parent;
        std::vector<std::unique_ptr<Node>> m_children;
    };

    void
    printNode(const Node *, std::size_t depth = 0) noexcept;

} // namespace dom
