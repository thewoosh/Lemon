/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

namespace dom {

    struct NamedNodeMap  {
        [[nodiscard]] virtual std::size_t
        length() const noexcept = 0;

        [[nodiscard]] virtual Attr *
        item(std::size_t index) noexcept = 0;

        [[nodiscard]] virtual const Attr *
        item(std::size_t index) const noexcept = 0;

        [[nodiscard]] virtual const Attr *
        getNamedItem(std::string_view qualifiedName) const noexcept = 0;
    };

} // namespace dom
