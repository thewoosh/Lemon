/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/DOM/Attr.hpp"

#include <fmt/core.h>

#include "Include/fmt.hpp"

namespace dom {

    std::string
    Attr::name() noexcept {
        if (!m_namespacePrefix.has_value())
            return m_localName;

        return fmt::format(FMTLIB_WRAPPER("{}:{}"), m_namespacePrefix.value(), m_localName);
    }

} // namespace dom
