/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/DOM/Element.hpp"

#include "Source/Base/Assert.hpp"
#include "Source/HTML/Tools/CreateElement.hpp"
#include "Source/Infra/Namespaces.hpp"

namespace dom {

    void
    Element::reset() noexcept {
        // To be overridden by an element that has a reset algorithm
        // https://html.spec.whatwg.org/multipage/forms.html#category-reset
        // https://html.spec.whatwg.org/multipage/form-control-infrastructure.html#concept-form-reset-control
    }

    void
    appendAttributeToElement(Element *element, std::string_view name, std::string_view value) noexcept {
        // TODO handle attribute changes
        auto &entry = element->attributes().emplace_back(std::string(name), std::string(value));

        if (name == "class") {
            std::string_view remaining = entry.value();
            while (!std::empty(remaining)) {
                auto space = remaining.find(' ');
                std::string_view clazz;
                if (space != std::string_view::npos) {
                    clazz = remaining.substr(0, space);
                    remaining = remaining.substr(space + 1);
                } else {
                    clazz = remaining;
                    remaining = {};
                }

                if (std::empty(clazz))
                    continue;

                if (std::find(std::cbegin(element->classList()), std::cend(element->classList()), clazz)
                        == std::end(element->classList())) {
                    element->classList().emplace_back(clazz);
                }
            }
        }
    }

    /**
     * https://dom.spec.whatwg.org/#concept-create-element
     */
    std::unique_ptr<Element>
    createElement(Document *document, Node *parent, std::string &&localName, std::string_view inNamespace,
                  std::optional<std::string> &&prefix, std::optional<std::string_view> is,
                  std::optional<bool> synchronousCustomElements) {
        static_cast<void>(document);
        static_cast<void>(prefix);
        static_cast<void>(is);
        static_cast<void>(synchronousCustomElements);

        // TODO we should do custom element resolving here in the future

        if (inNamespace == infra::namespaces::HTML) {
            return html::createElement(document, parent, std::move(localName));
        }

        LEMON_ASSERT_NOT_REACHED_OR_RETURN(nullptr);
    }

} // namespace dom
