/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string_view>

namespace dom {

    /**
     * https://dom.spec.whatwg.org/#concept-document-quirks
     */
    enum class QuirksMode {
        INITIAL,

        NO_QUIRKS,
        QUIRKS,
        LIMITED_QUIRKS,
    };

    [[nodiscard]] inline constexpr std::string_view
    toString(QuirksMode quirksMode) noexcept {
        switch (quirksMode) {
            case QuirksMode::NO_QUIRKS: return "no-quirks";
            case QuirksMode::QUIRKS: return "quirks";
            case QuirksMode::LIMITED_QUIRKS: return "limited-quirks";
            default: return "(invalid)";
        }
    }

} // namespace dom
