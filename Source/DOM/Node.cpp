/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/DOM/Node.hpp"

#include "Source/DOM/Element.hpp"

namespace dom {

    Element *
    Node::findImmediateChildWithTagName(std::string_view tagName) const noexcept {
        for (auto &child : m_children) {
            if (child->nodeType() == NodeType::ELEMENT) {
                auto *element = static_cast<Element *>(child.get());
                if (element->localName() == tagName) {
                    return element;
                }
            }
        }
        return nullptr;
    }

    void
    Node::forEachChildElementByTagName(std::string_view tagName, const std::function<void(Element *)> &callback) noexcept {
        for (auto &child : m_children) {
            if (child->nodeType() == NodeType::ELEMENT) {
                auto *element = static_cast<Element *>(child.get());
                if (element->localName() == tagName) {
                    callback(element);
                }
            }

            child->forEachChildElementByTagName(tagName, callback);
        }
    }

    std::string
    Node::toString() const noexcept {
        if (nodeType() == NodeType::ELEMENT) {
            return fmt::format("ELEMENT {}", static_cast<const Element *>(this)->localName());
        }

        return std::string(dom::toString(nodeType()));
    }

    void
    printNode(const Node *node, std::size_t depth) noexcept {
        LOG_DEBUG("{}{}", std::string(depth * 2, ' '), node->toString());
        for (const auto &child : node->children()) {
            printNode(child.get(), depth + 1);
        }
    }

} // namespace dom
