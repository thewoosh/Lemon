/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include "Source/DOM/Node.hpp"
#include "Source/DOM/QuirksMode.hpp"

namespace html {
    struct HTMLBodyElement;
    struct HTMLHeadElement;
    struct HTMLHtmlElement;
    struct HTMLStyleElement;
} // namespace html

namespace dom {

    struct Document
            : public Node {
        [[nodiscard]] inline
        Document() noexcept
                : Node(NodeType::DOCUMENT, nullptr) {
        }

        [[nodiscard]] inline constexpr html::HTMLBodyElement *
        bodyElement() const noexcept {
            return m_bodyElement;
        }

        /**
         * https://html.spec.whatwg.org/multipage/parsing.html#frameset-ok-flag
         */
        [[nodiscard]] inline constexpr bool
        framesetOK() const noexcept {
            return m_framesetOK;
        }

        [[nodiscard]] inline constexpr html::HTMLHeadElement *
        headElement() const noexcept {
            return m_headElement;
        }

        [[nodiscard]] inline constexpr html::HTMLHtmlElement *
        htmlElement() const noexcept {
            return m_htmlElement;
        }

        [[nodiscard]] inline constexpr bool
        isIframeSrcdocDocument() const noexcept {
            return m_isIframeSrcdocDocument;
        }

        [[nodiscard]] inline constexpr bool
        parserCannotChangeTheModeFlag() const noexcept {
            return m_parserCannotChangeTheModeFlag;
        }

        void
        postLoadHooks() noexcept;

        [[nodiscard]] inline constexpr QuirksMode
        quirksMode() const noexcept {
            return m_quirksMode;
        }

        /**
         * Tells whether or not to enable scripting in this document.
         *
         * https://html.spec.whatwg.org/multipage/parsing.html#scripting-flag
         */
        [[nodiscard]] inline constexpr bool
        scriptingFlag() const noexcept {
            return m_scriptingFlag;
        }

        inline constexpr void
        setFramesetFlagToNotOK() noexcept {
            m_framesetOK = false;
        }

        inline constexpr void
        setQuirksMode(QuirksMode quirksMode) noexcept {
            m_quirksMode = quirksMode;
        }

        [[nodiscard]] inline std::vector<html::HTMLStyleElement *> &
        styles() noexcept {
            return m_styles;
        }

        [[nodiscard]] inline const std::vector<html::HTMLStyleElement *> &
        styles() const noexcept {
            return m_styles;
        }

        [[nodiscard]] inline constexpr std::string_view
        title() const noexcept {
            return m_title;
        }

    private:
        bool m_scriptingFlag{false};
        bool m_framesetOK{true};
        bool m_isIframeSrcdocDocument{false};
        bool m_parserCannotChangeTheModeFlag{false};
        QuirksMode m_quirksMode{QuirksMode::NO_QUIRKS};

        std::vector<html::HTMLStyleElement *> m_styles;

        html::HTMLBodyElement *m_bodyElement{};
        html::HTMLHeadElement *m_headElement{};
        html::HTMLHtmlElement *m_htmlElement{};

        std::string_view m_title{};
    };

} // namespace dom
