/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <memory>
#include <string_view>

#include "Source/DOM/Attr.hpp"
#include "Source/DOM/Node.hpp"

namespace dom {

    struct Document;

    struct Element
            : public Node {
        [[nodiscard]] inline
        Element(Node *parent, std::string &&localName) noexcept
                : Node(NodeType::ELEMENT, parent)
                , m_localName(std::move(localName)) {
        }

        virtual
        ~Element() noexcept = default;

        [[nodiscard]] inline const std::string *
        firstAttribute(std::string_view name) noexcept {
            const auto ret = std::find_if(std::cbegin(m_attributes), std::cend(m_attributes),
                                          [&] (const Attr &attribute) -> bool {
                                              return attribute.localName() == name;
                                          });

            if (ret == std::cend(m_attributes))
                return nullptr;
            return &ret->value();
        }

        /**
         * For-each-es the attributes with the specified name.
         *
         * The callback may return a boolean to indicate whether or not to
         * continue looping through the queried attributes.
         */
        template<typename Callback>
        inline void
        attributesWithName(std::string_view name, Callback callback) const noexcept {
            for (const auto &attribute : m_attributes) {
                if (attribute.localName() == name) {
                    if constexpr (std::is_same_v<decltype(callback(attribute.value())), void>)
                        callback(attribute.value());
                    else if (!callback(attribute.value()))
                        return;
                }
            }
        }

        [[nodiscard]] inline std::vector<Attr> &
        attributes() noexcept {
            return m_attributes;
        }

        [[nodiscard]] inline const std::vector<Attr> &
        attributes() const noexcept {
            return m_attributes;
        }

        [[nodiscard]] inline std::vector<std::string> &
        classList() noexcept {
            return m_classList;
        }

        [[nodiscard]] inline const std::vector<std::string> &
        classList() const noexcept {
            return m_classList;
        }

        [[nodiscard]] inline std::optional<std::string_view>
        namespacePrefix() const noexcept {
            return m_namespacePrefix;
        }

        [[nodiscard]] inline std::optional<std::string_view>
        namespaceURI() const noexcept {
            return m_namespaceURI;
        }

        [[nodiscard]] inline const std::string &
        localName() const noexcept {
            return m_localName;
        }

        /**
         * To be overridden by an element that has a reset algorithm
         * https://html.spec.whatwg.org/multipage/forms.html#category-reset
         * https://html.spec.whatwg.org/multipage/form-control-infrastructure.html#concept-form-reset-control
         */
        virtual void
        reset() noexcept;

    protected:
        std::optional<std::string> m_namespaceURI{};
        std::optional<std::string> m_namespacePrefix{};
        std::string m_localName;

        std::vector<Attr> m_attributes{};

        std::vector<std::string> m_classList{};
    };

    /**
     * https://dom.spec.whatwg.org/#concept-element-attributes-append
     */
    void
    appendAttributeToElement(Element *, std::string_view name, std::string_view value) noexcept;

    /**
     * https://dom.spec.whatwg.org/#concept-create-element
     */
    [[nodiscard]] std::unique_ptr<Element>
    createElement(Document *, Node *parent, std::string &&localName, std::string_view inNamespace,
                  std::optional<std::string> &&prefix=std::nullopt, std::optional<std::string_view> is=std::nullopt,
                  std::optional<bool> synchronousCustomElements=std::nullopt);

} // namespace dom
