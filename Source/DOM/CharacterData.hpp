/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string>

#include "Source/DOM/Node.hpp"

namespace dom {

    struct CharacterData
            : public Node {
        [[nodiscard]] inline
        CharacterData(NodeType nodeType, Node *parent, std::string &&data) noexcept
                : Node(nodeType, parent)
                , m_data(std::move(data)) {
        }

        [[nodiscard]] inline std::string &
        data() noexcept {
            return m_data;
        }

        [[nodiscard]] inline const std::string &
        data() const noexcept {
            return m_data;
        }

    private:
        std::string m_data;
    };

} // namespace dom
