/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <optional>
#include <string>

#include "Source/DOM/NodeBase.hpp"

namespace dom {

    struct Attr
            : public NodeBase {
        [[nodiscard]] inline explicit
        Attr(std::string &&localName={}, std::string &&value={},
             std::optional<std::string> &&namespaceURI={}, std::optional<std::string> &&namespacePrefix={}) noexcept
                : NodeBase(NodeType::ATTRIBUTE)
                , m_namespaceURI(std::move(namespaceURI))
                , m_namespacePrefix(std::move(namespacePrefix))
                , m_localName(std::move(localName))
                , m_value(std::move(value)) {
        }

        [[nodiscard]] inline std::string &
        localName() noexcept {
            return m_localName;
        }

        [[nodiscard]] inline const std::string &
        localName() const noexcept {
            return m_localName;
        }

        [[nodiscard]] std::string
        name() noexcept;

        [[nodiscard]] inline std::optional<std::string> &
        namespacePrefix() noexcept {
            return m_namespacePrefix;
        }

        [[nodiscard]] inline const std::optional<std::string> &
        namespacePrefix() const noexcept {
            return m_namespacePrefix;
        }

        [[nodiscard]] inline std::optional<std::string> &
        namespaceURI() noexcept {
            return m_namespaceURI;
        }

        [[nodiscard]] inline const std::optional<std::string> &
        namespaceURI() const noexcept {
            return m_namespaceURI;
        }

        [[nodiscard]] inline std::string &
        value() noexcept {
            return m_value;
        }

        [[nodiscard]] inline const std::string &
        value() const noexcept {
            return m_value;
        }

    private:
        std::optional<std::string> m_namespaceURI{};
        std::optional<std::string> m_namespacePrefix{};
        std::string m_localName;
        std::string m_value;
    };

} // namespace dom
