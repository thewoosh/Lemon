/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/DOM/Document.hpp"

#include "Source/Base/Assert.hpp"
#include "Source/DOM/Text.hpp"
#include "Source/HTML/Elements/HTMLBodyElement.hpp"
#include "Source/HTML/Elements/HTMLHeadElement.hpp"
#include "Source/HTML/Elements/HTMLHtmlElement.hpp"

namespace dom {

    void
    Document::postLoadHooks() noexcept {
        m_htmlElement = static_cast<html::HTMLHtmlElement *>(findImmediateChildWithTagName("html"));
        LEMON_ASSERT(m_htmlElement);

        m_headElement = static_cast<html::HTMLHeadElement *>(m_htmlElement->findImmediateChildWithTagName("head"));
        LEMON_ASSERT(m_headElement);

        m_bodyElement = static_cast<html::HTMLBodyElement *>(m_htmlElement->findImmediateChildWithTagName("body"));
        LEMON_ASSERT(m_bodyElement);

        for (const auto &child : m_headElement->children()) {
            if (child->nodeType() != NodeType::ELEMENT)
                continue;

            const auto *element = static_cast<const dom::Element *>(child.get());
            if (element->namespaceURI() != infra::namespaces::HTML)
                continue;

            if (!std::empty(element->children()) && element->localName() == "title") {
                m_title = static_cast<const dom::Text *>(element->children().front().get())->data();
            }
        }
    }

} // namespace dom
