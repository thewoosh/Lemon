/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include <string_view>

#include <cstdio>

#include "Source/Application.hpp"
#include "Source/Base/About.hpp"
#include "Source/Base/ExitCode.hpp"
#include "Source/Base/Logger.hpp"

[[nodiscard]] int
printInformationAndExit() noexcept;

int
main(int argc, char *argv[]) noexcept {
    std::string_view fileName{LEMON_MAIN_DEFAULT_FILE};

    for (std::size_t i = 1; i < static_cast<std::size_t>(argc); ++i) {
        const std::string_view arg{argv[i]};
        if (arg.starts_with("--")) {
            if (arg == "--about"
                    || arg == "--copying"
                    || arg == "--copyright"
                    || arg == "--info"
                    || arg == "--information"
                    || arg == "--version") {
                return printInformationAndExit();
            }
        } else {
            fileName = arg;
        }
    }

    if (std::empty(fileName)) {
        LOG_ERROR("Empty file path specified!");
        return base::ExitCode::NoFileSpecified;
    }

    Application application{};
    return application.run(fileName) ? EXIT_SUCCESS : EXIT_FAILURE;
}

int
printInformationAndExit() noexcept {
    std::printf("Lemon Web Browser\n"
                "Version: %hhu.%hhu.%hhu\n"
                "\n"
                "Copyright (C) 2021 Tristan Gerritsen\n"
                "All Rights Reserved\n"
                "Licensed under the Mozilla Public License 2.0\n",
                base::About::version.major(), base::About::version.minor(), base::About::version.patch());
    return base::ExitCode::Success;
}
