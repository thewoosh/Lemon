/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string>
#include <string_view>

#include <cstddef> // for std::size_t
#include <cstdio> // for std::FILE

#include "Source/Base/CompilerFeatures.hpp"
#include "Source/IO/FileResult.hpp"

namespace io {

    class FileReader {
    public:
        [[nodiscard]] explicit
        FileReader(const std::string &name) noexcept;

        [[nodiscard]] explicit
        FileReader(std::string_view name) noexcept;

        [[nodiscard]] inline constexpr explicit
        operator bool() const noexcept {
            return m_file != nullptr;
        }

        [[nodiscard]] FileResult
        read(std::size_t size) noexcept;

        [[nodiscard]] FileResult
        readAll() noexcept;

        [[nodiscard]] inline constexpr std::size_t
        size() const noexcept {
            return m_size;
        }

    private:
        void
        initialize(const char *) noexcept;

        std::FILE *m_file{nullptr};
        std::size_t m_size{0};
    };
} // namespace io
