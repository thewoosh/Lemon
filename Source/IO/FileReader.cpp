/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/IO/FileReader.hpp"

namespace io {

    FileReader::FileReader(const std::string &name) noexcept {
        initialize(name.c_str());
    }

    FileReader::FileReader(std::string_view name) noexcept {
        initialize(std::string(name).c_str());
    }

    void
    FileReader::initialize(const char *path) noexcept {
        m_file = std::fopen(path, "rb");
        if (m_file == nullptr)
            return;

        // Calculate file size
        std::fseek(m_file, 0L, SEEK_END);
        m_size = static_cast<std::size_t>(std::ftell(m_file));
        std::rewind(m_file);
    }

    FileResult
    FileReader::read(std::size_t size) noexcept {
        if (m_file == nullptr)
            return {false, {}};

        std::string string{};
        string.resize(size);

        char *ptr{std::data(string)};

        while (size > 0) {
            auto result = std::fread(ptr, sizeof(*ptr), size, m_file);
            if (result == 0)
                return {false, std::move(string)};

            result *= sizeof(*ptr);
            ptr += result;
            size -= result;
        }

        return {true, std::move(string)};
    }

    FileResult
    FileReader::readAll() noexcept {
        // TODO should this function read the rest of the file, instead of
        //      this?
        return read(m_size);
    }

} // namespace io
