/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string>
#include <utility>

#include "Source/Base/CompilerFeatures.hpp"

namespace io {

    struct FileResult {
        [[nodiscard]] inline LEMON_CONSTEXPR_STRING
        FileResult(bool couldRead, std::string &&contents) noexcept
                : m_couldRead(couldRead)
                , m_contents(std::move(contents)) {
        }

        [[nodiscard]] inline LEMON_CONSTEXPR_STRING std::string &
        contents() noexcept {
            return m_contents;
        }

        [[nodiscard]] inline LEMON_CONSTEXPR_STRING const std::string &
        contents() const noexcept {
            return m_contents;
        }

        [[nodiscard]] inline constexpr bool
        couldRead() const noexcept {
            return m_couldRead;
        }

    private:
        bool m_couldRead;
        std::string m_contents;
    };

} // namespace io
