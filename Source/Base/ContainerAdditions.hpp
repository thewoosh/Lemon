/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <vector>

namespace base {

    template <typename T, typename... Args>
    [[nodiscard]] std::vector<T>
    createVector(Args&&... args) {
        std::vector<T> vec{};
        vec.reserve(sizeof...(Args));

        using junkType = int[];
        static_cast<void>(junkType{0, (vec.emplace_back(std::forward<Args>(args)), 0)...});
        return vec;
    }

} // namespace base
