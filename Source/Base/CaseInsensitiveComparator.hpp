/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <algorithm> // for std::lexicographical_compare
#include <functional> // for std::binary_function
#include <string_view>

#include "Include/text/Tools.hpp"

namespace base {

    struct CaseInsensitiveComparator : std::binary_function<std::string_view, std::string_view, bool> {
        [[nodiscard]] inline constexpr bool
        operator() (std::string_view a, std::string_view b) const noexcept {
            return std::lexicographical_compare(std::cbegin(a), std::cend(a), std::cbegin(b), std::cend(b),
                     [] (char characterA, char characterB) {
                         return text::toASCIILowercase(characterA) < text::toASCIILowercase(characterB);
                     }
            );
        }
    };

} // namespace base
