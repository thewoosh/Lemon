/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/Base/Logger.hpp"

#include <cstdlib> // for std::abort

#undef LEMON_ASSERT

#ifdef __PRETTY_FUNCTION__
#   define LEMON_FUNCTION __PRETTY_FUNCTION__
#elif defined(__FUNCSIG__)
#   define LEMON_FUNCTION __FUNCSIG__
#else
#   define LEMON_FUNCTION __func__
#endif

#ifdef NDEBUG
#   define LEMON_ASSERT(e) ((void)0)
#   define LEMON_ASSERT_NOT_REACHED_OR_RETURN_VOID() return
#   define LEMON_ASSERT_NOT_REACHED_OR_RETURN(retval) return retval
#else
#   define _LEMON_ASSERT_GENERAL \
        LOG_ERROR("File: {}:{}", __FILE__, __LINE__); \
        LOG_ERROR("Function: {}", LEMON_FUNCTION);   \
        std::abort();

#   define LEMON_ASSERT(e) \
        do {                \
            if (!(e)) {          \
                LOG_ERROR("Assertion failed: {}", #e); \
                _LEMON_ASSERT_GENERAL \
            } \
        } while (0)

#   define LEMON_ASSERT_NOT_REACHED_OR_RETURN_VOID() \
        do { \
            LOG_ERROR("Unreachable code reached!"); \
            _LEMON_ASSERT_GENERAL \
        } while(0)

#   define LEMON_ASSERT_NOT_REACHED_OR_RETURN(retval) \
        do { \
            static_cast<void>(retval); \
            LOG_ERROR("Unreachable code reached!"); \
            _LEMON_ASSERT_GENERAL \
        } while(0)
#endif
