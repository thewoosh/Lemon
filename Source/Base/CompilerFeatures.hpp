/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 *
 * https://gcc.gnu.org/projects/cxx-status.html
 * https://libcxx.llvm.org/Status/Cxx20.html
 * https://docs.microsoft.com/en-us/cpp/overview/visual-cpp-language-conformance
 */

#pragma once

#if defined(_MSC_VER) && _MSC_VER >= 1929
//  constexpr vector support arrived in VS 2019 16.10
#   define LEMON_CONSTEXPR_STRING constexpr
#   define LEMON_CONSTEXPR_VECTOR constexpr
#else
#   define LEMON_CONSTEXPR_STRING
#   define LEMON_CONSTEXPR_VECTOR
#endif
