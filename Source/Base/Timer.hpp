/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

namespace base {

    class Timer {
    public:
        using Unit = long long;

        [[nodiscard]] static Unit
        now() noexcept;

        [[nodiscard]] inline
        Timer() noexcept
                : m_start(now()) {
        }

        [[nodiscard]] inline constexpr Unit
        delta() const noexcept {
            return m_end - m_start;
        }

        inline void
        start() noexcept {
            m_end = {};
            m_start = now();
        }

        inline void
        stop() noexcept {
            m_end = now();
        }

    private:
        Unit m_start{};
        Unit m_end{};
    };

} // namespace base
