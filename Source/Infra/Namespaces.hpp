/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include "Source/DOM/NodeType.hpp"

#include <string_view>

namespace infra::namespaces {

    constexpr const std::string_view HTML{"http://www.w3.org/1999/xhtml"};
    constexpr const std::string_view MathML{"http://www.w3.org/1998/Math/MathML"};
    constexpr const std::string_view SVG{"http://www.w3.org/2000/svg"};
    constexpr const std::string_view XLink{"http://www.w3.org/1999/xlink"};
    constexpr const std::string_view XML{"http://www.w3.org/XML/1998/namespace"};
    constexpr const std::string_view XMLNS{"http://www.w3.org/2000/xmlns/"};

} // namespace infra::namespaces
