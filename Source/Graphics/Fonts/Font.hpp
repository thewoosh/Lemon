/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <map>
#include <optional>

#ifdef __GNUC__
#   pragma GCC diagnostic push
#   pragma GCC diagnostic ignored "-Wold-style-cast"
#endif
#include <freetype2/ft2build.h>
#include FT_FREETYPE_H
#include FT_ADVANCES_H
#include FT_ERRORS_H
#ifdef __GNUC__
#   pragma GCC diagnostic pop
#endif

#include "Source/Graphics/Fonts/Character.hpp"

namespace gfx {

    class Font;

    class GeneratedCharacterMap {
    public:
        [[nodiscard]] inline explicit
        GeneratedCharacterMap(Font *) noexcept {
        }

        [[nodiscard]] inline std::map<char32_t, Character> &
        characterMap() noexcept {
            return m_characterMap;
        }

        [[nodiscard]] inline const std::map<char32_t, Character> &
        characterMap() const noexcept {
            return m_characterMap;
        }

        inline constexpr void
        setVerticalAdvance(float value) noexcept {
            m_verticalAdvance = value;
        }

        [[nodiscard]] inline constexpr float
        verticalAdvance() const noexcept {
            return m_verticalAdvance;
        }

        inline constexpr void
        setDescender(float value) noexcept {
            m_descender = value;
        }

        [[nodiscard]] inline constexpr float
        descender() const noexcept {
            return m_descender;
        }

        inline constexpr void
        setPixelSize(float value) noexcept {
            m_pixelSize = value;
        }

        [[nodiscard]] inline constexpr float
        pixelSize() const noexcept {
            return m_pixelSize;
        }

    private:
        std::map<char32_t, Character> m_characterMap{};
        float m_verticalAdvance{};
        float m_descender{};
        float m_pixelSize{};
    };

    class Font {
    public:
        [[nodiscard]] Font(Font &&) noexcept = default;
        Font &operator=(Font &&) noexcept = default;

        [[nodiscard]] bool
        loadFromFile(std::string_view path) noexcept;

        ~Font() noexcept;

        [[nodiscard]] inline std::map<std::uint32_t, GeneratedCharacterMap> &
        maps() noexcept {
            return m_maps;
        }

        [[nodiscard]] inline const std::map<std::uint32_t, GeneratedCharacterMap> &
        maps() const noexcept {
            return m_maps;
        }

        [[nodiscard]] const Character *
        characterOfSize(std::uint32_t size, char32_t character) noexcept;

        [[nodiscard]] inline FT_Face &
        ftFace() noexcept {
            return m_ftFace;
        }

        [[nodiscard]] inline const FT_Face &
        ftFace() const noexcept {
            return m_ftFace;
        }

        [[nodiscard]] inline FT_Library &
        ftLibrary() noexcept {
            return m_ftLibrary;
        }

        [[nodiscard]] inline const FT_Library &
        ftLibrary() const noexcept {
            return m_ftLibrary;
        }

        [[nodiscard]] Font() noexcept = default;

    private:
        std::map<std::uint32_t, GeneratedCharacterMap> m_maps{};
        FT_Library m_ftLibrary{};
        FT_Face m_ftFace{};
    };

} // namespace gfx
