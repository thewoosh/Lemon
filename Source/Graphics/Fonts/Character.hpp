/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include "Source/Graphics/OpenGL/Resources/Texture2D.hpp"
#include "Source/Graphics/Types/Position.hpp"

namespace gfx {

    struct Character {
        [[nodiscard]] inline
        Character(gle::Texture2D &&texture, Size<float> size, Position<float> bearing, float advance) noexcept
                  : m_texture(std::move(texture))
                  , m_size(size)
                  , m_bearing(bearing)
                  , m_advance(advance) {
        }

        [[nodiscard]] inline constexpr float
        advance() const noexcept {
            return m_advance;
        }

        [[nodiscard]] inline constexpr Position<float>
        bearing() const noexcept {
            return m_bearing;
        }

        [[nodiscard]] inline constexpr Size<float>
        size() const noexcept {
            return m_size;
        }

        [[nodiscard]] inline constexpr gle::Texture2D &
        texture() noexcept {
            return m_texture;
        }

        [[nodiscard]] inline constexpr const gle::Texture2D &
        texture() const noexcept {
            return m_texture;
        }

    private:
        gle::Texture2D m_texture;
        Size<float> m_size;
        Position<float> m_bearing;
        float m_advance;
    };

} // namespace gfx
