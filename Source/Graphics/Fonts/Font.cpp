/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/Graphics/Fonts/Font.hpp"

#include <GL/glew.h>

#include "Source/Base/Assert.hpp"

namespace gfx {

    [[nodiscard]] static Character *
    generateCharacter(GeneratedCharacterMap &characterMap, FT_Face &face, char32_t codePoint) noexcept {
        if (FT_Load_Char(face, codePoint, FT_LOAD_RENDER)) {
            LOG_ERROR("Failed to load character from FreeType");
            return nullptr;
        }

        if (FT_Set_Pixel_Sizes(face, 0, static_cast<FT_UInt>(characterMap.pixelSize())) != 0)
            LEMON_ASSERT_NOT_REACHED_OR_RETURN(nullptr);

        const Size uSize{static_cast<std::uint32_t>(face->glyph->bitmap.width),
                         static_cast<std::uint32_t>(face->glyph->bitmap.rows)};
        const Size fSize{static_cast<float>(face->glyph->bitmap.width),
                         static_cast<float>(face->glyph->bitmap.rows)};

        gle::Texture2D texture;
        if (!texture.generate(gle::Texture2DIntegerType::UNSIGNED_CHAR, gle::Texture2DFormat::GRAYSCALE, uSize,
                              face->glyph->bitmap.buffer)) {
            LOG_ERROR("Failed to generate texture for character");
            return nullptr;
        }

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        auto characterIt = characterMap.characterMap().emplace(codePoint, Character{
                std::move(texture), fSize,
                Position{static_cast<float>(face->glyph->bitmap_left),
                         static_cast<float>(face->glyph->bitmap_top) - static_cast<float>(face->glyph->bitmap.rows)},
                static_cast<float>(face->glyph->advance.x >> 6)
        });

        return &characterIt.first->second;
    }

    [[nodiscard]] inline std::string_view
    translateFreetypeError(FT_Error error) noexcept {
        const char *string = FT_Error_String(error);
        if (string)
            return string;

        if (error == 0x01)
            return "Failed to open file.";
        return "Unknown error occurred";
    }

    bool
    Font::loadFromFile(std::string_view path) noexcept {
        if (FT_Init_FreeType(&m_ftLibrary)) {
            LOG_TRACE("Failed to initialize FreeType!");
            return false;
        }

        if (auto error = FT_New_Face(ftLibrary(), std::string(path).c_str(), 0, &m_ftFace)) {
            LOG_TRACE("Failed to load font from FreeType: {}", translateFreetypeError(error));
            return false;
        }

        return true;
    }

    const Character *
    Font::characterOfSize(std::uint32_t size, char32_t character) noexcept {
        auto characterMapIt = m_maps.find(size);
        const bool isNew = characterMapIt == std::cend(m_maps);
//        GeneratedCharacterMap &map = isNew
//                                   ? m_maps.emplace(size, GeneratedCharacterMap{this}).first->second
//                                   : characterMapIt->second;
        GeneratedCharacterMap *ptrMap;
        if (isNew) {
            GeneratedCharacterMap map{this};

            auto result = m_maps.emplace(size, std::move(map));
            LEMON_ASSERT(result.second);
            ptrMap = &result.first->second;
        } else
            ptrMap = &characterMapIt->second;

        GeneratedCharacterMap &map = *ptrMap;
        if (!isNew) {
            auto characterIt = characterMapIt->second.characterMap().find(character);
            if (characterIt != std::cend(characterMapIt->second.characterMap())) {
                return &characterIt->second;
            }
        } else {
            map.setPixelSize(static_cast<float>(size));
            FT_Set_Pixel_Sizes(m_ftFace, 0, static_cast<FT_UInt>(size));
            // Fail paint: can we get face->size after this call?
            map.setVerticalAdvance(static_cast<float>(m_ftFace->size->metrics.height >> 6));
            map.setDescender(static_cast<float>(m_ftFace->size->metrics.descender >> 6));
        }

        return generateCharacter(map, m_ftFace, character);
    }

    Font::~Font() noexcept {
        FT_Done_Face(m_ftFace);
        FT_Done_FreeType(m_ftLibrary);
    }

} // namespace gfx
