/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

namespace gfx {

    template <typename Type>
    struct Position {
        [[nodiscard]] inline constexpr
        Position(Type x, Type y) noexcept
                : m_x(x)
                , m_y(y) {
        }

        [[nodiscard]] inline constexpr Type &
        x() noexcept {
            return m_x;
        }

        [[nodiscard]] inline constexpr Type
        x() const noexcept {
            return m_x;
        }

        [[nodiscard]] inline constexpr Type &
        y() noexcept {
            return m_y;
        }

        [[nodiscard]] inline constexpr Type
        y() const noexcept {
            return m_y;
        }

    private:
        Type m_x;
        Type m_y;
    };

} // namespace gfx
