/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

namespace gfx {

    template <typename Type>
    struct Size {
        [[nodiscard]] inline constexpr
        Size(Type width, Type height) noexcept
                : m_width(width)
                , m_height(height) {
        }

        [[nodiscard]] inline constexpr Type &
        height() noexcept {
            return m_height;
        }

        [[nodiscard]] inline constexpr Type
        height() const noexcept {
            return m_height;
        }

        [[nodiscard]] inline constexpr Type &
        width() noexcept {
            return m_width;
        }

        [[nodiscard]] inline constexpr Type
        width() const noexcept {
            return m_width;
        }

    private:
        Type m_width;
        Type m_height;
    };

} // namespace gfx
