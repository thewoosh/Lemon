/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

namespace gfx {

    /**
     * REMEMBER: The colors don't go from 0 to 256, but 0.0 to 1.0.
     */
    struct Color {
        [[nodiscard]] constexpr
        Color() noexcept = default;

        [[nodiscard]] inline constexpr
        Color(float r, float g, float b, float a) noexcept
                : m_r(r)
                , m_g(g)
                , m_b(b)
                , m_a(a) {
        }

        [[nodiscard]] inline static constexpr Color
        fromRGB(std::uint8_t r, std::uint8_t g, std::uint8_t b) noexcept {
            return Color{static_cast<float>(r) / 256.0f, static_cast<float>(g) / 256.0f,
                         static_cast<float>(b) / 256.0f, 1.0f};
        }

        [[nodiscard]] inline constexpr float
        r() const noexcept {
            return m_r;
        }

        [[nodiscard]] inline constexpr float
        g() const noexcept {
            return m_g;
        }

        [[nodiscard]] inline constexpr float
        b() const noexcept {
            return m_b;
        }

        [[nodiscard]] inline constexpr float
        a() const noexcept {
            return m_a;
        }

    private:
        float m_r{};
        float m_g{};
        float m_b{};
        float m_a{};
    };

} // namespace gfx
