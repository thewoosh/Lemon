/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include "Source/Graphics/Types/Color.hpp"

namespace gfx {

    class RenderTarget {
    public:
        virtual
        ~RenderTarget() noexcept = default;

        virtual void
        beginFrame([[maybe_unused]] Color initialBackgroundColor={1.0f, 1.0f, 1.0f, 1.0f}) noexcept {
        }

        virtual void
        endFrame() noexcept {
        }

    protected:
        RenderTarget() noexcept = default;
    };

} // namespace gfx
