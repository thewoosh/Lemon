/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "GLPainter.hpp"

#include <array>
#include <optional>
#include <string_view>

#ifdef __GNUC__
#   pragma GCC diagnostic push
#   pragma GCC diagnostic ignored "-Wold-style-cast"
#endif
#include <freetype2/ft2build.h>
#include FT_FREETYPE_H
#include FT_ADVANCES_H
#include FT_ERRORS_H
#ifdef __GNUC__
#   pragma GCC diagnostic pop
#endif

#include <GL/glew.h>

#include "Include/text/UnicodeBlocks/C0ControlsAndBasicLatin.hpp"
#include "Include/text/UnicodeBlocks/Specials.hpp"

#include "Source/Base/Assert.hpp"
#include "Source/Base/Logger.hpp"
#include "Source/GUI/Elements/Text.hpp"
#include "Source/Graphics/Fonts/Font.hpp"
#include "Source/Text/Encoding/UTF8Iterator.hpp"

namespace gfx {

    Size<float>
    GLPainter::calculateTextSize(std::string_view contents, FontSize fontSize) noexcept {
        return calculateTextSize(m_defaultFont, contents, fontSize);
    }

    gfx::Size<float>
    GLPainter::calculateTextSize(Font *font, std::string_view contents, FontSize fontSize) noexcept {
        float width{0};

        bool encodeStatus = text::encoding::forEachUTF8(contents, [&] (char32_t codePoint) {
            width += static_cast<float>(font->characterOfSize(fontSize, codePoint)->advance());
        });

        LEMON_ASSERT(encodeStatus);
        static_cast<void>(encodeStatus);

        const auto &generatedMap = font->maps().find(fontSize)->second;
        return {width, generatedMap.verticalAdvance()};
    }

    bool
    GLPainter::createTextShader() noexcept {
        m_textShader = gle::ShaderProgram{
            gle::Shader{gle::Shader::ConstructionMode::GLSL_SOURCE, gle::ShaderType::VERTEX,R"(
                #version 330 core

                layout (location = 0) in vec4 vertex; // <vec2 pos, vec2 tex>

                out vec2 textureCoordinates;

                void main() {
                    gl_Position = vec4(vertex.xy, 0.0, 1.0);
                    textureCoordinates = vertex.zw;
                }
            )"},
            gle::Shader{gle::Shader::ConstructionMode::GLSL_SOURCE, gle::ShaderType::FRAGMENT, R"(
                #version 330 core

                in vec2 textureCoordinates;

                out vec4 output_color;

                uniform sampler2D characterTexture;
                uniform vec4 textColor;

                void main() {
                    output_color = vec4(textColor.rgb, texture(characterTexture, textureCoordinates).r);
                }
            )"},
        };

        if (!m_textShader.isValid())
            return false;

        glUseProgram(m_textShader.programID());

        auto samplerUniform = glGetUniformLocation(m_textShader.programID(), "characterTexture");
        if (samplerUniform == -1) {
            LOG_ERROR("Couldn't find uniform location of \"characterTexture\"");
            return false;
        }
        glUniform1i(samplerUniform, 0);

        m_textColorUniformLocation = glGetUniformLocation(m_textShader.programID(), "textColor");
        if (m_textColorUniformLocation == -1) {
            LOG_ERROR("Couldn't find uniform location of \"textColor\"");
            return false;
        }

        // Disable byte alignment restrictions
        glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

        return true;
    }

    Font *
    GLPainter::loadFontFromDisk(std::string_view path) noexcept {
        auto &font = m_fonts.emplace_back();

        if (!font.loadFromFile(path)) {
            m_fonts.pop_back();
            return nullptr;
        }

        return &font;
    }

    void
    GLPainter::paintText(std::string_view contents, Position<float> position, Color color, FontSize fontSize) noexcept {
        if (color.a() == 0.0f || std::empty(contents))
            return;

        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

        glUseProgram(m_textShader.programID());
        glUniform4f(m_textColorUniformLocation, color.r(), color.g(), color.b(), color.a());
        glActiveTexture(GL_TEXTURE0);

        const auto &generatedMap = m_currentFont->maps().find(fontSize)->second;

        bool encodeStatus = text::encoding::forEachUTF8(contents, [&] (char32_t codePoint) {
            const auto *character = m_currentFont->characterOfSize(fontSize, codePoint);
            LEMON_ASSERT(character);

            const float width  = character->size().width();
            const float height = character->size().height();

            const float x      = position.x() + (character->bearing().x());
            const float y      = static_cast<float>(m_renderSize.height()) - (position.y()
                    - (character->bearing().y())
                    + (generatedMap.verticalAdvance() + generatedMap.descender()));

            glBindTexture(GL_TEXTURE_2D, character->texture().textureID());

            m_renderQuad.drawWithTextureCoordinates(gfx::Position{x, y}, {width, height});

            position.x() += static_cast<float>(character->advance());
        });

        glDisable(GL_BLEND);
//        glDisable(GL_CULL_FACE);

        LEMON_ASSERT(encodeStatus);
        static_cast<void>(encodeStatus);
    }

    void
    GLPainter::paintUnderlineDecorationForText(std::string_view text, Position<float> position, Color color,
                                               FontSize fontSize, float width) noexcept {
        const auto size = calculateTextSize(text, fontSize);
        const auto *character = m_currentFont->characterOfSize(fontSize, 'a');
        const auto &generatedMap = m_currentFont->maps().find(fontSize)->second;

        const auto y = position.y()
                     + (generatedMap.verticalAdvance() + generatedMap.descender() - character->bearing().y()) * 1.1f;

        paintRect({position.x(), y}, {size.width(), width}, color);
    }

} // namespace gfx
