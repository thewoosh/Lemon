/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "GLPainter.hpp"

#include "Source/Base/Assert.hpp"
#include "Source/Base/Logger.hpp"
#include "Source/Graphics/OpenGL/Resources/RenderTarget.hpp"
#include "Source/GUI/Elements/Text.hpp"

#include <GL/glew.h>

namespace gfx {

    void
    GLPainter::beginFrame() noexcept {
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    }

    RenderTarget *
    GLPainter::createRenderTarget(gfx::Size<std::uint32_t> size) noexcept {
        auto &renderTarget = m_renderTargets.emplace_back();

        if (!renderTarget.initialize(size)) {
            m_renderTargets.pop_back();
            return nullptr;
        }

        return &renderTarget;
    }

    bool
    GLPainter::createRectangleShader() noexcept {
        m_rectangleShader = gle::ShaderProgram{
            gle::Shader{gle::Shader::ConstructionMode::GLSL_SOURCE, gle::ShaderType::VERTEX, R"(
            #version 330 core

            layout (location = 0) in vec2 vertex_position;

            void main() {
                gl_Position = vec4(vertex_position, 0.0, 1.0);
            }
            )"},
            gle::Shader{gle::Shader::ConstructionMode::GLSL_SOURCE, gle::ShaderType::FRAGMENT, R"(
            #version 330 core

            out vec4 output_color;

            uniform vec4 uniform_color;

            void main() {
                output_color = uniform_color;
            }
            )"},
        };

        if (!m_rectangleShader.isValid())
            return false;

        glUseProgram(m_rectangleShader.programID());

        m_rectangleShaderColorUniform = gle::UniformVector4{
            glGetUniformLocation(m_rectangleShader.programID(), "uniform_color")
        };

        return true;
    }

    bool
    GLPainter::createTextureShader() noexcept {
        m_textureShader = gle::ShaderProgram{
            gle::Shader{gle::Shader::ConstructionMode::GLSL_SOURCE, gle::ShaderType::VERTEX, R"(
                #version 330 core

                layout (location = 0) in vec2 vertex;

                out vec2 textureCoordinates;

                void main() {
                    gl_Position = vec4(vertex.xy, 0.0, 1.0);
                    textureCoordinates = (vertex.xy + vec2(1, 1)) / 2.0;
                }
            )"},
            gle::Shader{gle::Shader::ConstructionMode::GLSL_SOURCE, gle::ShaderType::FRAGMENT, R"(
                #version 330 core

                in vec2 textureCoordinates;

                out vec4 output_color;

                uniform sampler2D uniform_texture;

                void main() {
                    output_color = texture(uniform_texture, textureCoordinates);
                }
            )"},
        };

        if (!m_textureShader.isValid())
            return false;

        glUseProgram(m_textureShader.programID());

        auto samplerUniform = glGetUniformLocation(m_textureShader.programID(), "uniform_texture");
        if (samplerUniform == -1) {
            LOG_ERROR("Couldn't find uniform location of \"uniform_texture\"");
            return false;
        }

        glUniform1i(samplerUniform, 0);
        return true;
    }

    bool
    GLPainter::initialize(Size<std::uint32_t> renderSize) noexcept {
        if (glewInit() != GLEW_OK) {
            LOG_ERROR("Failed to initialize GLEW");
            return false;
        }

        glClearColor(1.0f, 1.0f, 1.0f, 1.0f);

        if (!createRectangleShader())
            return false;

        if (!m_renderQuad.generate())
            return false;

        if (!createTextShader())
            return false;

        if (!createTextureShader())
            return false;

        onFramebufferResize(renderSize);

        return true;
    }

    void
    GLPainter::onFramebufferResize(Size<std::uint32_t> renderSize) noexcept {
        m_renderSize = renderSize;
        glViewport(0, 0, static_cast<GLsizei>(renderSize.width()), static_cast<GLsizei>(renderSize.height()));
        m_renderQuad.setFramebufferSize(renderSize);
    }

    void
    GLPainter::paintBorderAroundRect(Position<float> position, Size<float> size, Color color, float thickness) noexcept {
        // top
        paintRect({position.x() - thickness, position.y() - thickness}, {size.width() + thickness, thickness}, color);
        // bottom
        paintRect({position.x() - thickness, position.y() + size.height()}, {size.width() + thickness, thickness}, color);
        // left
        paintRect({position.x() - thickness, position.y() - thickness}, {thickness, size.height() + thickness}, color);
        // right
        paintRect({position.x() + size.width(), position.y() - thickness}, {thickness, size.height() + thickness + thickness}, color);
    }

    void
    GLPainter::paintRect(Position<float> position, Size<float> size, Color color) noexcept {
        glUseProgram(m_rectangleShader.programID());
        m_rectangleShaderColorUniform.store({color.r(), color.g(), color.b(), color.a()});
        m_renderQuad.draw(position, size);
    }

    void
    GLPainter::paintRenderTarget(RenderTarget *inRenderTarget, Position<float> position, Size<float> size) noexcept {
        auto *const renderTarget = static_cast<gle::GLRenderTarget *>(inRenderTarget);

        if (size.width() == 0.0f)
            size.width() = static_cast<float>(renderTarget->size().width());
        if (size.height() == 0.0f)
            size.height() = static_cast<float>(renderTarget->size().height());

        glUseProgram(m_textureShader.programID());
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, renderTarget->textureID());
        m_renderQuad.draw(position, size);
    }

    void
    GLPainter::paintTexturedRect(Position<float> position, Size<float> size) noexcept {
        static_cast<void>(position);
        static_cast<void>(size);
        LEMON_ASSERT_NOT_REACHED_OR_RETURN_VOID();
    }

    void
    GLPainter::setCurrentFont(Font *font) noexcept {
        LEMON_ASSERT(font);
        m_currentFont = font;
    }

    void
    GLPainter::setDefaultFont(Font *font) noexcept {
        LEMON_ASSERT(font);
        m_defaultFont = font;

        if (m_currentFont == nullptr)
            m_currentFont = font;
    }

    void
    GLPainter::setDefaultFontAsCurrentFont() noexcept {
        m_currentFont = m_defaultFont;
    }

} // namespace gfx
