/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include "Source/Graphics/Fonts/Font.hpp"
#include "Source/Graphics/Painter.hpp"
#include "Source/Graphics/OpenGL/Resources/RenderQuad.hpp"
#include "Source/Graphics/OpenGL/Resources/RenderTarget.hpp"
#include "Source/Graphics/OpenGL/Shaders/ShaderProgram.hpp"
#include "Source/Graphics/OpenGL/Shaders/Uniform.hpp"

namespace gfx::gle {
    class GLRenderTarget;
} // namespace gfx::gle

namespace gfx {

    struct Text;

    class GLPainter final
            : public Painter {
    public:
        [[nodiscard]] bool
        initialize(Size<std::uint32_t> renderSize) noexcept override;

        void
        beginFrame() noexcept override;

        [[nodiscard]] RenderTarget *
        createRenderTarget(gfx::Size<std::uint32_t> size) noexcept override;

        [[nodiscard]] Size<float>
        calculateTextSize(std::string_view, FontSize fontSize) noexcept override;

        [[nodiscard]] Size<float>
        calculateTextSize(Font *, std::string_view, FontSize fontSize) noexcept override;

        [[nodiscard]] Font *
        loadFontFromDisk(std::string_view path) noexcept override;

        void
        setCurrentFont(Font *) noexcept override;

        void
        setDefaultFont(Font *) noexcept override;

        void
        setDefaultFontAsCurrentFont() noexcept override;

        void
        onFramebufferResize(Size<std::uint32_t>) noexcept override;

        void
        paintBorderAroundRect(Position<float>, Size<float>, Color, float width) noexcept override;

        void
        paintRect(Position<float>, Size<float>, Color) noexcept override;

        void
        paintRenderTarget(RenderTarget *renderTarget, Position<float>, Size<float> size) noexcept override;

        void
        paintText(std::string_view, Position<float>, Color, FontSize) noexcept override;

        void
        paintTexturedRect(Position<float>, Size<float>) noexcept override;

        void
        paintUnderlineDecorationForText(std::string_view, Position<float>, Color, FontSize, float) noexcept override;

    private:
        [[nodiscard]] bool
        createRectangleShader() noexcept;

        [[nodiscard]] bool
        createTextShader() noexcept;

        [[nodiscard]] bool
        createTextureShader() noexcept;

        gle::ShaderProgram m_textureShader{};
        gle::ShaderProgram m_rectangleShader{};
        gle::UniformVector4 m_rectangleShaderColorUniform{-1};
        gle::RenderQuad m_renderQuad{};

        std::vector<Font> m_fonts{};
        Font *m_defaultFont{nullptr};
        Font *m_currentFont{nullptr};

        int m_textColorUniformLocation{};
        gle::ShaderProgram m_textShader{};
        Size<std::uint32_t> m_renderSize{0, 0};

        std::vector<gle::GLRenderTarget> m_renderTargets{};
    };

} // namespace gfx
