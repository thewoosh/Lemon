/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 *
 * From <https://gitlab.com/thewoosh/Lavender>. I've changed the namespace so
 * it's in the gui namespace, and added a 0-argument constructor.
 */

#pragma once

#include "Shader.hpp"

namespace gfx::gle {

    class ShaderProgram {
        unsigned int m_programID{};

        Shader m_vertexShader{};
        Shader m_fragmentShader{};

    public:
        [[nodiscard]] inline constexpr
        ShaderProgram() noexcept = default;

        [[nodiscard]]
        ShaderProgram(ShaderProgram &&shaderProgram) noexcept
                : m_programID(shaderProgram.programID())
                , m_vertexShader(std::move(shaderProgram.m_vertexShader))
                , m_fragmentShader(std::move(shaderProgram.m_fragmentShader)) {
            shaderProgram.m_programID = 0;
        }

        ShaderProgram &
        operator=(ShaderProgram &&shaderProgram) noexcept {
            std::swap(this->m_programID, shaderProgram.m_programID);
            std::swap(this->m_fragmentShader, shaderProgram.m_fragmentShader);
            std::swap(this->m_vertexShader, shaderProgram.m_vertexShader);
            return *this;
        }

        [[nodiscard]]
        ShaderProgram(Shader &&inVertexShader, Shader &&inFragmentShader) noexcept;

        ~ShaderProgram() noexcept;

        [[nodiscard]] constexpr inline bool
        isValid() const noexcept {
            return m_programID != 0 && m_vertexShader.isValid() && m_fragmentShader.isValid();
        }

        [[nodiscard]] constexpr inline unsigned int
        programID() const noexcept {
            return m_programID;
        }
    };

} // namespace gfx::gle
