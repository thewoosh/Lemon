/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Uniform.hpp"

#include <GL/glew.h>

namespace gfx::gle {

    void
    UniformMatrix4::store(const math::Matrix4x4<float> &matrix) noexcept {
        glUniformMatrix4fv(location(), 1, GL_TRUE, &matrix.m_data[0][0]);
    }

    void
    UniformVector2::store(const math::Vector<float, 2> &vector) noexcept {
        glUniform2f(location(), vector.x(), vector.y());
    }

    void
    UniformVector3::store(const math::Vector<float, 3> &vector) noexcept {
        glUniform3f(location(), vector.x(), vector.y(), vector.z());
    }

    void
    UniformVector4::store(const math::Vector<float, 4> &vector) noexcept {
        glUniform4f(location(), vector.x(), vector.y(), vector.z(), vector.w());
    }

} // namespace gfx::gle
