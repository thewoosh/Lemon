/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 *
 * From <https://gitlab.com/thewoosh/Lavender>. I've changed:
 * (1) the namespace so it's in the gui namespace.
 * (2) the printing and formatting to use the logger.
 * (3) removed printUniforms()
 */

#include "ShaderProgram.hpp"

#include <array>

#include <cassert>

#include <GL/glew.h>

#include "Shader.hpp"

namespace gfx::gle {

    ShaderProgram::ShaderProgram(Shader &&inVertexShader, Shader &&inFragmentShader) noexcept
            : m_programID(glCreateProgram())
            , m_vertexShader(std::move(inVertexShader))
            , m_fragmentShader(std::move(inFragmentShader)) {
        if (m_programID == 0)
            return;

        m_vertexShader.attach(m_programID);
        m_fragmentShader.attach(m_programID);
        glLinkProgram(m_programID);

        GLint success{};
        glGetProgramiv(m_programID, GL_LINK_STATUS, &success);

        if (success == GL_FALSE) {
            std::array<char, 512> buffer{};
            glGetProgramInfoLog(m_programID, static_cast<GLsizei>(std::size(buffer)), nullptr, std::data(buffer));

            LOG_ERROR("ShaderProgram failed to link: \"{}\"", std::data(buffer));
            return;
        }

        glUseProgram(m_programID);
    }

    ShaderProgram::~ShaderProgram() noexcept {
        m_vertexShader.detach();
        m_fragmentShader.detach();

        if (m_programID != 0) {
            glDeleteProgram(m_programID);
        }
    }

} // namespace gfx::gle
