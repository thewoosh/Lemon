/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 *
 * From <https://gitlab.com/thewoosh/Lavender>. I've changed:
 * (1) the namespace so it's in the gui namespace.
 * (2) the loadGLSLPath() function, so it uses the io::FileReader.
 * (3) the printing and formatting to use the logger / {fmt}.
 * (4) removed exceptions
 */

#include "Shader.hpp"

#include <array>
#include <type_traits>

#include <GL/glew.h>

#include "Source/Base/Assert.hpp"
#include "Source/Base/Logger.hpp"
#include "Source/IO/FileReader.hpp"

namespace gfx::gle {

    [[nodiscard]] constexpr inline GLenum
    ConvertShaderTypeToGLEnum(ShaderType type) noexcept {
        switch (type) {
            case ShaderType::VERTEX:
                return GL_VERTEX_SHADER;
            case ShaderType::FRAGMENT:
                return GL_FRAGMENT_SHADER;
            default:
                LEMON_ASSERT_NOT_REACHED_OR_RETURN(GL_NONE);
        }
    }

    Shader::Shader(ConstructionMode constructionMode,
                   ShaderType shaderType,
                   std::string_view sv) {
        static_assert(std::is_same_v<GLuint, decltype(Shader::m_shaderID)>);
        static_assert(std::is_same_v<GLchar, char>);

        if (!createShader(shaderType))
            return;

        if (!loadShaderData(constructionMode, sv)
                || !compileShader()) {
            glDeleteShader(m_shaderID);
            m_shaderID = 0;
        }
    }

    Shader::~Shader() noexcept {
        if (m_shaderID != 0) {
            if (m_programID != 0) {
                glDetachShader(m_programID, m_shaderID);
            }

            glDeleteShader(m_shaderID);
        }
    }

    void
    Shader::attach(std::uint32_t program) noexcept {
        m_programID = program;
        glAttachShader(program, m_shaderID);
    }

    bool
    Shader::createShader(ShaderType shaderType) noexcept {
        m_shaderID = glCreateShader(ConvertShaderTypeToGLEnum(shaderType));
        return m_shaderID != 0;
    }

    void
    Shader::detach() noexcept {
        if (m_programID != 0) {
            glDetachShader(m_programID, m_shaderID);
            m_programID = 0;
        }
    }

    bool
    Shader::compileShader() const noexcept {
        glCompileShader(m_shaderID);

        GLint success{};
        glGetShaderiv(m_shaderID, GL_COMPILE_STATUS, &success);

        if (success == GL_FALSE) {
            std::array<char, 512> buffer{};
            glGetShaderInfoLog(m_shaderID, static_cast<GLsizei>(std::size(buffer)), nullptr, std::data(buffer));
            LOG_ERROR("Shader compile error!");
            LOG_ERROR("OpenGL Shader info log: \"{}\"", std::data(buffer));

            return false;
        }

        return true;
    }

    bool
    Shader::loadShaderData(ConstructionMode constructionMode, std::string_view sv) noexcept {
        switch (constructionMode) {
            case ConstructionMode::GLSL_PATH:
                return loadGLSLPath(sv);
            case ConstructionMode::GLSL_SOURCE:
                return loadGLSLSource(sv);
            default:
                LEMON_ASSERT_NOT_REACHED_OR_RETURN(false);
        }
    }

    bool
    Shader::loadGLSLPath(std::string_view inPath) const noexcept {
        const auto path = fmt::format("Resources/Shaders/{}", inPath);
        io::FileReader reader{path};

        if (!reader) {
            LOG_ERROR("Failed to open shader file \"{}\"!", path);
            return false;
        }

        const auto result = reader.readAll();
        if (!result.couldRead()) {
            LOG_ERROR("Failed to read from shader file \"{}\"!", path);
            return false;
        }

        const auto *data = std::data(result.contents());
        const auto size = static_cast<GLint>(std::size(result.contents()));
        glShaderSource(m_shaderID, 1, &data, &size);

        return glGetError() == GL_NO_ERROR;
    }

    bool
    Shader::loadGLSLSource(std::string_view source) const noexcept {
        const char *data = std::data(source);
        glShaderSource(m_shaderID, 1, std::addressof(data), nullptr);
        return true;
    }

} // namespace gfx::gle