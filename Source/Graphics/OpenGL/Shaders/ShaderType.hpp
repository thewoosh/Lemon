/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 *
 * From <https://gitlab.com/thewoosh/Lavender>. I've changed the namespace so
 * it's in the gui namespace.
 */

#pragma once

namespace gfx::gle {

    enum class ShaderType {
        VERTEX,
        FRAGMENT
    };

} // namespace gfx::gle
