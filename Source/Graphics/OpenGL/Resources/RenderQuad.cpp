/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "RenderQuad.hpp"

#include <GL/glew.h>

#include "Source/Base/Assert.hpp"

namespace gfx::gle {

    constexpr const std::array<GLfloat, 24> g_emptyVertexData{
        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0,
        0, 0, 0, 0,
    };

    RenderQuad::~RenderQuad() noexcept {
        if (m_vbo != 0) {
            glDeleteBuffers(1, &m_vbo);
        }

        if (m_vao != 0) {
            glDeleteVertexArrays(1, &m_vao);
        }
    }

    void
    RenderQuad::draw(Position<float> position, Size<float> size) const noexcept {
        LEMON_ASSERT(m_vao != 0);
        LEMON_ASSERT(m_vbo != 0);

        glBindVertexArray(m_vao);
        glBindBuffer(GL_ARRAY_BUFFER, m_vbo);

        const auto fw = static_cast<float>(m_framebufferSize.width()),
                   fh = static_cast<float>(m_framebufferSize.height());
        const float minX = position.x() / fw * 2 - 1,
                    minY = (fh - position.y()) / fh * 2 - 1,
                    maxX = (position.x() + size.width()) / fw * 2 - 1,
                    maxY = (fh - position.y() - size.height()) / fh * 2 - 1;

        const std::array<GLfloat, 12> vertexData{
            minX, minY,
            maxX, minY,
            maxX, maxY,

            minX, minY,
            maxX, maxY,
            minX, maxY
        };

        glBufferSubData(GL_ARRAY_BUFFER, 0, std::size(vertexData) * sizeof(GLfloat), std::data(vertexData));

        glDrawArrays(GL_TRIANGLE_STRIP, 0, static_cast<GLsizei>(std::size(vertexData) / 2));
    }

    void
    RenderQuad::drawWithTextureCoordinates(Position<float> position, Size<float> size) const noexcept {
        LEMON_ASSERT(m_vao != 0);
        LEMON_ASSERT(m_vbo != 0);

        glBindVertexArray(m_vao);
        glBindBuffer(GL_ARRAY_BUFFER, m_vbo);

        const auto fw = static_cast<float>(m_framebufferSize.width()),
        fh = static_cast<float>(m_framebufferSize.height());
        const float minX = position.x() / fw * 2 - 1,
                    minY = position.y() / fh * 2 - 1,
                    maxX = (position.x() + size.width()) / fw * 2 - 1,
                    maxY = (position.y() + size.height()) / fh * 2 - 1;

        const std::array<GLfloat, 24> vertexData{
            minX, maxY, 0.0f, 0.0f,
            minX, minY, 0.0f, 1.0f,
            maxX, minY, 1.0f, 1.0f,

            minX, maxY, 0.0f, 0.0f,
            maxX, minY, 1.0f, 1.0f,
            maxX, maxY, 1.0f, 0.0f
        };

        glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 16, nullptr);

        glBufferSubData(GL_ARRAY_BUFFER, 0, std::size(vertexData) * sizeof(GLfloat), std::data(vertexData));

        glDrawArrays(GL_TRIANGLES, 0, static_cast<GLsizei>(std::size(vertexData) / 4));

        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);
    }

    bool
    RenderQuad::generate() noexcept {
        glGenVertexArrays(1, &m_vao);
        glBindVertexArray(m_vao);

        glGenBuffers(1, &m_vbo);
        glBindBuffer(GL_ARRAY_BUFFER, m_vbo);

        if (m_vao == 0 || m_vbo == 0) {
            return false;
        }

#if defined(GL_ARB_buffer_storage) && GL_ARB_buffer_storage
        m_hasBufferStorage = glBufferStorage != nullptr;
#else
        m_hasBufferStorage = false;
#endif

        if (m_hasBufferStorage) {
            glBufferStorage(GL_ARRAY_BUFFER, std::size(g_emptyVertexData) * sizeof(GLfloat), std::data(g_emptyVertexData),
                            GL_DYNAMIC_STORAGE_BIT | GL_MAP_WRITE_BIT);
        } else {
            glBufferData(GL_ARRAY_BUFFER, std::size(g_emptyVertexData) * sizeof(GLfloat), std::data(g_emptyVertexData),
                         GL_DYNAMIC_DRAW);
        }

        glEnableVertexAttribArray(0);
        glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, nullptr);

        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);

        return glGetError() == GL_NO_ERROR;
    }

} // namespace gfx::gle
