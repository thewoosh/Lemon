/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <cstdint>

#include "Source/Graphics/Types/Size.hpp"
#include "Source/Graphics/RenderTarget.hpp"

namespace gfx::gle {

    class GLRenderTarget final
            : public RenderTarget {
    public:
        ~GLRenderTarget() noexcept;

        [[nodiscard]] bool
        initialize(gfx::Size<std::uint32_t> size) noexcept;

        void
        beginFrame(Color initialBackgroundColor) noexcept override;

        void
        endFrame() noexcept override;

        [[nodiscard]] inline constexpr gfx::Size<std::uint32_t>
        size() const noexcept {
            return m_size;
        }

        [[nodiscard]] inline constexpr unsigned int
        textureID() const noexcept {
            return m_textureID;
        }

    private:
        gfx::Size<std::uint32_t> m_size{0, 0};
        unsigned int m_framebufferID{};
        unsigned int m_textureID{};
    };

} // namespace gfx::gle
