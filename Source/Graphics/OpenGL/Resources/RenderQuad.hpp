/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include "Source/Graphics/Types/Position.hpp"
#include "Source/Graphics/Types/Size.hpp"

namespace gfx::gle {

    class RenderQuad {
    public:
        ~RenderQuad() noexcept;

        void
        draw(Position<float>, Size<float>) const noexcept;

        void
        drawWithTextureCoordinates(Position<float>, Size<float>) const noexcept;

        [[nodiscard]] bool
        generate() noexcept;

        inline constexpr void
        setFramebufferSize(Size<std::uint32_t> size) noexcept {
            m_framebufferSize = size;
        }

    private:
        Size<std::uint32_t> m_framebufferSize{0, 0};
        bool m_hasBufferStorage{};
        unsigned int m_vao{};
        unsigned int m_vbo{};
    };

} // namespace gfx::gle
