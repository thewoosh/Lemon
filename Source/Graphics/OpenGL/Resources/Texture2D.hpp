/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <cstdint>

#include "Source/Graphics/Types/Size.hpp"

namespace gfx::gle {

    enum class Texture2DIntegerType {
        UNSIGNED_CHAR,
        UNSIGNED_INT,
    };

    enum class Texture2DFormat {
        GRAYSCALE,
    };

    class Texture2D {
    public:
        [[nodiscard]] inline constexpr Texture2D() noexcept = default;
        Texture2D(const Texture2D &) = delete;

        inline constexpr
        Texture2D(Texture2D &&texture2D) noexcept
                : m_textureID(texture2D.m_textureID) {
            texture2D.m_textureID = 0;
        }

        ~Texture2D() noexcept;

        void
        bind() const noexcept;

        [[nodiscard]] bool
        generate(Texture2DIntegerType, Texture2DFormat, Size<std::uint32_t>, const void *data) noexcept;

        [[nodiscard]] inline constexpr unsigned int
        textureID() const noexcept {
            return m_textureID;
        }

    private:
        unsigned int m_textureID{};
    };

} // namespace gfx::gle
