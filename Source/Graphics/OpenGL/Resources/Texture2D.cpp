/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Texture2D.hpp"

#include <GL/glew.h>

#include "Source/Base/Assert.hpp"

namespace gfx::gle {

    [[nodiscard]] inline constexpr GLenum
    convertIntegerType(Texture2DIntegerType texture2DIntegerType) noexcept {
        switch (texture2DIntegerType) {
            case Texture2DIntegerType::UNSIGNED_CHAR:
                return GL_UNSIGNED_BYTE;
                case Texture2DIntegerType::UNSIGNED_INT:
                return GL_UNSIGNED_INT;
        }

        LEMON_ASSERT_NOT_REACHED_OR_RETURN(GL_NONE);
    }

    [[nodiscard]] inline constexpr GLenum
    convertFormat(Texture2DFormat texture2DFormat) noexcept {
        switch (texture2DFormat) {
            case Texture2DFormat::GRAYSCALE:
                return GL_RED;
        }

        LEMON_ASSERT_NOT_REACHED_OR_RETURN(GL_NONE);
    }

    Texture2D::~Texture2D() noexcept {
        if (m_textureID != 0)
            glDeleteTextures(1, &m_textureID);
    }

    void
    Texture2D::bind() const noexcept {
        glBindTexture(GL_TEXTURE_2D, m_textureID);
    }

    bool
    Texture2D::generate(Texture2DIntegerType integerType, Texture2DFormat format, Size<std::uint32_t> size,
                        const void *data) noexcept {
        glGenTextures(1, &m_textureID);
        bind();

        const auto textureType = convertFormat(format);
        const GLuint lod = 0;
        const GLuint border = 0;

        glTexImage2D(GL_TEXTURE_2D, lod, static_cast<int>(textureType), static_cast<GLsizei>(size.width()),
                     static_cast<GLsizei>(size.height()), border, textureType, convertIntegerType(integerType), data);

        return glGetError() == GL_NO_ERROR;
    }

} // namespace gfx::gle
