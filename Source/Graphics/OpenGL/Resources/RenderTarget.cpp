/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/Graphics/OpenGL/Resources/RenderTarget.hpp"

#include <array>

#include <GL/glew.h>

#include "Source/Base/Assert.hpp"
#include "Source/Base/Logger.hpp"

namespace gfx::gle {

    struct FramebufferBindGuard {
        inline explicit
        FramebufferBindGuard(GLuint framebufferID) noexcept {
            glBindFramebuffer(GL_FRAMEBUFFER, framebufferID);
        }

        inline
        ~FramebufferBindGuard() noexcept {
            glBindFramebuffer(GL_FRAMEBUFFER, 0);
        }
    };

    struct TextureBindGuard {
        inline explicit
        TextureBindGuard(GLuint textureID) noexcept {
            glBindTexture(GL_TEXTURE_2D, textureID);
        }

        inline
        ~TextureBindGuard() noexcept {
            glBindTexture(GL_TEXTURE_2D, 0);
        }
    };

    GLRenderTarget::~GLRenderTarget() noexcept {
        glDeleteTextures(1, &m_textureID);
        glDeleteFramebuffers(1, &m_framebufferID);
    }

    void
    GLRenderTarget::beginFrame(Color initialBackgroundColor) noexcept {
        glBindFramebuffer(GL_FRAMEBUFFER, m_framebufferID);
        glViewport(0, 0, static_cast<GLsizei>(m_size.width()), static_cast<GLsizei>(m_size.height()));\

        glClearColor(initialBackgroundColor.r(), initialBackgroundColor.g(), initialBackgroundColor.b(),
                     initialBackgroundColor.a());
        glClear(GL_COLOR_BUFFER_BIT);
    }

    void
    GLRenderTarget::endFrame() noexcept {
        glBindFramebuffer(GL_FRAMEBUFFER, 0);
    }

    bool
    GLRenderTarget::initialize(gfx::Size<std::uint32_t> size) noexcept {
        glGenFramebuffers(1, &m_framebufferID);

        if (m_framebufferID == 0) {
            LOG_ERROR("GLRenderTarget: failed to create framebuffer!");
            return false;
        }

        FramebufferBindGuard framebufferBindGuard{m_framebufferID};

        glGenTextures(1, &m_textureID);

        if (m_textureID == 0) {
            LOG_ERROR("RenderTarget: failed to create texture!");
            return false;
        }

        TextureBindGuard textureBindGuard{m_textureID};

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, static_cast<GLsizei>(size.width()),
                     static_cast<GLsizei>(size.height()), 0, GL_RGB, GL_UNSIGNED_BYTE, nullptr);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

        glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, m_textureID, 0);

        constexpr const std::array<GLenum, 1> drawBuffers{GL_COLOR_ATTACHMENT0};
        glDrawBuffers(std::size(drawBuffers), std::data(drawBuffers));

        if (glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE) {
            LOG_ERROR("RenderTarget: framebuffer is apparently incomplete!");
            return false;
        }

        m_size = size;
        return true;
    }

} // namespace gfx::gle
