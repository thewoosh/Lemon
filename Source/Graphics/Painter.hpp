/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include "Source/Graphics/Forward.hpp"
#include "Source/Graphics/RenderTarget.hpp"
#include "Source/Graphics/Types/Color.hpp"
#include "Source/Graphics/Types/Position.hpp"
#include "Source/Graphics/Types/Size.hpp"

namespace gfx {

    class Painter {
    public:
        virtual
        ~Painter() noexcept = default;

        [[nodiscard]] virtual bool
        initialize(gfx::Size<std::uint32_t> windowSize) noexcept = 0;

        virtual void
        beginFrame() noexcept {}

        virtual void
        endFrame() noexcept {}

        [[nodiscard]] virtual RenderTarget *
        createRenderTarget(gfx::Size<std::uint32_t> size) noexcept = 0;

        /**
         * Same as the other calculateTextSize(), but using the default font.
         */
        [[nodiscard]] virtual Size<float>
        calculateTextSize(std::string_view, FontSize fontSize) noexcept = 0;

        [[nodiscard]] virtual Size<float>
        calculateTextSize(Font *, std::string_view, FontSize fontSize) noexcept = 0;

        [[nodiscard]] virtual Font *
        loadFontFromDisk(std::string_view path) noexcept = 0;

        virtual void
        setCurrentFont(Font *) noexcept = 0;

        virtual void
        setDefaultFont(Font *) noexcept = 0;

        virtual void
        setDefaultFontAsCurrentFont() noexcept = 0;

        virtual void
        onFramebufferResize(Size<std::uint32_t>) noexcept = 0;

        virtual void
        paintBorderAroundRect(Position<float>, Size<float>, Color, float width) noexcept = 0;

        virtual void
        paintRect(Position<float>, Size<float>, Color) noexcept = 0;

        virtual void
        paintRenderTarget(RenderTarget *renderTarget, Position<float> position = {0, 0},
                          Size<float> size={0, 0}) noexcept = 0;

        virtual void
        paintText(std::string_view, Position<float>, Color, FontSize) noexcept = 0;

        virtual void
        paintTexturedRect(Position<float>, Size<float>) noexcept = 0;

        virtual void
        paintUnderlineDecorationForText(std::string_view, Position<float>, Color, FontSize, float width) noexcept = 0;
    };

} // namespace gfx
