/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/Application.hpp"

#include <fmt/core.h>
#include <fmt/format.h>

#include "Source/Base/About.hpp"
#include "Source/Base/Logger.hpp"
#include "Source/Graphics/OpenGL/GLPainter.hpp"
#include "Source/GUI/Window/GLFW/GLFWWindow.hpp"
#include "Source/HTML/Parser/Parser.hpp"
#include "Source/IO/FileReader.hpp"
#include "Source/Text/Encoding/UTF8Decoder.hpp"

void
Application::changeCursor(gui::CursorType cursor) noexcept {
    m_window->setCursor(cursor);
}

void
Application::changeTitle(std::string_view tabTitle) noexcept {
    m_window->setTitle(createTitle(tabTitle));
}

bool
Application::createRenderer() noexcept {
    m_painter = std::make_unique<gfx::GLPainter>();

    if (!m_painter->initialize(m_window->framebufferSize())) {
        LOG_ERROR("Failed to create Renderer (OpenGL)");
        return false;
    }

    return true;
}

bool
Application::createTab(std::string_view location) noexcept {
    io::FileReader reader(location);
    if (!reader) {
        LOG_ERROR("Unexpected I/O open error for file: {}", location);
        return false;
    }

    const auto fileContents = reader.readAll();
    if (!fileContents.couldRead()) {
        LOG_ERROR("Unexpected I/O read error for file: {}", location);
        return false;
    }

    const auto data = text::encoding::decodeUTF8(fileContents.contents());
    if (!data.has_value()) {
        LOG_ERROR("UTF-8 decode error!");
        return false;
    }

    html::Parser parser{};
    auto document = parser.parseDocument(data.value());

    if (!document)
        return false;

    m_tabs.emplace_back(this, std::move(document));

    if (!m_tabs.back().initialize(m_painter.get()))
        return false;

    return true;
}

std::string
Application::createTitle(std::string_view tabTitle) noexcept {
    return fmt::format(fmt::runtime("{} — {} version {}.{}.{}"), tabTitle, base::About::name,
                       base::About::version.major(), base::About::version.minor(), base::About::version.patch());
}

bool
Application::createWindow() noexcept {
    m_window = std::make_unique<gui::GLFWWindow>();

    if (!m_window->create(gui::GraphicsAPI::OPENGL, createTitle("New Tab"), {1280, 720})) {
        LOG_ERROR("Failed to create Window (GLFW)");
        return false;
    }

    m_window->setMouseClickCallback([&] (gui::event::MouseClick mouseClick) {
        m_tabs.front().onMouseClicked(mouseClick);
    });

    m_window->setMouseMoveCallback([&] (gui::event::MouseMove event) {
        m_tabs.front().onMouseMove(event);
    });

    m_window->setResizeCallback([&] (gui::event::WindowResize event) {
        m_painter->onFramebufferResize(event.size());
        m_tabs.front().onWindowResize(event);
    });

    return true;
}

bool
Application::loadDefaultFont() noexcept {
    constexpr const std::array<std::string_view, 2> fontPaths{
        "/usr/share/fonts/noto/NotoSans-Regular.ttf",
        "/usr/share/fonts/truetype/noto/NotoSans-Regular.ttf"
    };

    for (const auto &path : fontPaths) {
        if (auto *font = m_painter->loadFontFromDisk(path)) {
            m_painter->setDefaultFont(font);
            return true;
        }
    }

    LOG_ERROR("Failed to find a suitable font!");
    return false;
}

bool
Application::run(std::string_view location) noexcept {
    if (!createWindow())
        return false;

    if (!createRenderer())
        return false;

    if (!loadDefaultFont())
        return false;

    if (!createTab(location))
        return false;

    while (!m_window->closeRequested()) {
        m_window->poll();
        m_painter->beginFrame();

        // TODO event loop
        // TODO paint
        m_tabs.front().paint(m_painter.get());

        m_painter->endFrame();
        m_window->swapBuffers();
    }

    return true;
}
