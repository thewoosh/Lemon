/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string_view>

#include "Source/Graphics/Painter.hpp"
#include "Source/GUI/Window/Window.hpp"
#include "Source/Tab/Tab.hpp"

class Application {
public:
    void
    changeCursor(gui::CursorType cursor) noexcept;

    void
    changeTitle(std::string_view) noexcept;

    [[nodiscard]] bool
    run(std::string_view location) noexcept;

    [[nodiscard]] gui::Window *
    window() noexcept {
        return m_window.get();
    }

    [[nodiscard]] const gui::Window *
    window() const noexcept {
        return m_window.get();
    }

private:
    [[nodiscard]] static std::string
    createTitle(std::string_view tabTitle) noexcept;

    [[nodiscard]] bool
    createRenderer() noexcept;

    [[nodiscard]] bool
    createTab(std::string_view location) noexcept;

    [[nodiscard]] bool
    createWindow() noexcept;

    [[nodiscard]] bool
    loadDefaultFont() noexcept;

    std::unique_ptr<gfx::Painter> m_painter;
    std::unique_ptr<gui::Window> m_window;

    std::vector<Tab> m_tabs{};
};
