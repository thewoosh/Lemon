/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/CSS/ColorMap.hpp"

namespace css {

    // https://www.w3.org/TR/css-color-3/#html4
    ColorMap::ColorMap() noexcept {
        m_map.emplace("aliceblue", gfx::Color::fromRGB(0xf0, 0xf8, 0xff));
        m_map.emplace("antiquewhite", gfx::Color::fromRGB(0xfa, 0xeb, 0xd7));
        m_map.emplace("aqua", gfx::Color::fromRGB(0x00, 0xff, 0xff));
        m_map.emplace("aquamarine", gfx::Color::fromRGB(0x7f, 0xff, 0xd4));
        m_map.emplace("azure", gfx::Color::fromRGB(0xf0, 0xff, 0xff));
        m_map.emplace("beige", gfx::Color::fromRGB(0xf5, 0xf5, 0xdc));
        m_map.emplace("bisque", gfx::Color::fromRGB(0xff, 0xe4, 0xc4));
        m_map.emplace("black", gfx::Color::fromRGB(0x00, 0x00, 0x00));
        m_map.emplace("blanchedalmond", gfx::Color::fromRGB(0xff, 0xeb, 0xcd));
        m_map.emplace("blue", gfx::Color::fromRGB(0x00, 0x00, 0xff));
        m_map.emplace("blueviolet", gfx::Color::fromRGB(0x8a, 0x2b, 0xe2));
        m_map.emplace("brown", gfx::Color::fromRGB(0xa5, 0x2a, 0x2a));
        m_map.emplace("burlywood", gfx::Color::fromRGB(0xde, 0xb8, 0x87));
        m_map.emplace("cadetblue", gfx::Color::fromRGB(0x5f, 0x9e, 0xa0));
        m_map.emplace("chartreuse", gfx::Color::fromRGB(0x7f, 0xff, 0x00));
        m_map.emplace("chocolate", gfx::Color::fromRGB(0xd2, 0x69, 0x1e));
        m_map.emplace("coral", gfx::Color::fromRGB(0xff, 0x7f, 0x50));
        m_map.emplace("cornflowerblue", gfx::Color::fromRGB(0x64, 0x95, 0xed));
        m_map.emplace("cornsilk", gfx::Color::fromRGB(0xff, 0xf8, 0xdc));
        m_map.emplace("crimson", gfx::Color::fromRGB(0xdc, 0x14, 0x3c));
        m_map.emplace("cyan", gfx::Color::fromRGB(0x00, 0xff, 0xff));
        m_map.emplace("darkblue", gfx::Color::fromRGB(0x00, 0x00, 0x8b));
        m_map.emplace("darkcyan", gfx::Color::fromRGB(0x00, 0x8b, 0x8b));
        m_map.emplace("darkgoldenrod", gfx::Color::fromRGB(0xb8, 0x86, 0x0b));
        m_map.emplace("darkgray", gfx::Color::fromRGB(0xa9, 0xa9, 0xa9));
        m_map.emplace("darkgreen", gfx::Color::fromRGB(0x00, 0x64, 0x00));
        m_map.emplace("darkgrey", gfx::Color::fromRGB(0xa9, 0xa9, 0xa9));
        m_map.emplace("darkkhaki", gfx::Color::fromRGB(0xbd, 0xb7, 0x6b));
        m_map.emplace("darkmagenta", gfx::Color::fromRGB(0x8b, 0x00, 0x8b));
        m_map.emplace("darkolivegreen", gfx::Color::fromRGB(0x55, 0x6b, 0x2f));
        m_map.emplace("darkorange", gfx::Color::fromRGB(0xff, 0x8c, 0x00));
        m_map.emplace("darkorchid", gfx::Color::fromRGB(0x99, 0x32, 0xcc));
        m_map.emplace("darkred", gfx::Color::fromRGB(0x8b, 0x00, 0x00));
        m_map.emplace("darksalmon", gfx::Color::fromRGB(0xe9, 0x96, 0x7a));
        m_map.emplace("darkseagreen", gfx::Color::fromRGB(0x8f, 0xbc, 0x8f));
        m_map.emplace("darkslateblue", gfx::Color::fromRGB(0x48, 0x3d, 0x8b));
        m_map.emplace("darkslategray", gfx::Color::fromRGB(0x2f, 0x4f, 0x4f));
        m_map.emplace("darkslategrey", gfx::Color::fromRGB(0x2f, 0x4f, 0x4f));
        m_map.emplace("darkturquoise", gfx::Color::fromRGB(0x00, 0xce, 0xd1));
        m_map.emplace("darkviolet", gfx::Color::fromRGB(0x94, 0x00, 0xd3));
        m_map.emplace("deeppink", gfx::Color::fromRGB(0xff, 0x14, 0x93));
        m_map.emplace("deepskyblue", gfx::Color::fromRGB(0x00, 0xbf, 0xff));
        m_map.emplace("dimgray", gfx::Color::fromRGB(0x69, 0x69, 0x69));
        m_map.emplace("dimgrey", gfx::Color::fromRGB(0x69, 0x69, 0x69));
        m_map.emplace("dodgerblue", gfx::Color::fromRGB(0x1e, 0x90, 0xff));
        m_map.emplace("firebrick", gfx::Color::fromRGB(0xb2, 0x22, 0x22));
        m_map.emplace("floralwhite", gfx::Color::fromRGB(0xff, 0xfa, 0xf0));
        m_map.emplace("forestgreen", gfx::Color::fromRGB(0x22, 0x8b, 0x22));
        m_map.emplace("fuchsia", gfx::Color::fromRGB(0xff, 0x00, 0xff));
        m_map.emplace("gainsboro", gfx::Color::fromRGB(0xdc, 0xdc, 0xdc));
        m_map.emplace("ghostwhite", gfx::Color::fromRGB(0xf8, 0xf8, 0xff));
        m_map.emplace("gold", gfx::Color::fromRGB(0xff, 0xd7, 0x00));
        m_map.emplace("goldenrod", gfx::Color::fromRGB(0xda, 0xa5, 0x20));
        m_map.emplace("gray", gfx::Color::fromRGB(0x80, 0x80, 0x80));
        m_map.emplace("green", gfx::Color::fromRGB(0x00, 0x80, 0x00));
        m_map.emplace("greenyellow", gfx::Color::fromRGB(0xad, 0xff, 0x2f));
        m_map.emplace("grey", gfx::Color::fromRGB(0x80, 0x80, 0x80));
        m_map.emplace("honeydew", gfx::Color::fromRGB(0xf0, 0xff, 0xf0));
        m_map.emplace("hotpink", gfx::Color::fromRGB(0xff, 0x69, 0xb4));
        m_map.emplace("indianred", gfx::Color::fromRGB(0xcd, 0x5c, 0x5c));
        m_map.emplace("indigo", gfx::Color::fromRGB(0x4b, 0x00, 0x82));
        m_map.emplace("ivory", gfx::Color::fromRGB(0xff, 0xff, 0xf0));
        m_map.emplace("khaki", gfx::Color::fromRGB(0xf0, 0xe6, 0x8c));
        m_map.emplace("lavender", gfx::Color::fromRGB(0xe6, 0xe6, 0xfa));
        m_map.emplace("lavenderblush", gfx::Color::fromRGB(0xff, 0xf0, 0xf5));
        m_map.emplace("lawngreen", gfx::Color::fromRGB(0x7c, 0xfc, 0x00));
        m_map.emplace("lemonchiffon", gfx::Color::fromRGB(0xff, 0xfa, 0xcd));
        m_map.emplace("lightblue", gfx::Color::fromRGB(0xad, 0xd8, 0xe6));
        m_map.emplace("lightcoral", gfx::Color::fromRGB(0xf0, 0x80, 0x80));
        m_map.emplace("lightcyan", gfx::Color::fromRGB(0xe0, 0xff, 0xff));
        m_map.emplace("lightgoldenrodyellow", gfx::Color::fromRGB(0xfa, 0xfa, 0xd2));
        m_map.emplace("lightgray", gfx::Color::fromRGB(0xd3, 0xd3, 0xd3));
        m_map.emplace("lightgreen", gfx::Color::fromRGB(0x90, 0xee, 0x90));
        m_map.emplace("lightgrey", gfx::Color::fromRGB(0xd3, 0xd3, 0xd3));
        m_map.emplace("lightpink", gfx::Color::fromRGB(0xff, 0xb6, 0xc1));
        m_map.emplace("lightsalmon", gfx::Color::fromRGB(0xff, 0xa0, 0x7a));
        m_map.emplace("lightseagreen", gfx::Color::fromRGB(0x20, 0xb2, 0xaa));
        m_map.emplace("lightskyblue", gfx::Color::fromRGB(0x87, 0xce, 0xfa));
        m_map.emplace("lightslategray", gfx::Color::fromRGB(0x77, 0x88, 0x99));
        m_map.emplace("lightslategrey", gfx::Color::fromRGB(0x77, 0x88, 0x99));
        m_map.emplace("lightsteelblue", gfx::Color::fromRGB(0xb0, 0xc4, 0xde));
        m_map.emplace("lightyellow", gfx::Color::fromRGB(0xff, 0xff, 0xe0));
        m_map.emplace("lime", gfx::Color::fromRGB(0x00, 0xff, 0x00));
        m_map.emplace("limegreen", gfx::Color::fromRGB(0x32, 0xcd, 0x32));
        m_map.emplace("linen", gfx::Color::fromRGB(0xfa, 0xf0, 0xe6));
        m_map.emplace("magenta", gfx::Color::fromRGB(0xff, 0x00, 0xff));
        m_map.emplace("maroon", gfx::Color::fromRGB(0x80, 0x00, 0x00));
        m_map.emplace("mediumaquamarine", gfx::Color::fromRGB(0x66, 0xcd, 0xaa));
        m_map.emplace("mediumblue", gfx::Color::fromRGB(0x00, 0x00, 0xcd));
        m_map.emplace("mediumorchid", gfx::Color::fromRGB(0xba, 0x55, 0xd3));
        m_map.emplace("mediumpurple", gfx::Color::fromRGB(0x93, 0x70, 0xdb));
        m_map.emplace("mediumseagreen", gfx::Color::fromRGB(0x3c, 0xb3, 0x71));
        m_map.emplace("mediumslateblue", gfx::Color::fromRGB(0x7b, 0x68, 0xee));
        m_map.emplace("mediumspringgreen", gfx::Color::fromRGB(0x00, 0xfa, 0x9a));
        m_map.emplace("mediumturquoise", gfx::Color::fromRGB(0x48, 0xd1, 0xcc));
        m_map.emplace("mediumvioletred", gfx::Color::fromRGB(0xc7, 0x15, 0x85));
        m_map.emplace("midnightblue", gfx::Color::fromRGB(0x19, 0x19, 0x70));
        m_map.emplace("mintcream", gfx::Color::fromRGB(0xf5, 0xff, 0xfa));
        m_map.emplace("mistyrose", gfx::Color::fromRGB(0xff, 0xe4, 0xe1));
        m_map.emplace("moccasin", gfx::Color::fromRGB(0xff, 0xe4, 0xb5));
        m_map.emplace("navajowhite", gfx::Color::fromRGB(0xff, 0xde, 0xad));
        m_map.emplace("navy", gfx::Color::fromRGB(0x00, 0x00, 0x80));
        m_map.emplace("oldlace", gfx::Color::fromRGB(0xfd, 0xf5, 0xe6));
        m_map.emplace("olive", gfx::Color::fromRGB(0x80, 0x80, 0x00));
        m_map.emplace("olivedrab", gfx::Color::fromRGB(0x6b, 0x8e, 0x23));
        m_map.emplace("orange", gfx::Color::fromRGB(0xff, 0xa5, 0x00));
        m_map.emplace("orangered", gfx::Color::fromRGB(0xff, 0x45, 0x00));
        m_map.emplace("orchid", gfx::Color::fromRGB(0xda, 0x70, 0xd6));
        m_map.emplace("palegoldenrod", gfx::Color::fromRGB(0xee, 0xe8, 0xaa));
        m_map.emplace("palegreen", gfx::Color::fromRGB(0x98, 0xfb, 0x98));
        m_map.emplace("paleturquoise", gfx::Color::fromRGB(0xaf, 0xee, 0xee));
        m_map.emplace("palevioletred", gfx::Color::fromRGB(0xdb, 0x70, 0x93));
        m_map.emplace("papayawhip", gfx::Color::fromRGB(0xff, 0xef, 0xd5));
        m_map.emplace("peachpuff", gfx::Color::fromRGB(0xff, 0xda, 0xb9));
        m_map.emplace("peru", gfx::Color::fromRGB(0xcd, 0x85, 0x3f));
        m_map.emplace("pink", gfx::Color::fromRGB(0xff, 0xc0, 0xcb));
        m_map.emplace("plum", gfx::Color::fromRGB(0xdd, 0xa0, 0xdd));
        m_map.emplace("powderblue", gfx::Color::fromRGB(0xb0, 0xe0, 0xe6));
        m_map.emplace("purple", gfx::Color::fromRGB(0x80, 0x00, 0x80));
        m_map.emplace("red", gfx::Color::fromRGB(0xff, 0x00, 0x00));
        m_map.emplace("rosybrown", gfx::Color::fromRGB(0xbc, 0x8f, 0x8f));
        m_map.emplace("royalblue", gfx::Color::fromRGB(0x41, 0x69, 0xe1));
        m_map.emplace("saddlebrown", gfx::Color::fromRGB(0x8b, 0x45, 0x13));
        m_map.emplace("salmon", gfx::Color::fromRGB(0xfa, 0x80, 0x72));
        m_map.emplace("sandybrown", gfx::Color::fromRGB(0xf4, 0xa4, 0x60));
        m_map.emplace("seagreen", gfx::Color::fromRGB(0x2e, 0x8b, 0x57));
        m_map.emplace("seashell", gfx::Color::fromRGB(0xff, 0xf5, 0xee));
        m_map.emplace("sienna", gfx::Color::fromRGB(0xa0, 0x52, 0x2d));
        m_map.emplace("silver", gfx::Color::fromRGB(0xc0, 0xc0, 0xc0));
        m_map.emplace("skyblue", gfx::Color::fromRGB(0x87, 0xce, 0xeb));
        m_map.emplace("slateblue", gfx::Color::fromRGB(0x6a, 0x5a, 0xcd));
        m_map.emplace("slategray", gfx::Color::fromRGB(0x70, 0x80, 0x90));
        m_map.emplace("slategrey", gfx::Color::fromRGB(0x70, 0x80, 0x90));
        m_map.emplace("snow", gfx::Color::fromRGB(0xff, 0xfa, 0xfa));
        m_map.emplace("springgreen", gfx::Color::fromRGB(0x00, 0xff, 0x7f));
        m_map.emplace("steelblue", gfx::Color::fromRGB(0x46, 0x82, 0xb4));
        m_map.emplace("tan", gfx::Color::fromRGB(0xd2, 0xb4, 0x8c));
        m_map.emplace("teal", gfx::Color::fromRGB(0x00, 0x80, 0x80));
        m_map.emplace("thistle", gfx::Color::fromRGB(0xd8, 0xbf, 0xd8));
        m_map.emplace("tomato", gfx::Color::fromRGB(0xff, 0x63, 0x47));
        m_map.emplace("transparent", gfx::Color{1.0f, 0.0f, 0.0f, 0.0f});
        m_map.emplace("turquoise", gfx::Color::fromRGB(0x40, 0xe0, 0xd0));
        m_map.emplace("violet", gfx::Color::fromRGB(0xee, 0x82, 0xee));
        m_map.emplace("wheat", gfx::Color::fromRGB(0xf5, 0xde, 0xb3));
        m_map.emplace("white", gfx::Color::fromRGB(0xff, 0xff, 0xff));
        m_map.emplace("whitesmoke", gfx::Color::fromRGB(0xf5, 0xf5, 0xf5));
        m_map.emplace("yellow", gfx::Color::fromRGB(0xff, 0xff, 0x00));
        m_map.emplace("yellowgreen", gfx::Color::fromRGB(0x9a, 0xcd, 0x32));
    }

    std::optional<gfx::Color>
    ColorMap::find(std::string_view name) const noexcept {
        auto color = m_map.find(name);

        if (color != std::cend(m_map)) {
            return color->second;
        }

        return std::nullopt;
    }

} // namespace css
