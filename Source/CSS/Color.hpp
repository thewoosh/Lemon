/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <cstdint>

namespace css {

    struct Color {
        [[nodiscard]] Color() noexcept = default;
        [[nodiscard]] Color(Color &&) noexcept = default;
        [[nodiscard]] Color(const Color &) noexcept = default;

        [[nodiscard]] inline constexpr
        Color(std::uint8_t r, std::uint8_t g, std::uint8_t b, std::uint8_t a = 0xFF)
            : m_r(r)
            , m_g(g)
            , m_b(b)
            , m_a(a) {
        }

        [[nodiscard]] inline constexpr std::uint8_t
        r() const noexcept {
            return m_r;
        }

        [[nodiscard]] inline constexpr std::uint8_t
        g() const noexcept {
            return m_g;
        }

        [[nodiscard]] inline constexpr std::uint8_t
        b() const noexcept {
            return m_b;
        }

        [[nodiscard]] inline constexpr std::uint8_t
        a() const noexcept {
            return m_a;
        }

    private:
        std::uint8_t m_r;
        std::uint8_t m_g;
        std::uint8_t m_b;
        std::uint8_t m_a;
    };

} // namespace css
