/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <vector>

#include "Source/CSS/DeclarationTranslator.hpp"
#include "Source/CSS/StyleProperties.hpp"

namespace dom {
    struct Document;
    struct Element;
} // namespace dom

namespace css {

    struct Stylesheet;

    class StyleResolver {
    public:
        [[nodiscard]] explicit
        StyleResolver(const dom::Document *document) noexcept;

        ~StyleResolver() noexcept;

        void
        collectStyleProperties(StyleProperties &properties, const dom::Element *element) const noexcept;

    private:
        void
        collectStylePropertiesForStylesheet(const Stylesheet &, StyleProperties &, const dom::Element *) const noexcept;

        void
        collectStylesheets() noexcept;

        const dom::Document *const m_document;
        std::vector<Stylesheet> m_stylesheets;

        DeclarationTranslator m_declarationTranslator{};
    };

} // namespace css
