/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/CSS/DeclarationTranslator.hpp"

#include "Source/Base/Assert.hpp"
#include "Source/CSS/Declaration.hpp"
#include "Source/CSS/StyleProperties.hpp"
#include "Source/CSS/ValueParser.hpp"

namespace css {

    using namespace ValueParser;

    template <typename T, typename ValueConverter>
    void
    applyShorthandToEdges(Edge<T> &edge, const std::vector<Value> &values, ValueConverter valueConverter) noexcept {
        if (values.size() == 1) {
            if (auto value = valueConverter(values[0])) {
                edge.top = value.value();
                edge.bottom = value.value();
                edge.left = value.value();
                edge.right = value.value();
            }
        } else if (values.size() == 2) {
            if (auto value = valueConverter(values[0])) {
                edge.top = value.value();
                edge.bottom = value.value();
            }
            if (auto value = valueConverter(values[1])) {
                edge.left = value.value();
                edge.right = value.value();
            }
        } else if (values.size() == 3) {
            if (auto value = valueConverter(values[0])) {
                edge.top = value.value();
            }
            if (auto value = valueConverter(values[1])) {
                edge.right = value.value();
                edge.left = value.value();
            }
            if (auto value = valueConverter(values[2])) {
                edge.bottom = value.value();
            }
        } else if (values.size() == 4) {
            if (auto value = valueConverter(values[0])) {
                edge.top = value.value();
            }
            if (auto value = valueConverter(values[2])) {
                edge.right = value.value();
            }
            if (auto value = valueConverter(values[3])) {
                edge.bottom = value.value();
            }
            if (auto value = valueConverter(values[1])) {
                edge.left = value.value();
            }
        }
    }

    DeclarationTranslator::DeclarationTranslator() noexcept {
        const auto pixelConverter = [] (const Value &value) {
            return toPixels(value);
        };

        m_map.emplace("background-color", [this] (StyleProperties &styleProperties, const Declaration &declaration) {
            if (auto color = toColor(m_colorMap, declaration.values().at(0)))
                styleProperties.backgroundColor = color.value();
        });

        m_map.emplace("border", [&] (StyleProperties &styleProperties, const Declaration &declaration) {
            if (std::size(declaration.values()) == 0 || std::size(declaration.values()) > 3) {
                LOG_WARNING("Invalid 'border' property!");
                return;
            }

            const auto previousColor = styleProperties.borderColor;
            const auto previousStyle = styleProperties.borderStyle;
            const auto previousWidth = styleProperties.border;

            bool hasWidth{false}, hasStyle{false}, hasColor{false};
            bool isInvalid{false};
            for (const auto &value : declaration.values()) {
                // TODO there are thick, medium etc. border-widths

                if (auto colorValue = toColor(m_colorMap, value)) {
                    if (hasColor) {
                        LOG_WARNING("Invalid 'border' property: multiple colors defined");
                        isInvalid = true;
                        break;
                    }

                    hasColor = true;
                    styleProperties.borderColor = colorValue.value();
                } else if (value.type() == Value::Type::LENGTH) {
                    if (hasWidth) {
                        LOG_WARNING("Invalid 'border' property: multiple widths defined");
                        isInvalid = true;
                        break;
                    }

                    hasWidth = true;
                    styleProperties.border = toPixels(value).value();
                } else if (value.type() == Value::Type::IDENTIFIER) {
                    if (hasStyle) {
                        LOG_WARNING("Invalid 'border' property: multiple widths defined");
                        isInvalid = true;
                        break;
                    }

                    const auto &style = std::get<std::string>(value);
                    if (style == "solid") {
                        styleProperties.borderStyle = BorderStyle::SOLID;
                    } else if (style == "inset") {
                        // TODO!
                        styleProperties.borderStyle = BorderStyle::SOLID;
                    } else if (style == "none") {
                        styleProperties.borderStyle = BorderStyle::NONE;
                    } else {
                        LOG_WARNING("Invalid 'border' property: invalid border style: \"{}\"", style);
                        isInvalid = true;
                    }

                    hasStyle = true;
                } else {
                    LOG_WARNING("Invalid 'border' property: undefined value of type {}", value.type());
                    isInvalid = true;
                }
            }

            if (isInvalid) {
                styleProperties.borderColor = previousColor;
                styleProperties.borderStyle = previousStyle;
                styleProperties.border = previousWidth;
            }
        });

        m_map.emplace("border-color", [&] (StyleProperties &styleProperties, const Declaration &declaration) {
            if (auto color = toColor(m_colorMap, declaration.values().at(0)))
                styleProperties.borderColor = Edge{color.value()};
        });

        m_map.emplace("border-style", [&] (StyleProperties &styleProperties, const Declaration &declaration) {
            if (declaration.values().at(0).type() != Value::Type::IDENTIFIER)
                return;

            const auto &type = std::get<std::string>(declaration.values().at(0));
            if (type == "solid") {
                styleProperties.borderStyle = Edge{BorderStyle::SOLID};
            } else if (type == "none") {
                styleProperties.borderStyle = Edge{BorderStyle::NONE};
            } else {
                LOG_WARNING("Unknown border-style type: {}", type);
            }
        });

        m_map.emplace("border-width", [&] (StyleProperties &styleProperties, const Declaration &declaration) {
            applyShorthandToEdges(styleProperties.border, declaration.values(), pixelConverter);
        });

        m_map.emplace("border-left-width", [&] (StyleProperties &styleProperties, const Declaration &declaration) {
            if (declaration.values().empty())
                return;
            if (auto length = toPixels(declaration.values().at(0)))
                styleProperties.margin.left = length.value();
        });

        m_map.emplace("border-right-width", [&] (StyleProperties &styleProperties, const Declaration &declaration) {
            if (declaration.values().empty())
                return;
            if (auto length = toPixels(declaration.values().at(0)))
                styleProperties.margin.right = length.value();
        });

        m_map.emplace("border-top-width", [&] (StyleProperties &styleProperties, const Declaration &declaration) {
            if (declaration.values().empty())
                return;
            if (auto length = toPixels(declaration.values().at(0)))
                styleProperties.margin.top = length.value();
        });

        m_map.emplace("border-bottom-width", [&] (StyleProperties &styleProperties, const Declaration &declaration) {
            if (declaration.values().empty())
                return;
            if (auto length = toPixels(declaration.values().at(0)))
                styleProperties.margin.bottom = length.value();
        });

        m_map.emplace("clear", [&] (StyleProperties &styleProperties, const Declaration &declaration) {
            if (declaration.values().size() != 1 || declaration.values().front().type() != Value::Type::IDENTIFIER) {
                LOG_ERROR("Invalid value for 'clear' property!");
                return;
            }

            const auto &type = std::get<std::string>(declaration.values().front());
            if (type == "left") {
                styleProperties.clear = ClearType::LEFT;
            } else if (type == "right") {
                styleProperties.clear = ClearType::RIGHT;
            } else if (type == "none") {
                styleProperties.clear = ClearType::NONE;
            } else if (type == "inline-start") {
                styleProperties.clear = ClearType::INLINE_START;
            } else if (type == "inline-end") {
                styleProperties.clear = ClearType::INLINE_END;
            } else if (type == "both") {
                styleProperties.clear = ClearType::BOTH;
            } else {
                LOG_ERROR("Invalid value for 'clear' property: {}", type);
            }
        });

        m_map.emplace("color", [this] (StyleProperties &styleProperties, const Declaration &declaration) {
            if (auto color = toColor(m_colorMap, declaration.values().at(0)))
                styleProperties.textColor = color.value();
        });

        m_map.emplace("display", [&] (StyleProperties &styleProperties, const Declaration &declaration) {
            if (declaration.values().at(0).type() != Value::Type::IDENTIFIER) {
                LOG_WARNING("Invalid value for 'display' property: index={}", declaration.values().at(0).index());
                return;
            }

            const auto &type = std::get<std::string>(declaration.values().at(0));
            if (type == "block") {
                styleProperties.display = DisplayType::BLOCK;
            } else if (type == "inline") {
                styleProperties.display = DisplayType::INLINE;
            } else if (type == "list-item") {
                styleProperties.display = DisplayType::LIST_ITEM;
            } else if (type == "none") {
                styleProperties.display = DisplayType::NONE;
            } else {
                LOG_WARNING("Invalid value for 'display' property: \"{}\"", type);
            }
        });

        m_map.emplace("float", [&] (StyleProperties &styleProperties, const Declaration &declaration) {
            if (declaration.values().size() != 1 || declaration.values().front().type() != Value::Type::IDENTIFIER) {
                LOG_ERROR("Invalid value for 'float' property!");
                return;
            }

            const auto &type = std::get<std::string>(declaration.values().front());
            if (type == "left") {
                styleProperties.floatType = FloatType::LEFT;
            } else if (type == "right") {
                styleProperties.floatType = FloatType::RIGHT;
            } else if (type == "none") {
                styleProperties.floatType = FloatType::NONE;
            } else if (type == "inline-start") {
                styleProperties.floatType = FloatType::INLINE_START;
            } else if (type == "inline-end") {
                styleProperties.floatType = FloatType::INLINE_END;
            } else {
                LOG_ERROR("Invalid value for 'float' property: {}", type);
            }
        });

        m_map.emplace("font-size", [&] (StyleProperties &styleProperties, const Declaration &declaration) {
            if (auto pixels = toPixels(declaration.values().at(0)))
                styleProperties.fontSize = pixels.value();
        });

        m_map.emplace("height", [&] (StyleProperties &styleProperties, const Declaration &declaration) {
            if (declaration.values().empty()) {
                LOG_WARNING("Invalid value for 'height' property: empty");
                return;
            }
            if (declaration.values().front().type() == Value::Type::IDENTIFIER) {
                const auto &string = std::get<std::string>(declaration.values().front());
                if (string == "auto") {
                    styleProperties.height.value = 0;
                    styleProperties.height.unit = VLVUnit::AUTO;
                } else {
                    LOG_WARNING("Invalid value for 'height' property: \"{}\"", string);
                }
            } else if (auto pixels = toPixels(declaration.values().front())) {
                styleProperties.height = pixels.value();
            } else {
                LOG_WARNING("Invalid value for 'height' property: ???");
            }
        });

        m_map.emplace("margin", [&] (StyleProperties &styleProperties, const Declaration &declaration) {
            applyShorthandToEdges(styleProperties.margin, declaration.values(), pixelConverter);
        });

        m_map.emplace("margin-left", [&] (StyleProperties &styleProperties, const Declaration &declaration) {
            if (declaration.values().empty())
                return;
            if (auto length = toPixels(declaration.values().at(0)))
                styleProperties.margin.left = length.value();
        });

        m_map.emplace("margin-right", [&] (StyleProperties &styleProperties, const Declaration &declaration) {
            if (declaration.values().empty())
                return;
            if (auto length = toPixels(declaration.values().at(0)))
                styleProperties.margin.right = length.value();
        });

        m_map.emplace("margin-top", [&] (StyleProperties &styleProperties, const Declaration &declaration) {
            if (declaration.values().empty())
                return;
            if (auto length = toPixels(declaration.values().at(0)))
                styleProperties.margin.top = length.value();
        });

        m_map.emplace("margin-bottom", [&] (StyleProperties &styleProperties, const Declaration &declaration) {
            if (declaration.values().empty())
                return;
            if (auto length = toPixels(declaration.values().at(0)))
                styleProperties.margin.bottom = length.value();
        });

        m_map.emplace("padding", [&] (StyleProperties &styleProperties, const Declaration &declaration) {
            applyShorthandToEdges(styleProperties.padding, declaration.values(), pixelConverter);
        });

        m_map.emplace("padding-left", [&] (StyleProperties &styleProperties, const Declaration &declaration) {
            if (declaration.values().empty())
                return;
            if (auto length = toPixels(declaration.values().at(0)))
                styleProperties.padding.left = length.value();
        });

        m_map.emplace("padding-right", [&] (StyleProperties &styleProperties, const Declaration &declaration) {
            if (declaration.values().empty())
                return;
            if (auto length = toPixels(declaration.values().at(0)))
                styleProperties.padding.right = length.value();
        });

        m_map.emplace("padding-top", [&] (StyleProperties &styleProperties, const Declaration &declaration) {
            if (declaration.values().empty())
                return;
            if (auto length = toPixels(declaration.values().at(0)))
                styleProperties.padding.top = length.value();
        });

        m_map.emplace("padding-bottom", [&] (StyleProperties &styleProperties, const Declaration &declaration) {
            if (declaration.values().empty())
                return;
            if (auto length = toPixels(declaration.values().at(0)))
                styleProperties.padding.bottom = length.value();
        });

        m_map.emplace("text-decoration", [&] (StyleProperties &styleProperties, const Declaration &declaration) {
            // TODO this should be a shorthand
            if (!declaration.values().empty() && declaration.values().front().type() == Value::Type::IDENTIFIER) {
                const auto &type = std::get<std::string>(declaration.values().front());

                if (type == "none") {
                    styleProperties.textDecorationLine = TextDecorationLineType::NONE;
                    return;
                }

                if (type == "underline") {
                    styleProperties.textDecorationLine = TextDecorationLineType::UNDERLINE;
                    return;
                }
            }
            LOG_ERROR("Invalid 'text-decoration' declaration!");
        });

        m_map.emplace("unicode-bidi", [&] (StyleProperties &styleProperties, const Declaration &declaration) {
            if (declaration.values().size() != 1 || declaration.values().front().type() != Value::Type::IDENTIFIER) {
                LOG_ERROR("Invalid value for 'unicode-bidi' property!");
                return;
            }

            const auto &type = std::get<std::string>(declaration.values().front());
            if (type == "normal") {
                styleProperties.unicodeBidi = UnicodeBidiType::NORMAL;
            } else if (type == "embed") {
                styleProperties.unicodeBidi = UnicodeBidiType::EMBED;
            } else if (type == "isolate") {
                styleProperties.unicodeBidi = UnicodeBidiType::ISOLATE;
            } else if (type == "bidi-override") {
                styleProperties.unicodeBidi = UnicodeBidiType::BIDI_OVERRIDE;
            } else if (type == "isolate-override") {
                styleProperties.unicodeBidi = UnicodeBidiType::ISOLATE_OVERRIDE;
            } else if (type == "plaintext") {
                styleProperties.unicodeBidi = UnicodeBidiType::PLAINTEXT;
            } else {
                LOG_ERROR("Invalid value for 'unicode-bidi' property: {}", type);
            }
        });

        m_map.emplace("width", [&] (StyleProperties &styleProperties, const Declaration &declaration) {
            if (declaration.values().empty()) {
                LOG_WARNING("Invalid value for 'width' property: empty");
                return;
            }
            if (declaration.values().front().type() == Value::Type::IDENTIFIER) {
                const auto &string = std::get<std::string>(declaration.values().front());
                if (string == "auto") {
                    styleProperties.width.value = 0;
                    styleProperties.width.unit = VLVUnit::AUTO;
                } else {
                    LOG_WARNING("Invalid value for 'width' property: \"{}\"", string);
                }
            } else if (auto pixels = toPixels(declaration.values().front())) {
                styleProperties.width = pixels.value();
            } else {
                LOG_WARNING("Invalid value for 'width' property: ???");
            }
        });
    }

    void
    DeclarationTranslator::applyDeclarationToProperties(StyleProperties &styleProperties,
                                                        const Declaration &declaration) const noexcept {
        // TODO check case-sensitivity
        const auto it = m_map.find(declaration.propertyName());

        if (it == std::cend(m_map)) {
            LOG_WARNING("Unknown property \"{}\"", declaration.propertyName());
            return;
        }

        it->second(styleProperties, declaration);
    }

} // namespace css
