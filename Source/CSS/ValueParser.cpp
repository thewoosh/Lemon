/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/CSS/ValueParser.hpp"

#include "Include/text/Tools.hpp"

#include "Source/Base/Assert.hpp"

namespace css {

    std::optional<float>
    ValueParser::toNumber(const Value &value) noexcept {
        if (value.type() == Value::Type::LENGTH) {
            const auto &length = std::get<Length>(value);
            if (length.type() == Length::Type::ABSOLUTE)
                return length.value();
        }

        return std::nullopt;
    }

    std::optional<float>
    ValueParser::toPercentage(const Value &value) noexcept {
        if (value.type() == Value::Type::LENGTH) {
            const auto &length = std::get<Length>(value);
            if (length.type() == Length::Type::PERCENTAGE)
                return length.value();
        }

        return std::nullopt;
    }

    std::optional<VariadicLengthValue<float>>
    ValueParser::toPixels(const Value &value) noexcept {
        if (value.type() != Value::Type::LENGTH)
            return std::nullopt;
        const auto &length = std::get<Length>(value);

        constexpr const auto inches = 96.0f;
        constexpr const auto centimeter = inches / 2.54f;

        switch (length.type()) {
            case Length::Type::ABSOLUTE:
                return VariadicLengthValue<float>{length.value(), VLVUnit::PIXELS};
            case Length::Type::CENTIMETERS:
                return VariadicLengthValue<float>{length.value() * centimeter, VLVUnit::PIXELS};
            case Length::Type::MILLIMETERS:
                return VariadicLengthValue<float>{length.value() * centimeter / 10.0f, VLVUnit::PIXELS};
            case Length::Type::QUARTER_MILLIMETERS:
                return VariadicLengthValue<float>{length.value() * centimeter / 40.0f, VLVUnit::PIXELS};
            case Length::Type::INCHES:
                return VariadicLengthValue<float>{length.value() * inches, VLVUnit::PIXELS};
            case Length::Type::PICAS:
                return VariadicLengthValue<float>{length.value() * inches / 6.0f, VLVUnit::PIXELS};
            case Length::Type::POINTS:
                return VariadicLengthValue<float>{length.value() * inches / 72.0f, VLVUnit::PIXELS};
            case Length::Type::PIXELS:
                return VariadicLengthValue<float>{length.value(), VLVUnit::PIXELS};

            case Length::Type::PERCENTAGE:
                return VariadicLengthValue<float>{length.value(), VLVUnit::PERCENTAGE};

            case Length::Type::EM:
                return VariadicLengthValue<float>{length.value(), VLVUnit::EM};
            case Length::Type::EX:
                return VariadicLengthValue<float>{length.value(), VLVUnit::EX};
            case Length::Type::CH:
                return VariadicLengthValue<float>{length.value(), VLVUnit::CH};
            case Length::Type::REM:
                return VariadicLengthValue<float>{length.value(), VLVUnit::REM};

            default:
                LEMON_ASSERT_NOT_REACHED_OR_RETURN((VariadicLengthValue<float>{length.value(), VLVUnit::PIXELS}));
        }
    }

    [[nodiscard]] std::optional<std::uint8_t>
    convertHexDigit(char character) noexcept {
        if (text::isDigit(character))
            return character - '0';
        if (character >= 'a' && character <= 'f')
            return character - 'a' + 0xa;
        if (character >= 'A' && character <= 'F')
            return character - 'a' + 0xa;
        return std::nullopt;
    }

    [[nodiscard]] std::optional<std::uint8_t>
    convertDoubleHexDigit(char a, char b) noexcept {
        if (const auto left = convertHexDigit(a))
            if (const auto right = convertHexDigit(b))
                return left.value() * 0x10 + right.value();
        return std::nullopt;
    }

    // https://www.w3.org/TR/css-color-4/#typedef-color
    std::optional<gfx::Color>
    ValueParser::toColor(const ColorMap &colorMap, const Value &value) noexcept {
        if (value.type() == Value::Type::IDENTIFIER) {
            if (auto color = colorMap.find(std::get<std::string>(value))) {
                return color.value();
            }

            return std::nullopt;
        }

        // https://www.w3.org/TR/css-color-4/#hex-notation
        if (value.type() == Value::Type::HASH) {
            const auto &string = std::get<std::string>(value);
            std::optional<std::uint8_t> red, green, blue, alpha;

            if (string.length() == 3) {
                red = convertHexDigit(string[0]);
                green = convertHexDigit(string[1]);
                blue = convertHexDigit(string[2]);
                alpha = 255;
            } else if (string.length() == 4) {
                red = convertHexDigit(string[0]);
                green = convertHexDigit(string[1]);
                blue = convertHexDigit(string[2]);
                alpha = convertHexDigit(string[3]);
            } else if (string.length() == 6) {
                red = convertDoubleHexDigit(string[0], string[1]);
                green = convertDoubleHexDigit(string[2], string[3]);
                blue = convertDoubleHexDigit(string[4], string[5]);
                alpha = 255;
            } else if (string.length() == 8) {
                red = convertDoubleHexDigit(string[0], string[1]);
                green = convertDoubleHexDigit(string[2], string[3]);
                blue = convertDoubleHexDigit(string[4], string[5]);
                alpha = convertDoubleHexDigit(string[6], string[7]);
            }

            if (red && green && blue && alpha)
                return gfx::Color{static_cast<float>(red.value()) / 255.0f,
                                  static_cast<float>(green.value()) / 255.0f,
                                  static_cast<float>(blue.value()) / 255.0f,
                                  static_cast<float>(alpha.value()) / 255.0f};
        }

        // https://www.w3.org/TR/css-color-4/#funcdef-rgb
        // https://developer.mozilla.org/en-US/docs/Web/CSS/color_value/rgb()
        if (value.type() == Value::Type::FUNCTION) {
            const auto &function = std::get<FunctionValueData>(value);
            bool isFunctionRGBA = false;
            if (function.name == "rgb" || (isFunctionRGBA = function.name == "rgba")) {
                if (isFunctionRGBA) {
                    if (function.parameters.size() != 4) {
                        LOG_WARNING("Invalid parameter count for rgba() function: {}", function.parameters.size());
                        return std::nullopt;
                    }
                } else if (function.parameters.size() < 3 || function.parameters.size() > 4) {
                    LOG_WARNING("Invalid parameter count for rgb() function: {}", function.parameters.size());
                    return std::nullopt;
                }

                float red, green, blue, alpha{1.0f};

                if (auto redPercentage = toPercentage(function.parameters[0])) {
                    red = std::min(redPercentage.value() / 100.0f, 1.0f);
                    const auto greenPercentage = toPercentage(function.parameters[1]);
                    const auto bluePercentage = toPercentage(function.parameters[2]);
                    if (!greenPercentage.has_value()) {
                        LOG_WARNING("Invalid value for green component of rgb[a]() function!");
                        return std::nullopt;
                    }
                    if (!bluePercentage.has_value()) {
                        LOG_WARNING("Invalid value for green component of rgb[a]() function!");
                        return std::nullopt;
                    }
                    green = greenPercentage.value() / 100.0f;
                    blue = bluePercentage.value() / 100.0f;
                } else if (auto redNumber = toNumber(function.parameters[0])) {
                    red = redNumber.value() / 255.0f;
                    const auto greenNumber = toNumber(function.parameters[1]);
                    const auto blueNumber = toNumber(function.parameters[2]);
                    if (!greenNumber.has_value()) {
                        LOG_WARNING("Invalid value for green component of rgb[a]() function!");
                        return std::nullopt;
                    }
                    if (!blueNumber.has_value()) {
                        LOG_WARNING("Invalid value for green component of rgb[a]() function!");
                        return std::nullopt;
                    }
                    green = greenNumber.value() / 255.0f;
                    blue = blueNumber.value() / 255.0f;
                } else {
                    return std::nullopt;
                }

                red = std::clamp(red, 0.0f, 1.0f);
                green = std::clamp(green, 0.0f, 1.0f);
                blue = std::clamp(blue, 0.0f, 1.0f);

                if (function.parameters.size() == 4) {
                    if (auto alphaNumber = toNumber(function.parameters[3])) {
                        alpha = alphaNumber.value();
                    } else if (auto alphaPercentage = toPercentage(function.parameters[3])) {
                        alpha = alphaPercentage.value() / 100.0f;
                    } else {
                        LOG_WARNING("Invalid value for alpha component of rgba() function!");
                        return std::nullopt;
                    }

                    alpha = std::clamp(alpha, 0.0f, 1.0f);
                }

                return gfx::Color{red, green, blue, alpha};
            }
        }

        return std::nullopt;
    }

} // css

