/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/CSS/StyleResolver.hpp"

#include "Include/text/Tools.hpp"

#include "Source/Base/Assert.hpp"
#include "Source/CSS/DefaultStylesheet.hpp"
#include "Source/CSS/Parser.hpp"
#include "Source/CSS/Stylesheet.hpp"
#include "Source/CSS/StyleRule.hpp"
#include "Source/DOM/Document.hpp"
#include "Source/DOM/Text.hpp"
#include "Source/HTML/Elements/HTMLStyleElement.hpp"

namespace css {

    StyleResolver::StyleResolver(const dom::Document *document) noexcept
            : m_document(document)
            , m_stylesheets{} {
        collectStylesheets();
    }

    StyleResolver::~StyleResolver() noexcept = default;

    void
    StyleResolver::collectStylesheets() noexcept {
        for (const auto *styleElement : m_document->styles()) {
            if (styleElement->children().empty())
                continue;

            LEMON_ASSERT(std::size(styleElement->children()) == 1);
            LEMON_ASSERT(styleElement->children().front()->nodeType() == dom::NodeType::TEXT);

            css::Parser parser{static_cast<const dom::Text *>(styleElement->children().front().get())->data()};
            auto stylesheet = parser.parseStylesheet();
            if (stylesheet.has_value()) {
                m_stylesheets.push_back(std::move(stylesheet.value()));
            }
        }
    }

    [[nodiscard]] bool
    equalsStringIgnoreCase(std::string_view a, std::string_view b) noexcept {
        if (std::size(a) != std::size(b))
            return false;
        if (std::empty(a))
            return true;
        for (std::size_t i = 0; i < std::size(a); ++i) {
            if (text::toASCIILowercase(a[i]) != text::toASCIILowercase(b[i]))
                return false;
        }
        return true;
    }

    [[nodiscard]] bool
    matchesComponent(const dom::Element *element, const css::Selector::Component &component) noexcept {
        switch (component.type) {
            case Selector::Component::Type::ID: {
                bool found = false;
                element->attributesWithName("id", [&] (const std::string &value) -> bool {
                    if (!equalsStringIgnoreCase(value, component.value))
                        return true;
                    found = true;
                    return false;
                });
                return found;
            }
            case Selector::Component::Type::TYPE:
                return equalsStringIgnoreCase(element->localName(), component.value);
            case Selector::Component::Type::CLASS:
                return std::find(std::begin(element->classList()), std::end(element->classList()), component.value)
                        != std::end(element->classList());
            case Selector::Component::Type::PSEUDO_CLASS:
                // TODO!
                return false;
        }

        return false;
    }

    [[nodiscard]] bool
    matchesSelector(const dom::Element *element, const css::Selector &selector, std::size_t componentID) noexcept {
        const auto &component = selector.components()[componentID];

        if (!matchesComponent(element, component))
            return false;

        switch (component.relationType) {
            case Selector::Component::RelationType::BEGIN:
                return true;
            case Selector::Component::RelationType::DESCENDANT:
                LEMON_ASSERT(componentID != 0);
                for (const auto *parent = element->parent(); parent != nullptr; parent = parent->parent()) {
                    if (parent->nodeType() != dom::NodeType::ELEMENT)
                        continue;
                    if (matchesSelector(static_cast<const dom::Element *>(parent), selector, componentID - 1))
                        return true;
                }
                return false;
            case Selector::Component::RelationType::UNDEFINED:
                LEMON_ASSERT_NOT_REACHED_OR_RETURN(false);
        }

        return false;
    }

    void
    StyleResolver::collectStylePropertiesForStylesheet(const Stylesheet &stylesheet, StyleProperties &properties,
                                                       const dom::Element *element) const noexcept {
        for (const auto &rule : stylesheet.rules()) {
            LEMON_ASSERT(rule->type() == Rule::Type::STYLE);
            const auto *styleRule = static_cast<const StyleRule *>(rule.get());

            for (const auto &selector : styleRule->selectors()) {
                if (matchesSelector(element, selector, std::size(selector.components()) - 1)) {
                    for (const auto &declaration : styleRule->declarations()) {
                        m_declarationTranslator.applyDeclarationToProperties(properties, declaration);
                    }

                    break;
                }
            }
        }
    }

    void
    StyleResolver::collectStyleProperties(StyleProperties &properties, const dom::Element *element) const noexcept {
        collectStylePropertiesForStylesheet(DefaultStylesheet::get(), properties, element);

        for (const auto &stylesheet : m_stylesheets) {
            collectStylePropertiesForStylesheet(stylesheet, properties, element);
        }

        element->attributesWithName("style", [&] (std::string_view value) {
            css::Parser parser{value};
            auto declarations = parser.parseDeclarationList();
            for (const auto &declaration : declarations) {
                m_declarationTranslator.applyDeclarationToProperties(properties, declaration);
            }
        });
    }

} // namespace css
