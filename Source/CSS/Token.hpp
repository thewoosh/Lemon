/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string>

namespace css {

    /**
     * https://www.w3.org/TR/css-syntax-3/#tokenization
     */
    struct Token {
        enum class Type {
            IDENT,
            FUNCTION,
            AT_KEYWORD,
            HASH,
            STRING,
            BAD_STRING,
            URL,
            BAD_URL,
            DELIM,
            NUMBER,
            PERCENTAGE, // 10
            DIMENSION,
            WHITESPACE,
            CDO,
            CDC,
            COLON,
            SEMICOLON,
            COMMA,
            LEFT_SQUARE_BRACKET,
            RIGHT_SQUARE_BRACKET,
            LEFT_PARENTHESIS, // 20
            RIGHT_PARENTHESIS,
            LEFT_CURLY_BRACKET,
            RIGHT_CURLY_BRACKET,
            END_OF_FILE,
        };

        static constexpr const bool HashUnrestricted = false;
        static constexpr const bool HashID = true;

        [[nodiscard]] inline constexpr Type
        type() const noexcept {
            return m_type;
        }

        [[nodiscard]] inline static Token
        createEOF() noexcept {
            return Token{Type::END_OF_FILE};
        }

        Type m_type;
        std::string m_stringData{};
        bool m_flag{};
        int m_integer{};
        float m_number{};
    };

} // namespace css
