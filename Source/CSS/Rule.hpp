/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

namespace css {

    struct Rule {
        enum class Type {
            STYLE,
        };

        [[nodiscard]] inline constexpr Type
        type() const noexcept {
            return m_type;
        }

    protected:
        [[nodiscard]] inline constexpr explicit
        Rule(Type type) noexcept
                : m_type(type) {
        }

    private:
        Type m_type;
    };

} // namespace css
