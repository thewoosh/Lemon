/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string>
#include <variant>

#include "Source/CSS/Color.hpp"
#include "Source/CSS/Length.hpp"

namespace css {

    struct Value;

    struct FunctionValueData {
        std::string name;
        std::vector<Value> parameters;
    };

    struct Value
            : public std::variant<Color, std::string, Length, FunctionValueData, char32_t> {
        enum class Type {
            COLOR,
            DELIMITER,
            FUNCTION,
            HASH,
            IDENTIFIER,
            LENGTH,
            STRING,
        };

        [[nodiscard]] inline constexpr Type
        type() const noexcept {
            return m_type;
        }

        template<typename InternalValue>
        [[nodiscard]] inline
        Value(Type type, InternalValue &&value) noexcept
                : variant(std::forward<InternalValue>(value))
                , m_type(type) {
        }

    private:
        Type m_type;
    };

} // namespace css
