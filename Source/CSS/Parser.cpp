/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#define CSS_PARSER_CPP
#include "Source/CSS/Parser.hpp"

#include "Include/text/Tools.hpp"
#include "Include/text/UnicodeBlocks/C0ControlsAndBasicLatin.hpp"
#include "Include/text/UnicodeBlocks/Specials.hpp"

#include "Source/Base/Assert.hpp"
#include "Source/CSS/StyleRule.hpp"
#include "Source/Text/Encoding/UTF8Decoder.hpp"
#include "Source/Text/Encoding/UTF8Encoder.hpp"

namespace css {

    Parser::Parser(std::string_view sv) noexcept {
        auto string = text::encoding::decodeUTF8(sv);
        LEMON_ASSERT(string.has_value());

        m_input = std::move(string.value());
        m_it = std::cbegin(m_input);
    }

    [[nodiscard]] inline static constexpr bool
    isHexDigit(char32_t character) noexcept {
        return (character >= text::LATIN_SMALL_LETTER_A && character <= text::LATIN_SMALL_LETTER_F)
            || (character >= text::LATIN_CAPITAL_LETTER_A && character <= text::LATIN_CAPITAL_LETTER_F)
            || text::isDigit(character);
    }

    [[nodiscard]] static std::string
    createSingleCodePointString(char32_t codePoint) noexcept {
        std::string string;
        text::encoding::appendCodePoint(string, codePoint);
        return string;
    }

    [[nodiscard]] inline static bool
    stringEqualsIgnoreCase(std::string_view a, std::string_view b) noexcept {
        return std::equal(std::cbegin(a), std::cend(a), std::cbegin(b), std::cend(b));
    }

    [[nodiscard]] inline static constexpr bool
    isWhitespace(char32_t character) noexcept {
        return character == text::LINE_FEED || character == text::CHARACTER_TABULATION || character == text::SPACE;
    }

    /**
     * https://www.w3.org/TR/css-syntax-3/#name-start-code-point
     */
    [[nodiscard]] inline static constexpr bool
    isNameStartCodePoint(char32_t character) noexcept {
        return text::isASCIIAlpha(character) || character == text::LOW_LINE || character > 0x80;
    }

    /**
     * https://www.w3.org/TR/css-syntax-3/#name-code-point
     */
    [[nodiscard]] inline static constexpr bool
    isNameCodePoint(char32_t character) noexcept {
        return isNameStartCodePoint(character) || text::isDigit(character) || character == text::HYPHEN_MINUS;
    }

    /**
     * https://www.w3.org/TR/css-syntax-3/#check-if-two-code-points-are-a-valid-escape
     */
    [[nodiscard]] inline static constexpr bool
    checkIfTwoCodePointsAreAValidEscape(char32_t a, char32_t b) noexcept {
        return a == text::REVERSE_SOLIDUS && b != text::LINE_FEED;
    }

    /**
     * https://www.w3.org/TR/css-syntax-3/#check-if-three-code-points-would-start-an-identifier
     */
    [[nodiscard]] inline static constexpr bool
    checkIfThreeCodePointsWouldStartAnIdentifier(char32_t a, char32_t b, char32_t c) noexcept {
        if (a == text::HYPHEN_MINUS)
            return isNameStartCodePoint(b) || checkIfTwoCodePointsAreAValidEscape(b, c);
        if (isNameStartCodePoint(a))
            return true;
        return checkIfTwoCodePointsAreAValidEscape(a, b);
    }

    /**
     * https://www.w3.org/TR/css-syntax-3/#check-if-three-code-points-would-start-an-identifier
     */
    [[nodiscard]] inline static constexpr bool
    checkIfThreeCodePointsWouldStartAnIdentifier(std::u32string::const_iterator &it, std::u32string::const_iterator end) noexcept {
        return checkIfThreeCodePointsWouldStartAnIdentifier(
                it == end ? text::NULL_CHARACTER : *it,
                std::next(it) == end ? text::NULL_CHARACTER : *std::next(it),
                std::next(it, 2) == end ? text::NULL_CHARACTER : *std::next(it, 2));
    }

    [[nodiscard]] inline static constexpr bool
    checkIfThreeCodePointsWouldStartANumber(std::u32string::const_iterator &it, std::u32string::const_iterator end) noexcept {
        if (it == end)
            return false;

        if (*it == text::PLUS_SIGN || *it == text::HYPHEN_MINUS) {
            if (std::next(it) != end && text::isDigit(*std::next(it)))
                return true;
            if (std::distance(it, end) >= 2 && *std::next(it) == text::FULL_STOP && text::isDigit(*std::next(it, 2)))
                return true;
            return false;
        }

        if (*it == text::FULL_STOP)
            return std::next(it) != end && text::isDigit(*std::next(it));

        return text::isDigit(*it);
    }

    template <typename T>
    [[nodiscard]] inline static constexpr T
    hexASCIIToInt(char32_t ch) {
        if (ch >= '0' && ch <= '9')
            return static_cast<T>(ch - text::DIGIT_ZERO);
        if (ch >= 'A' && ch <= 'F')
            return static_cast<T>(ch - text::LATIN_CAPITAL_LETTER_A + 10);
        if (ch >= 'a' && ch <= 'f')
            return static_cast<T>(ch - text::LATIN_SMALL_LETTER_A + 10);
        return static_cast<T>(-1);
    }

    template <typename T>
    [[nodiscard]] static T
    convertHexDigitToInteger(std::u32string::const_iterator &it, std::u32string::const_iterator end) noexcept {
        T value = 0;

        for (T i = 0; i < 6; ++i) {
            if (it == end)
                break;

            const auto part = hexASCIIToInt<T>(*it++);
            if (part == static_cast<T>(-1))
                break;

            value += part << (8 * static_cast<T>(sizeof(std::int32_t)) - 4 * (i + 1));
        }

        return value;
    }

    /**
     * https://www.w3.org/TR/css-syntax-3/#consume-an-escaped-code-point
     */
    [[nodiscard]] static char32_t
    consumeAnEscapedCodePoint(std::u32string::const_iterator &it, std::u32string::const_iterator end)
                noexcept {
        if (it == end) {
            // TODO parse error
            return text::REPLACEMENT_CHARACTER;
        }
        if (isHexDigit(*it)) {
            const auto value = convertHexDigitToInteger<char32_t>(it, end);

            // consume space if it follows the hex value
            if (it != end && *it == text::SPACE)
                ++it;

            if (value == 0 || (value >= 0xD800 && value <= 0xDFFF) || value >= 0x10FFFF) {
                return text::REPLACEMENT_CHARACTER;
            }

            return value;
        }

        LEMON_ASSERT_NOT_REACHED_OR_RETURN(text::REPLACEMENT_CHARACTER);
    }

    /**
     * https://www.w3.org/TR/css-syntax-3/#consume-a-name
     */
    [[nodiscard]] static std::string
    consumeAName(std::u32string::const_iterator &it, std::u32string::const_iterator end) noexcept {
        std::string name;

        while (it != end) {
            const auto nextInputCodePoint = *it++;

            if (isNameCodePoint(nextInputCodePoint)) {
                text::encoding::appendCodePoint(name, nextInputCodePoint);
            } else if (std::next(it) != end && checkIfTwoCodePointsAreAValidEscape(nextInputCodePoint, *std::next(it))) {
                text::encoding::appendCodePoint(name, consumeAnEscapedCodePoint(it, end));
            } else {
                --it;
                break;
            }
        }

        return name;
    }

    [[nodiscard]] inline static bool
    doesCharacterStartString(char32_t character) noexcept {
        return character == text::QUOTATION_MARK || character == text::APOSTROPHE;
    }

    /**
     * https://www.w3.org/TR/css-syntax-3/#consume-a-url-token
     */
    [[nodiscard]] static Token
    consumeAURLToken(std::u32string::const_iterator &it, std::u32string::const_iterator end) noexcept {
        static_cast<void>(it);
        static_cast<void>(end);
        LEMON_ASSERT_NOT_REACHED_OR_RETURN(Token{Token::Type::URL});
    }

    /**
     * https://www.w3.org/TR/css-syntax-3/#consume-ident-like-token
     */
    [[nodiscard]] static Token
    consumeAnIdentLikeToken(std::u32string::const_iterator &it, std::u32string::const_iterator end) noexcept {
        auto string = consumeAName(it, end);
        if (it != end && *it == text::LEFT_PARENTHESIS) {
            it += 1;

            if (stringEqualsIgnoreCase(string, "url")) {
                while (std::distance(it, end) >= 2 && isWhitespace(*it) && isWhitespace(*std::next(it)))
                    it += 2;

                if (doesCharacterStartString(*it)) {
                    ++it;
                    return Token{Token::Type::FUNCTION, std::move(string)};
                }

                if (std::distance(it, end) >= 2 && isWhitespace(*it) && doesCharacterStartString(*std::next(it))) {
                    it += 2;
                    return Token{Token::Type::FUNCTION, std::move(string)};
                }

                return consumeAURLToken(it, end);
            }

            return Token{Token::Type::FUNCTION, std::move(string)};
        }

        return Token{Token::Type::IDENT, std::move(string)};
    }

    /**
     * https://www.w3.org/TR/css-syntax-3/#convert-a-string-to-a-number
     */
    [[nodiscard]] std::variant<int, float>
    convertAStringToANumber(std::string &&string, bool isInteger) noexcept {
        if (isInteger) {
            auto sign = 1;
            if (string.starts_with('-')) {
                sign = -1;
                std::copy(std::begin(string) + 1, std::end(string), std::begin(string));
                string.resize(string.length() - 1);
            } else if (string.starts_with('+')) {
                std::copy(std::begin(string) + 1, std::end(string), std::begin(string));
                string.resize(string.length() - 1);
            }
            return std::stoi(string) * sign;
        }


        std::string_view input{string};
        auto convertStringToNumber = [&] () -> float {
            auto it = std::begin(input);
            for (; it != std::end(input); ++it) {
                if (!text::isDigit(*it)) {
                    break;
                }
            }

            if (it == std::begin(input))
                return 0.0f;

            const char *begin = std::data(input);
            const auto *end = &*it;
            input = std::string_view{it, std::end(input)};

            return static_cast<float>(std::strtol(begin, const_cast<char **>(&end), 10));
        };

        // 1. Sign
        float sign = 1;
        if (input.starts_with('-')) {
            sign = -1;
            input = input.substr(1);
        } else if (input.starts_with('+')) {
            input = input.substr(1);
        }

        // 2. Integer Part
        const auto integerPart = convertStringToNumber();

        // 3. Decimal Part
        if (input.starts_with('.'))
            input = input.substr(1);

        // 4. Fractional Part
        auto fractionalDigitCount = static_cast<float>(input.size());
        const auto fractionalPart = convertStringToNumber();
        fractionalDigitCount -= static_cast<float>(input.size());

        // 5. Exponent Indicator
        float exponentSign = 1.0f;
        float exponent = 0;
        if (input.starts_with('E') || input.starts_with('e')) {
            input = input.substr(1);

            // 6. Exponent Sign
            if (input.starts_with('-')) {
                exponentSign = -1.0f;
                input = input.substr(1);
            } else {
                input = input.substr(1);
            }

            exponent = convertStringToNumber();
        }

        // A verification check; it really should be empty by now, otherwise we
        // were given a bad integer, which is disallowed by the standard.
        LEMON_ASSERT(std::empty(input));

        return sign * (integerPart + fractionalPart * std::pow(10.0f, -fractionalDigitCount))
                            * std::pow(10.0f, exponentSign * exponent);
    }

    /**
     * https://www.w3.org/TR/css-syntax-3/#consume-a-number
     */
    [[nodiscard]] std::variant<int, float>
    consumeANumber(std::u32string::const_iterator &it, std::u32string::const_iterator end) noexcept {
        bool isInteger{true};
        std::string repr;

        // 2.
        if (it != end && (*it == text::PLUS_SIGN || *it == text::HYPHEN_MINUS)) {
            repr.push_back(static_cast<char>(*it++));
        }

        // 3.
        while (it != end && text::isDigit(*it)) {
            repr.push_back(static_cast<char>(*it++));
        }

        // 4.
        if (std::distance(it, end) >= 2 && *it == text::FULL_STOP && text::isDigit(*std::next(it))) {
            repr.push_back('.');
            repr.push_back(static_cast<char>(*std::next(it)));
            it += 2;
            isInteger = false;

            // 4.
            while (it != end && text::isDigit(*it)) {
                repr.push_back(static_cast<char>(*it++));
            }
        }

        // 5. exponents
        if (it != end && (*it == text::LATIN_CAPITAL_LETTER_E || *it == text::LATIN_SMALL_LETTER_E)) {
            ++it; // consume 'E' or 'e'

            if (it != end && text::isDigit(*it)) {
                // 1. & 2.
                repr.push_back(static_cast<char>(*it++));
                // 3.
                isInteger = false;
                // 4.
                while (it != end && text::isDigit(*it)) {
                    repr.push_back(static_cast<char>(*it));
                }
            } else if (std::distance(it, end) >= 2 && (*it == text::HYPHEN_MINUS || *it == text::PLUS_SIGN)
                       && text::isDigit(*std::next(it))) {
                // 2.
                repr.push_back(static_cast<char>(*it));
                repr.push_back(static_cast<char>(*std::next(it)));
                // 1.
                it += 2;

                // 3.
                isInteger = false;

                // 4.
                while (it != end && text::isDigit(*it)) {
                    repr.push_back(static_cast<char>(*it));
                }
            } else {
                // don't consume the 'e' or 'E' if it isn't followed by a digit
                --it;
            }
        }

        // 6. & 7.
        return convertAStringToANumber(std::move(repr), isInteger);
    }

    /**
     * https://www.w3.org/TR/css-syntax-3/#consume-a-numeric-token
     */
    [[nodiscard]] static Token
    consumeANumericToken(std::u32string::const_iterator &it, std::u32string::const_iterator end) noexcept {
        auto value = consumeANumber(it, end);

        Token token{Token::Type::NUMBER};
        if (value.index() == 0)
            token.m_integer = std::get<int>(value);
        else
            token.m_number = std::get<float>(value);

        if (checkIfThreeCodePointsWouldStartAnIdentifier(it, end)) {
            token.m_type = Token::Type::DIMENSION;
            token.m_stringData = consumeAName(it, end);
        } else if (it != end && *it == text::PERCENTAGE_SIGN) {
            ++it;
            token.m_type = Token::Type::PERCENTAGE;
        }

        return token;
    }

    /**
     * https://www.w3.org/TR/css-syntax-3/#consume-string-token
     */
    Token
    Parser::consumeStringToken(char32_t endingCodePoint) noexcept {
        const auto end = std::cend(m_input);
        Token token{Token::Type::STRING};

        while (m_it != end) {
            const auto nextInputCodePoint = *m_it++;

            if (nextInputCodePoint == endingCodePoint) {
                return token;
            }

            if (nextInputCodePoint == text::LINE_FEED || nextInputCodePoint == text::CARRIAGE_RETURN
                    || nextInputCodePoint == text::FORM_FEED) {
                // TODO parse error.
                --m_it;
                LEMON_ASSERT(false);
            }

            if (nextInputCodePoint == text::REVERSE_SOLIDUS) {
                if (m_it == end)
                    continue;

                if (std::next(m_it) != end && checkIfTwoCodePointsAreAValidEscape(*m_it, *std::next(m_it))) {
                    text::encoding::appendCodePoint(token.m_stringData, consumeAnEscapedCodePoint(m_it, end));
                    continue;
                }

                text::encoding::appendCodePoint(token.m_stringData, *m_it);
            }
        }

        // TODO eof = parse error
        LOG_ERROR("EOF PARSE ERROR");
        return token;
    }

    Token
    Parser::consumeToken() noexcept {
        const auto end = std::cend(m_input);

        // Consume comments
        while (std::next(m_it) != end && *m_it == text::SOLIDUS && *std::next(m_it) == text::ASTERISK) {
            m_it += 2;
            while (true) {
                if (m_it == end) {
                    // TODO parse error: eof-in-comment
                    break;
                }
                if (std::distance(m_it, end) >= 2 && *m_it == text::ASTERISK && *std::next(m_it) == text::SOLIDUS) {
                    m_it += 2;
                    break;
                }
                ++m_it;
            }
        }

        if (m_it != end && isWhitespace(*m_it)) {
            while (m_it != end && isWhitespace(*m_it)) {
                ++m_it;
            }
            return Token{Token::Type::WHITESPACE};
        }

        if (m_it == end)
            return Token{Token::Type::END_OF_FILE};

        switch (*m_it) {
            case text::NUMBER_SIGN: {
                ++m_it;
                const bool isName = m_it != end && isNameCodePoint(*m_it);
                const bool isEscape = std::next(m_it) != end &&
                        checkIfTwoCodePointsAreAValidEscape(*m_it, *std::next(m_it));
                if (isName || isEscape) {
                    Token token{Token::Type::HASH};
                    if (checkIfThreeCodePointsWouldStartAnIdentifier(m_it, end)) {
                        token.m_flag = Token::HashID;
                    }
                    token.m_stringData = consumeAName(m_it, end);
                    return token;
                }
                return Token{Token::Type::DELIM, createSingleCodePointString(text::NUMBER_SIGN)};
            }
            case text::QUOTATION_MARK:
            case text::APOSTROPHE:
                return consumeStringToken(*m_it++);
            case text::LEFT_PARENTHESIS:
                ++m_it;
                return Token{Token::Type::LEFT_PARENTHESIS};
            case text::RIGHT_PARENTHESIS:
                ++m_it;
                return Token{Token::Type::RIGHT_PARENTHESIS};
            case text::PLUS_SIGN: LEMON_ASSERT_NOT_REACHED_OR_RETURN(Token::createEOF());
            case text::COMMA:
                ++m_it;
                return Token{Token::Type::COMMA};
            case text::HYPHEN_MINUS: LEMON_ASSERT_NOT_REACHED_OR_RETURN(Token::createEOF());
            case text::FULL_STOP:
                if (checkIfThreeCodePointsWouldStartANumber(m_it, end)) {
                    return consumeANumericToken(m_it, end);
                }
                return Token{Token::Type::DELIM, createSingleCodePointString(*m_it++)};
            case text::COLON:
                ++m_it;
                return Token{Token::Type::COLON};
            case text::SEMICOLON:
                ++m_it;
                return Token{Token::Type::SEMICOLON};
            case text::LESS_THAN_SIGN:
                if (std::distance(m_it, end) >= 4 && *std::next(m_it) == text::EXCLAMATION_MARK
                        && *std::next(m_it, 2) == text::HYPHEN_MINUS
                        && *std::next(m_it, 3) == text::HYPHEN_MINUS) {
                    m_it += 4;
                    return Token{Token::Type::CDO};
                } else {
                    return Token{Token::Type::DELIM, createSingleCodePointString(*m_it++)};
                }
            case text::COMMERCIAL_AT:
                ++m_it;
                if (std::distance(m_it, end) >= 3
                && checkIfThreeCodePointsWouldStartAnIdentifier(*m_it, *std::next(m_it),*std::next(m_it, 2))) {
                    return Token{Token::Type::AT_KEYWORD, consumeAName(m_it, end)};
                }
                return Token{Token::Type::DELIM, createSingleCodePointString(*m_it)};
            case text::LEFT_SQUARE_BRACKET:
                return Token{Token::Type::LEFT_SQUARE_BRACKET};
            case text::REVERSE_SOLIDUS:
                if (std::distance(m_it, end) >= 2
                        && checkIfTwoCodePointsAreAValidEscape(*std::next(m_it), *std::next(m_it, 2))) {
                    ++m_it;
                    return consumeAnIdentLikeToken(m_it, end);
                }
                // TODO parse error
                return Token{Token::Type::DELIM, createSingleCodePointString(*m_it++)};
            case text::RIGHT_SQUARE_BRACKET:
                ++m_it;
                return Token{Token::Type::RIGHT_SQUARE_BRACKET};
            case text::LEFT_CURLY_BRACKET:
                ++m_it;
                return Token{Token::Type::LEFT_CURLY_BRACKET};
            case text::RIGHT_CURLY_BRACKET:
                ++m_it;
                return Token{Token::Type::RIGHT_CURLY_BRACKET};
            default:
                if (text::isDigit(*m_it)) {
                    // No need to reconsume since we haven't consumed anything yet.
                    return consumeANumericToken(m_it, end);
                }

                if (isNameStartCodePoint(*m_it)) {
                    // No need to reconsume since we haven't consumed anything yet.
                    return consumeAnIdentLikeToken(m_it, end);
                }

                return Token{Token::Type::DELIM, createSingleCodePointString(*m_it++)};
        }
    }

    Token
    Parser::consumeTokenSkippingWhitespace() noexcept {
        Token token;

        do {
            token = consumeToken();
        } while (token.type() == Token::Type::WHITESPACE);

        return token;
    }

    enum class FunctionParameterCommaStyle {
        OPTIONAL_COMMAS,
        REQUIRED_COMMAS,
        NO_COMMAS,
    };

    [[nodiscard]] constexpr FunctionParameterCommaStyle
    determineFunctionParameterCommaStyle(std::string_view name) noexcept {
        if (name == "rgb" || name == "rgba")
            return FunctionParameterCommaStyle::OPTIONAL_COMMAS;
        return FunctionParameterCommaStyle::NO_COMMAS;
    }

    FunctionValueData
    Parser::consumeFunction(Token &&startToken) noexcept {
        FunctionValueData data;
        data.name = std::move(startToken.m_stringData);

        while (true) {
            auto token = consumeTokenSkippingWhitespace();

            if (token.type() == Token::Type::RIGHT_PARENTHESIS) {
                break;
            }

            if (auto value = consumeValue(std::move(token))) {
                data.parameters.push_back(std::move(value.value()));

                const auto savedIt = m_it;
                token = consumeTokenSkippingWhitespace();
                if (token.type() == Token::Type::RIGHT_PARENTHESIS)
                    break;

                const auto commaStyle = determineFunctionParameterCommaStyle(data.name);
                if (commaStyle == FunctionParameterCommaStyle::NO_COMMAS) {
                    m_it = savedIt;
                    continue;
                }

                if (token.type() == Token::Type::COMMA)
                    continue;

                if (commaStyle == FunctionParameterCommaStyle::OPTIONAL_COMMAS) {
                    m_it = savedIt;
                    continue;
                }

                LOG_ERROR("Expected comma-separated parameters for function with name \"{}\"", data.name);
                LEMON_ASSERT(false);
            }

            LEMON_ASSERT(false);
        }

        return data;
    }

    std::optional<Value>
    Parser::consumeValue(Token &&token) noexcept {
        // TODO should we care about the difference between CSS integer and numbers?
        const float numberValue = token.m_integer == 0
                                  ? token.m_number
                                  : static_cast<float>(token.m_integer);

        switch (token.type()) {
            case Token::Type::STRING:
                return Value{Value::Type::STRING, std::move(token.m_stringData)};
            case Token::Type::IDENT:
                return Value{Value::Type::IDENTIFIER, std::move(token.m_stringData)};
            case Token::Type::FUNCTION:
                return Value{Value::Type::FUNCTION, consumeFunction(std::move(token))};
            case Token::Type::HASH:
                return Value{Value::Type::HASH, std::move(token.m_stringData)};
            case Token::Type::DELIM:
                return Value{Value::Type::DELIMITER, static_cast<char32_t>(token.m_stringData.front())};
            case Token::Type::NUMBER:
                return Value{Value::Type::LENGTH, Length{Length::Type::ABSOLUTE, numberValue}};
            case Token::Type::PERCENTAGE:
                return Value{Value::Type::LENGTH, Length{Length::Type::PERCENTAGE, numberValue}};
            case Token::Type::DIMENSION:
                if (token.m_stringData == "cm") {
                    return Value{Value::Type::LENGTH, Length{Length::Type::CENTIMETERS, numberValue}};
                } else if (token.m_stringData == "mm") {
                    return Value{Value::Type::LENGTH, Length{Length::Type::MILLIMETERS, numberValue}};
                } else if (token.m_stringData == "Q") {
                    return Value{Value::Type::LENGTH, Length{Length::Type::QUARTER_MILLIMETERS, numberValue}};
                } else if (token.m_stringData == "in") {
                    return Value{Value::Type::LENGTH, Length{Length::Type::INCHES, numberValue}};
                } else if (token.m_stringData == "pc") {
                    return Value{Value::Type::LENGTH, Length{Length::Type::PICAS, numberValue}};
                } else if (token.m_stringData == "pt") {
                    return Value{Value::Type::LENGTH, Length{Length::Type::POINTS, numberValue}};
                } else if (token.m_stringData == "px") {
                    return Value{Value::Type::LENGTH, Length{Length::Type::PIXELS, numberValue}};
                } else if (token.m_stringData == "em") {
                    return Value{Value::Type::LENGTH, Length{Length::Type::EM, numberValue}};
                } else if (token.m_stringData == "ex") {
                    return Value{Value::Type::LENGTH, Length{Length::Type::EX, numberValue}};
                } else if (token.m_stringData == "ch") {
                    return Value{Value::Type::LENGTH, Length{Length::Type::CH, numberValue}};
                } else if (token.m_stringData == "rem") {
                    return Value{Value::Type::LENGTH, Length{Length::Type::REM, numberValue}};
                } else {
                    LOG_WARNING("Unknown dimension: {}{}", token.m_stringData, numberValue);
                }
                break;
            case Token::Type::COMMA:
                return Value{Value::Type::DELIMITER, text::COMMA};
            default:
                LOG_ERROR("Unexpected token: {} {}", token.type(), token.m_stringData);
                LEMON_ASSERT(false);
        }

        return std::nullopt;
    }

    std::optional<Declaration>
    Parser::consumeDeclaration() noexcept {
        std::string propertyName;

        const auto savedIterator = m_it;
        auto token = consumeTokenSkippingWhitespace();
        if (token.type() != Token::Type::IDENT) {
            m_it = savedIterator;
            LOG_WARNING("Failed to consume a declaration, because the begin token wasn't an IDENT; {} {}", token.type(),
                        token.m_stringData);
            return std::nullopt;
        }

        propertyName = std::move(token.m_stringData);

        token = consumeTokenSkippingWhitespace();
        if (token.type() != Token::Type::COLON) {
            m_it = savedIterator;
            LOG_WARNING("Failed to consume a declaration, because the property wasn't followed by a COLON; {} {}",
                        token.type(), token.m_stringData);
            return std::nullopt;
        }

        std::vector<Value> values{};
        while (true) {
            token = consumeTokenSkippingWhitespace();
            if (token.type() == Token::Type::RIGHT_CURLY_BRACKET) {
                // It shouldn't be consumed by this function:
                --m_it;
            } else if (token.type() != Token::Type::END_OF_FILE && token.type() != Token::Type::SEMICOLON) {
                auto result = consumeValue(std::move(token));
                LEMON_ASSERT(result.has_value());
                values.push_back(std::move(result.value()));
                continue;
            }

            return Declaration{std::move(propertyName), std::move(values)};
        }
    }

    std::optional<Selector>
    Parser::consumeSelector() noexcept {
        std::vector<Selector::Component> components{};

        auto component = consumeSelectorComponent();
        if (component == std::nullopt) {
            if (m_it != std::cend(m_input))
                LOG_WARNING("Failed to consume begin component");
            return std::nullopt;
        }
        component->relationType = Selector::Component::RelationType::BEGIN;
        components.push_back(std::move(component.value()));

        while (true) {
            const auto savedIterator = m_it;

            // Check for early ending
            auto token = consumeTokenSkippingWhitespace();
            m_it = savedIterator;
            if (token.type() == Token::Type::COMMA || token.type() == Token::Type::LEFT_CURLY_BRACKET)
                break;

            auto relationType{Selector::Component::RelationType::DESCENDANT};
            // TODO check for other types of relations here.

            component = consumeSelectorComponent();
            if (component == std::nullopt) {
                m_it = savedIterator;
                break;
            }

            component->relationType = relationType;
            components.push_back(std::move(component.value()));
        }

        return Selector{std::move(components)};
    }

    std::optional<Selector::Component>
    Parser::consumeSelectorComponent() noexcept {
        const auto savedIterator = m_it;
        auto token = consumeTokenSkippingWhitespace();

        if (token.type() == Token::Type::DELIM && token.m_stringData == ".") {
            token = consumeTokenSkippingWhitespace();

            if (token.type() != Token::Type::IDENT) {
                LOG_WARNING("FULL STOP wasn't followed by an identifier!");
                return std::nullopt;
            }

//            LOG_TRACE("Found CLASS selector component \"{}\"", token.m_stringData);
            return Selector::Component{Selector::Component::Type::CLASS,
                                       Selector::Component::RelationType::UNDEFINED,
                                       std::move(token.m_stringData)};
        }

        if (token.type() == Token::Type::HASH) {
//            LOG_TRACE("Found ID selector component \"{}\"", token.m_stringData);
            return Selector::Component{Selector::Component::Type::ID,
                                       Selector::Component::RelationType::UNDEFINED,
                                       std::move(token.m_stringData)};
        }

        if (token.type() == Token::Type::IDENT) {
//            LOG_TRACE("Found TYPE selector component \"{}\"", token.m_stringData);
            return Selector::Component{Selector::Component::Type::TYPE,
                                       Selector::Component::RelationType::UNDEFINED,
                                       std::move(token.m_stringData)};
        }

        if (token.type() == Token::Type::COLON) {
            // https://www.w3.org/TR/selectors-4/#pseudo-classes

            token = consumeTokenSkippingWhitespace();
            if (token.type() != Token::Type::IDENT) {
                LOG_ERROR("Pseudo-class not followed by an identifier, but a {} {}", token.type(), token.m_stringData);
                return std::nullopt;
            }

            return Selector::Component{Selector::Component::Type::PSEUDO_CLASS,
                                       Selector::Component::RelationType::UNDEFINED,
                                       std::move(token.m_stringData)};
        }

        if (token.type() == Token::Type::END_OF_FILE) {
            return std::nullopt;
        }

        LOG_ERROR("Unknown start of selector component: {} {}", token.type(), token.m_stringData);
        m_it = savedIterator;
        return std::nullopt;
    }

    std::unique_ptr<StyleRule>
    Parser::consumeStyleRule() noexcept {
        std::vector<Selector> selectors{};
        std::vector<Declaration> declarations{};

        // Consume selectors
        while (true) {
            auto selector = consumeSelector();
            if (!selector) {
                if (m_it != std::cend(m_input))
                    LOG_WARNING("Failed to consume selector");
                return nullptr;
            }

            selectors.push_back(std::move(selector.value()));

            auto separatorToken = consumeTokenSkippingWhitespace();
            if (separatorToken.type() == Token::Type::LEFT_CURLY_BRACKET)
                break;

            if (separatorToken.type() == Token::Type::COMMA)
                continue;

            LOG_WARNING("Selector incorrectly continued! {} {}", separatorToken.type(), separatorToken.m_stringData);
            return nullptr;
        }

        // Consume declarations
        while (true) {
            auto declaration = consumeDeclaration();
            if (!declaration.has_value()) {
                LOG_WARNING("Failed to consume declaration");
                return nullptr;
            }

            declarations.push_back(std::move(declaration.value()));

            auto savedIterator = m_it;
            if (consumeTokenSkippingWhitespace().type() == Token::Type::RIGHT_CURLY_BRACKET) {
                break;
            }

            m_it = savedIterator;
        }

        return std::make_unique<StyleRule>(std::move(selectors), std::move(declarations));
    }

    std::vector<Declaration>
    Parser::parseDeclarationList() noexcept {
        std::vector<Declaration> result{};

        while (m_it != std::cend(m_input)) {
            auto declaration = consumeDeclaration();
            if (!declaration.has_value())
                break;

            result.push_back(std::move(declaration.value()));
        }

        return result;
    }

    std::optional<Stylesheet>
    Parser::parseStylesheet() noexcept {
        Stylesheet stylesheet;

        while (m_it != std::cend(m_input)) {
            auto rule = consumeStyleRule();

            if (rule == nullptr)
                break;

            stylesheet.rules().push_back(std::move(rule));
        }

        if (m_it != std::cend(m_input))
            LOG_WARNING("CSS ended before END_OF_FILE!");

        return stylesheet;
    }

} // namespace css
