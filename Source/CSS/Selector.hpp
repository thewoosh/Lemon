/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

namespace css {

    struct Selector {
        struct Component {
            enum class Type {
                TYPE,
                ID,
                CLASS,
                PSEUDO_CLASS,
            };

            enum class RelationType {
                UNDEFINED,

                BEGIN,
                DESCENDANT,
            };

            Type type;
            RelationType relationType;
            std::string value;
        };

        [[nodiscard]] inline explicit
        Selector(std::vector<Component> &&components) noexcept
                : m_components(std::move(components)) {
        }

        [[nodiscard]] inline std::vector<Component> &
        components() noexcept {
            return m_components;
        }

        [[nodiscard]] inline const std::vector<Component> &
        components() const noexcept {
            return m_components;
        }

    private:
        std::vector<Component> m_components;
    };

} // namespace css
