/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <optional>
#include <string>
#include <string_view>

#include "Source/CSS/Declaration.hpp"
#include "Source/CSS/Stylesheet.hpp"

#ifdef CSS_PARSER_CPP
#   include <memory>
#   include "Source/CSS/StyleRule.hpp"
#   include "Source/CSS/Token.hpp"
#endif

namespace css {

    class Parser {
    public:
        [[nodiscard]] explicit
        Parser(std::string_view) noexcept;

        [[nodiscard]] std::vector<Declaration>
        parseDeclarationList() noexcept;

        [[nodiscard]] std::optional<Stylesheet>
        parseStylesheet() noexcept;

    private:
#ifdef CSS_PARSER_CPP
        [[nodiscard]] std::optional<Declaration>
        consumeDeclaration() noexcept;

        [[nodiscard]] FunctionValueData
        consumeFunction(Token &&startToken) noexcept;

        [[nodiscard]] std::optional<Selector>
        consumeSelector() noexcept;

        [[nodiscard]] std::optional<Selector::Component>
        consumeSelectorComponent() noexcept;

        [[nodiscard]] std::unique_ptr<StyleRule>
        consumeStyleRule() noexcept;

        [[nodiscard]] Token
        consumeStringToken(char32_t endingCodePoint) noexcept;

        [[nodiscard]] Token
        consumeToken() noexcept;

        [[nodiscard]] Token
        consumeTokenSkippingWhitespace() noexcept;

        [[nodiscard]] std::optional<Value>
        consumeValue(Token &&token) noexcept;
#endif

        std::u32string m_input{};
        std::u32string::const_iterator m_it{};
    };

} // namespace css
