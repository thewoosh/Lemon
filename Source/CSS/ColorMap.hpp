/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <map>
#include <optional>

#include "Source/Base/CaseInsensitiveComparator.hpp"
#include "Source/Graphics/Types/Color.hpp"

namespace css {

    class ColorMap {
    public:
        [[nodiscard]]
        ColorMap() noexcept;

        [[nodiscard]] std::optional<gfx::Color>
        find(std::string_view name) const noexcept;

    private:
        std::map<std::string_view, gfx::Color, base::CaseInsensitiveComparator> m_map{};
    };

} // namespace css
