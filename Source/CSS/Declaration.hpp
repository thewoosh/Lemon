/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <string>
#include <vector>

#include "Source/CSS/Value.hpp"

namespace css {

    struct Declaration {
        [[nodiscard]] inline
        Declaration(std::string &&propertyName, std::vector<Value> &&values) noexcept
                : m_propertyName(std::move(propertyName))
                , m_values(std::move(values)) {
        }

        [[nodiscard]] inline const std::string &
        propertyName() const noexcept {
            return m_propertyName;
        }

        [[nodiscard]] inline const std::vector<Value> &
        values() const noexcept {
            return m_values;
        }

    private:
        std::string m_propertyName;
        std::vector<Value> m_values;
    };

} // namespace css
