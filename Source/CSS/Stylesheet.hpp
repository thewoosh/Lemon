/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <memory>
#include <vector>

#include "Source/CSS/Rule.hpp"

namespace css {

    struct Stylesheet {
        [[nodiscard]] Stylesheet() noexcept = default;
        [[nodiscard]] Stylesheet(Stylesheet &&) noexcept = default;
        [[nodiscard]] Stylesheet(const Stylesheet &) noexcept = delete;

        [[nodiscard]] Stylesheet &operator=(Stylesheet &&) noexcept = default;
        [[nodiscard]] Stylesheet &operator=(const Stylesheet &) noexcept = delete;

        [[nodiscard]] inline explicit
        Stylesheet(std::vector<std::unique_ptr<Rule>> &&rules) noexcept
                : m_rules(std::move(rules)) {
        }

        [[nodiscard]] inline std::vector<std::unique_ptr<Rule>> &
        rules() noexcept {
            return m_rules;
        }

        [[nodiscard]] inline const std::vector<std::unique_ptr<Rule>> &
        rules() const noexcept {
            return m_rules;
        }

    private:
        std::vector<std::unique_ptr<Rule>> m_rules;
    };

} // namespace css
