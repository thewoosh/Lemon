/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

namespace css {

    struct Length {
        enum class Type {
            ABSOLUTE,
            PERCENTAGE,

            CENTIMETERS,
            MILLIMETERS,
            QUARTER_MILLIMETERS,
            INCHES,
            PICAS,
            POINTS,
            PIXELS,

            EM,
            EX,
            CH,
            REM,
        };

        [[nodiscard]] inline constexpr
        Length(Type type, float value) noexcept
                : m_type(type)
                , m_value(value) {
        }

        [[nodiscard]] inline constexpr Type
        type() const noexcept {
            return m_type;
        }

        [[nodiscard]] inline constexpr float
        value() const noexcept {
            return m_value;
        }

    private:
        Type m_type;
        float m_value;
    };

} // namespace css
