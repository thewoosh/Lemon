/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <vector>

#include "Source/CSS/Declaration.hpp"
#include "Source/CSS/Rule.hpp"
#include "Source/CSS/Selector.hpp"

namespace css {

    struct StyleRule
            : public Rule {

        [[nodiscard]] inline
        StyleRule(std::vector<Selector> &&selectors, std::vector<Declaration> &&declarations) noexcept
                : Rule(Type::STYLE)
                , m_selectors(std::move(selectors))
                , m_declarations(std::move(declarations)) {
        }

        [[nodiscard]] inline const std::vector<Declaration> &
        declarations() const noexcept {
            return m_declarations;
        }

        [[nodiscard]] inline const std::vector<Selector> &
        selectors() const noexcept {
            return m_selectors;
        }

    private:
        std::vector<Selector> m_selectors;
        std::vector<Declaration> m_declarations;
    };

} // namespace css
