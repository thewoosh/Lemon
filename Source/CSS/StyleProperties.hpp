/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include "Source/Graphics/Types/Color.hpp"

namespace css {

    enum class VLVUnit {
        UNDEFINED,
        AUTO,       // the value doesn't matter in this case.

        PIXELS,     // 1px, 2in, 3cm, etc.
        PERCENTAGE, // e.g. 50%
        EM,
        EX,
        CH,
        REM,
    };

    // TODO this name kinda sucks, xd
    template <typename ContainedType>
    struct VariadicLengthValue {
        ContainedType value;
        VLVUnit unit{VLVUnit::UNDEFINED};
    };

    template <typename Type>
    struct Edge {
        Type top{};
        Type bottom{};
        Type left{};
        Type right{};

        [[nodiscard]] Edge() noexcept = default;
        [[nodiscard]] Edge(Edge &&) noexcept = default;
        [[nodiscard]] Edge(const Edge &) noexcept = default;
        Edge &operator=(Edge &&) noexcept = default;
        Edge &operator=(const Edge &) noexcept = default;

        [[nodiscard]] inline constexpr
        Edge(const Type &value) noexcept
                : top(value)
                , bottom(value)
                , left(value)
                , right(value) {
        }
    };

    enum class BorderStyle {
        NONE,
        SOLID,
    };

    enum class ClearType {
        NONE,
        LEFT,
        RIGHT,
        BOTH,
        INLINE_START,
        INLINE_END
    };

    /**
     * CSS Display Module Level 3
     * https://drafts.csswg.org/css-display-3/#propdef-display
     */
    enum class DisplayType {
        UNDEFINED,

        NONE,
        BLOCK,
        INLINE,
        LIST_ITEM,
    };

    enum class FloatType {
        LEFT,
        RIGHT,
        NONE,
        INLINE_START,
        INLINE_END
    };

    enum class ListStyleType {
        DISC,
        SQUARE,
    };

    enum class TextDecorationLineType {
        NONE,
        UNDERLINE,
    };

    enum class TextUnderlinePosition {
        AUTO,
        UNDER,
    };

    enum class UnicodeBidiType {
        NORMAL,
        EMBED,
        ISOLATE,
        BIDI_OVERRIDE,
        ISOLATE_OVERRIDE,
        PLAINTEXT
    };

    [[nodiscard]] inline constexpr bool
    isInlineLevelDisplay(DisplayType displayType) noexcept {
        return displayType == DisplayType::INLINE;
    }

    struct StyleProperties {
        Edge<VariadicLengthValue<float>> margin{{0, VLVUnit::PIXELS}};
        Edge<VariadicLengthValue<float>> border{{0, VLVUnit::PIXELS}};
        Edge<VariadicLengthValue<float>> padding{{0, VLVUnit::PIXELS}};

        Edge<gfx::Color> borderColor{gfx::Color{0.0f, 0.0f, 0.0f, 0.0f}};
        Edge<BorderStyle> borderStyle{BorderStyle::NONE};

        VariadicLengthValue<float> inheritedFontSize{0.0f, VLVUnit::PIXELS};
        VariadicLengthValue<float> fontSize{16.0f, VLVUnit::PIXELS};

        gfx::Color textColor{0.0f, 0.0f, 0.0f, 1.0f};
        gfx::Color backgroundColor{0.0f, 0.0f, 0.0f, 0.0f};

        ClearType clear{ClearType::NONE};

        DisplayType display{DisplayType::INLINE};

        FloatType floatType{FloatType::NONE};

        VariadicLengthValue<float> height{0, VLVUnit::AUTO};

        ListStyleType listStyleType{ListStyleType::DISC};

        VariadicLengthValue<float> width{0, VLVUnit::AUTO};

        TextDecorationLineType textDecorationLine{TextDecorationLineType::NONE};
        TextUnderlinePosition textUnderlinePosition{TextUnderlinePosition::AUTO};
        float textDecorationThickness{2.0f};

        UnicodeBidiType unicodeBidi{UnicodeBidiType::NORMAL};

        bool heightIsAuto{true};
        bool widthIsAuto{true};
    };

} // namespace css
