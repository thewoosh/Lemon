/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <optional>

#include "Source/CSS/ColorMap.hpp"
#include "Source/CSS/StyleProperties.hpp"
#include "Source/CSS/Value.hpp"

namespace css::ValueParser {

    [[nodiscard]] std::optional<gfx::Color>
    toColor(const ColorMap &colorMap, const Value &value) noexcept;

    [[nodiscard]] std::optional<float>
    toNumber(const Value &) noexcept;

    [[nodiscard]] std::optional<float>
    toPercentage(const Value &) noexcept;

    [[nodiscard]] std::optional<VariadicLengthValue<float>>
    toPixels(const Value &) noexcept;

} // namespace css::ValueParser
