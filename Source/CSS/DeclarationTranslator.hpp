/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <functional>
#include <map>
#include <optional>

#include "Source/CSS/ColorMap.hpp"
#include "Source/Graphics/Types/Color.hpp"

namespace css {

    struct Declaration;
    struct StyleProperties;
    struct Value;

    class DeclarationTranslator {
    public:
        [[nodiscard]]
        DeclarationTranslator() noexcept;

        void
        applyDeclarationToProperties(StyleProperties &styleProperties, const Declaration &declaration) const noexcept;

    private:
        std::map<std::string_view, std::function<void(StyleProperties &, const Declaration &)>> m_map{};
        ColorMap m_colorMap{};
    };

} // namespace css
