/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#pragma once

#include <functional>
#include <memory>

#include "Source/DOM/Document.hpp"
#include "Source/Graphics/RenderTarget.hpp"
#include "Source/Graphics/Types/Color.hpp"
#include "Source/GUI/Event/MouseClick.hpp"
#include "Source/GUI/Event/MouseMove.hpp"
#include "Source/GUI/Event/WindowResize.hpp"

class Application;

namespace gfx {
    class Painter;
} // namespace gfx

namespace layout {
    class LayoutNode;
} // namespace layout

class Tab {
public:
    [[nodiscard]]
    Tab(Application *, std::unique_ptr<dom::Document> &&document) noexcept;

    [[nodiscard]] Tab(Tab &&) noexcept;

    ~Tab() noexcept;

    [[nodiscard]] inline dom::Document *
    document() noexcept {
        return m_document.get();
    }

    [[nodiscard]] inline const dom::Document *
    document() const noexcept {
        return m_document.get();
    }

    void
    dumpLayoutTree() const noexcept;

    [[nodiscard]] bool
    initialize(gfx::Painter *) noexcept;

    void
    paint(gfx::Painter *) noexcept;

    // Callbacks
    std::function<void(gui::event::MouseClick)> onMouseClicked;
    std::function<void(gui::event::MouseMove)> onMouseMove;
    std::function<void(gui::event::WindowResize)> onWindowResize;

private:
    [[nodiscard]] bool
    continueLoadDocument() noexcept;

    [[nodiscard]] gfx::Color
    findBackgroundColor() const noexcept;

    Application *const m_application;

    std::unique_ptr<dom::Document> m_document;

    // FIXME: get this from the Application class.
    gfx::Size<std::uint32_t> m_renderSize{1280, 720};

    bool m_layoutNeedsUpdate{true};
    bool m_layoutNeedsPaint{true};
    std::unique_ptr<layout::LayoutNode> m_rootLayoutNode{nullptr};
    layout::LayoutNode *m_bodyLayoutNode{nullptr};

    layout::LayoutNode *m_hoveredLayoutNode{nullptr};

    gfx::RenderTarget *m_pageRenderTarget{nullptr};
};

