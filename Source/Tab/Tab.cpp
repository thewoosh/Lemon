/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 */

#include "Source/Tab/Tab.hpp"

#include "Source/Application.hpp"
#include "Source/Base/Assert.hpp"
#include "Source/Base/Timer.hpp"
#include "Source/DOM/Element.hpp"
#include "Source/DOM/Text.hpp"
#include "Source/Graphics/Painter.hpp"
#include "Source/HTML/Elements/HTMLStyleElement.hpp"
#include "Source/Layout/LayoutEngine.hpp"
#include "Source/Layout/LayoutNode.hpp"
#include "Source/Layout/LayoutText.hpp"
#include "Source/Text/Strings.hpp"

Tab::Tab(Application *application, std::unique_ptr<dom::Document> &&document) noexcept
        : m_application(application)
        , m_document(std::move(document)) {
}

Tab::~Tab() noexcept = default;
Tab::Tab(Tab &&) noexcept = default;

bool
Tab::continueLoadDocument() noexcept {
    m_document->postLoadHooks();

    if (!std::empty(m_document->title()))
        m_application->changeTitle(text::trim(m_document->title()));

    m_document->forEachChildElementByTagName("style", [&] (dom::Element *element) {
        m_document->styles().push_back(static_cast<html::HTMLStyleElement *>(element));
    });

    return true;
}

void
dumpLayoutTree(const layout::LayoutNode *const node, std::size_t depth) noexcept {
    using namespace std::string_view_literals;

    std::string description;
    if (node->dom() == nullptr)
        description = "anonymous";
    else if (node->dom()->nodeType() == dom::NodeType::ELEMENT)
        description = fmt::format("element with tag-name \"{}\"", static_cast<const dom::Element *>(node->dom())->localName());
    else if (node->dom()->nodeType() == dom::NodeType::TEXT)
        description = fmt::format("text \"{}\"", static_cast<const layout::LayoutText *>(node)->text());
    else
        description = "???";

    constexpr const std::array nodeTypes{"block", "text"};
    constexpr const std::array displayTypes{"undefined", "none", "block", "inline", "list-item" };
    LOG_INFO("{}Layout({}) {} {}x{} at ({}, {}) for {}", std::string(depth * 4, ' '),
             nodeTypes[static_cast<std::size_t>(node->type())],
             displayTypes[static_cast<std::size_t>(node->style().display)],
             node->visualSize().width(), node->visualSize().height(),
             node->position().x(), node->position().y(),
             description);

    for (const auto &child : node->children()) {
        dumpLayoutTree(child.get(), depth + 1);
    }
}

__attribute__((flatten)) void
Tab::dumpLayoutTree() const noexcept {
    LOG_INFO("Layout Tree:");
    ::dumpLayoutTree(m_rootLayoutNode.get(), 1);
}

gfx::Color
Tab::findBackgroundColor() const noexcept {
    if (m_bodyLayoutNode == nullptr || m_bodyLayoutNode->style().backgroundColor.a() == 0.0f)
        return {1.0f, 1.0f, 1.0f, 1.0f};
    return m_bodyLayoutNode->style().backgroundColor;
}

bool
Tab::initialize(gfx::Painter *) noexcept {
    onMouseClicked = [&] (gui::event::MouseClick event) {
        auto result = m_rootLayoutNode->hitTest(event.position());
        if (result.layoutNode == nullptr) {
            LOG_INFO("No layout node matched for hit test (very weird!)");
            return;
        }

        if (result.layoutNode->dom() && result.layoutNode->dom()->onClick)
            result.layoutNode->dom()->onClick(event.button());

        if (event.button() == gui::event::MouseButton::MIDDLE) {
            const auto &style = result.layoutNode->style();
            LOG_INFO("=== Printing out Layout Information ===");

            LOG_INFO("    General Section");
            std::string dom{"null"};
            if (result.layoutNode->dom()) {
                if (result.layoutNode->dom()->nodeType() == dom::NodeType::DOCUMENT)
                    dom = "document node";
                else if (result.layoutNode->dom()->nodeType() == dom::NodeType::TEXT)
                    dom = fmt::format("text \"{}\"", static_cast<const dom::Text *>(result.layoutNode->dom())->data());
                else if (result.layoutNode->dom()->nodeType() == dom::NodeType::ELEMENT)
                    dom = fmt::format("element with tag name \"{}\"", static_cast<const dom::Element *>(result.layoutNode->dom())->localName());
            }
            LOG_INFO("        DOM: {}", dom);

            LOG_INFO("    Box Model Section");
            LOG_INFO("        content size: {}x{}", result.layoutNode->contentSize().width(),
                     result.layoutNode->contentSize().height());
            LOG_INFO("        visual size: {}x{}", result.layoutNode->visualSize().width(),
                     result.layoutNode->visualSize().height());
            LOG_INFO("        padding: t={} b={} l={} r={}", style.padding.top.value, style.padding.bottom.value,
                     style.padding.left.value, style.padding.right.value);
            LOG_INFO("        border:  t={} b={} l={} r={}", style.border.top.value, style.border.bottom.value,
                     style.border.left.value, style.border.right.value);
            LOG_INFO("        margin:  t={} b={} l={} r={}", style.margin.top.value, style.margin.bottom.value,
                     style.margin.left.value, style.margin.right.value);

            LOG_INFO("    Visual Section");
            LOG_INFO("        background-color: {} {} {} {}", style.backgroundColor.r() * 255,
                     style.backgroundColor.g() * 255, style.backgroundColor.b() * 255, style.backgroundColor.a() * 255);
            LOG_INFO("        color: {} {} {} {}", style.textColor.r() * 255, style.textColor.g() * 255,
                                                   style.textColor.b() * 255, style.textColor.a() * 255);
        }
    };

    onMouseMove = [&] (gui::event::MouseMove event) {
        if (!m_rootLayoutNode)
            return;

        auto result = m_rootLayoutNode->hitTest(event.currentPosition());
        m_hoveredLayoutNode = result.layoutNode;

        if (result.layoutNode == nullptr) {
            LOG_INFO("No layout node matched for hit test (very weird!)");
            return;
        }

        if (result.layoutNode->dom()->parent() && result.layoutNode->dom()->parent()->nodeType() == dom::NodeType::ELEMENT
            && static_cast<const dom::Element *>(result.layoutNode->dom()->parent())->localName() == "a") {
            m_application->changeCursor(gui::CursorType::HAND);
        } else {
            m_application->changeCursor(gui::CursorType::ARROW);
        }
    };

    onWindowResize = [&] (gui::event::WindowResize event) {
        m_renderSize = event.size();
        m_layoutNeedsUpdate = true;
    };

    return continueLoadDocument();
}

void
Tab::paint(gfx::Painter *painter) noexcept {
    if (m_layoutNeedsUpdate) {
        base::Timer layoutTimer{};

        layout::LayoutEngine layoutEngine{m_document.get()};
        m_rootLayoutNode = layoutEngine.generateFromDocument();
        m_bodyLayoutNode = layoutEngine.bodyLayoutNode();
        if (!m_rootLayoutNode) {
            LOG_ERROR("Failed to produce a Layout Tree");
            LEMON_ASSERT(false);
        }

        layoutEngine.doLayout(m_rootLayoutNode.get(), painter, {static_cast<float>(m_renderSize.width()),
                                                                static_cast<float>(m_renderSize.height())});
        layoutTimer.stop();
        LOG_DEBUG("Layout Update in {} ms", layoutTimer.delta());

        dumpLayoutTree();

        m_pageRenderTarget = painter->createRenderTarget(m_renderSize);
        m_layoutNeedsUpdate = false;
        m_layoutNeedsPaint = true;
    }

    if (m_layoutNeedsPaint) {
        base::Timer timer;

        const auto backgroundColor = findBackgroundColor();
        m_pageRenderTarget->beginFrame(backgroundColor);

        m_rootLayoutNode->paint(painter);
        m_layoutNeedsPaint = false;

        m_pageRenderTarget->endFrame();

        timer.stop();
        LOG_INFO("Layout Paint in {} ms", timer.delta());
    }

    painter->paintRenderTarget(m_pageRenderTarget);

    if (m_hoveredLayoutNode && m_application->window()->isKeyDown(gui::Key::LEFT_CONTROL)) {
        painter->paintBorderAroundRect(m_hoveredLayoutNode->position(), m_hoveredLayoutNode->visualSize(),
                                       gfx::Color{1.0f, 0.0f, 1.0f, 1.0f}, 4.0f);
    }
}
