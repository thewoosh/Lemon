/**
 * SPDX-License-Identifier: MPL-2.0
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at https://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen <tristan@thewoosh.org>
 * All Rights Reserved.
 *
 * https://www.unicode.org/charts/PDF/UFFF0.pdf
 */

#pragma once

#include "../Base.hpp"

namespace text {

    constexpr const CodePoint INTERLINEAR_ANNOTATION_ANCHOR = 0xFFF9;
    constexpr const CodePoint INTERLINEAR_ANNOTATION_SEPARATOR = 0xFFFA;
    constexpr const CodePoint INTERLINEAR_ANNOTATION_TERMINATOR = 0xFFFB;
    constexpr const CodePoint OBJECT_REPLACEMENT_CHARACTER = 0xFFFC;
    constexpr const CodePoint REPLACEMENT_CHARACTER = 0xFFFD;

} // namespace text
