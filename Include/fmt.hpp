/*
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Copyright (C) 2021 Tristan Gerritsen
 * All Rights Reserved.
 */

#pragma once

#include <fmt/format.h>

#if FMT_VERSION > 80000
#   define FMTLIB_WRAPPER fmt::runtime
#else
#   define FMTLIB_WRAPPER
#endif
